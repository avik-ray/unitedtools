/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

jQuery.noConflict();

var jqtree = new Object;

(function($) {
	$(function() {
		/**
		 * AJAX icon
		 */
		$("html").bind("ajaxStart", function() {
			$('#ajax_loading').show();
		}).bind("ajaxStop", function(){
			$('#ajax_loading').hide();
		});

		/**
		 * Generate the menu trees.
		 */
		$('.tree').each(function() {
			$(this).tree({
				data: ( jqtree[ $(this).attr('rel') ].length > 0 ? jqtree[ $(this).attr('rel') ] : [] ),
				autoOpen: true,
				dragAndDrop: true,
				onCreateLi: function(node, $li) {
					$li.attr('id', node.id).find('.jqtree-title').before('<a href="#" class="btn-remove">Delete</a><a href="#" class="btn-edit">Edit</a>');
				},
				onIsMoveHandle: function($element) {
					return (!$element.is('.btn-edit, .btn-remove, .custom, .custom *'));
				}
			});
		});

		/**
		 * Add a link to the current menu.
		 */
		$('#add-link, #add-page, #add-cat').submit(function(e) {
			e.preventDefault();

			this.menu.value = $(page_tabsJsTabs.activeTab).attr('rel');

			$.post(this.action, $(this).serialize(), function(data) {
				$('#add-page, #add-cat').find('input:checked').attr('checked', false);

				$('#'+page_tabsJsTabs.activeTab.id+'_content .tree').tree( 'loadData', data.json );
			}, 'json');
		});

		/**
		 * Bind tree drag-and-drop actions.
		 */
		$('.tree').bind(
			'tree.move',
			function(event) {
				event.preventDefault();
				event.move_info.do_move();

				$('#reorder-tree').val( $(this).tree('toJson') );
				$('#reorder-menu').val( $(page_tabsJsTabs.activeTab).attr('rel') );

				$.post( $('#reorder-submit').attr('action'), $('#reorder-submit').serialize(), function(data) {
					$('#'+page_tabsJsTabs.activeTab.id+'_content .tree').tree( 'loadData', data.json );
				}, 'json');
			}
		);

		/**
		 * Bind 'edit' button actions.
		 */
		$('body').delegate('ul.jqtree-tree .btn-edit', 'click', function(e) {
			e.preventDefault();
			
			var id = $(this).closest('li').prop('id');

			$('#edit-id').val( id );

			$.post( $('#edit-get-form').attr('action'), $('#edit-get-form').serialize(), function(data) {
				$('#'+id+' div:first .custom').remove();
				$('#'+id+' div:first').append( data.form );
				$('#'+id+' .custom').slideDown();
				$('#'+id+' .custom input[type=text]:first').focus();
			}, 'json');
		});

		/**
		 * Bind 'delete' button actions.
		 */
		$('body').delegate('ul.jqtree-tree .btn-remove', 'click', function(e) {
			e.preventDefault();

			var id = $(this).closest('li').prop('id');
			var $tree = $('#'+page_tabsJsTabs.activeTab.id+'_content .tree');

			if( confirm("This link and all children will be removed. Are you sure?") ) {
				$tree.tree( 'removeNode', $tree.tree( 'getNodeById', id ) );

				$('#delete-tree').val( $tree.tree('toJson') );
				$('#delete-menu').val( $(page_tabsJsTabs.activeTab).attr('rel') );

				$.post( $('#delete-submit').attr('action'), $('#delete-submit').serialize(), function(data) {
					$tree.tree( 'loadData', data.json );
				}, 'json');
			}
		});
	});
})(jQuery);

function edit_submit(id) {
	(function($) {
		// Copy and sync form fields to the unnested form. Convenience!
		$('#submit-contents').html( $('#edit-submit-form-'+id).html() );
		$('#submit-contents input[type=text]').each(function(idx,i) {
			$(i).val( $('#edit-submit-form-'+id+' input[name='+$(i).attr('name')+']').val() );
		});

		$.post( $('#edit-submit-form').attr('action'), $('#edit-submit-form').serialize(), function(data) {
			$('#'+page_tabsJsTabs.activeTab.id+'_content .tree').tree( 'loadData', data.json );
		}, 'json');
	})(jQuery);
}
