<?php

require_once 'abstract.php';


class Mage_Shell_Test extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {
        
        $oldLogLevel = Mage::helper("klevu_search/config")->getLogLevel();
        Mage::getModel('core/config')->saveConfig(Klevu_Search_Helper_Config::XML_PATH_LOG_LEVEL, 
            Zend_Log::DEBUG);

        $s = Mage::getModel('klevu_search/product_sync');
        $s->runManually();

        Mage::getModel('core/config')->saveConfig(Klevu_Search_Helper_Config::XML_PATH_LOG_LEVEL, 
                $oldLogLevel);
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f test.php -- [options]
USAGE;
    }
}

$shell = new Mage_Shell_Test();
$shell->run();
