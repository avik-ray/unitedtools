<?php

require_once 'abstract.php';


class Mage_Shell_Process_Autosync extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {
        ini_set('memory_limit', '1G');
        $h = Mage::helper('synclog');
        $h->processUpdates();
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f process_autosync.php -- [options]
USAGE;
    }
}

$shell = new Mage_Shell_Process_Autosync();
$shell->run();


