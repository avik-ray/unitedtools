<?php
require_once 'abstract.php';
 
class United_Tools_Shell_Remove_Duplicates extends Mage_Shell_Abstract
{
    public function run()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = 'select catalog_product_entity.sku, catalog_product_entity.entity_id from catalog_product_entity JOIN (SELECT sku FROM catalog_product_entity group by sku having count(sku) > 1) as dups ON (dups.sku=catalog_product_entity.sku)';

        foreach($connection->fetchAll($sql) as $row) {
            $product = Mage::getModel('catalog/product')->load($row['entity_id']);
            echo "Removing product with entity_id=".$product->getId(). "\n";
            $product->delete();
        }
    }

    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f remove_duplicates.php -- [options]

  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new United_Tools_Shell_Remove_Duplicates();
$shell->run();
