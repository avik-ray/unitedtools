var Custompackage = Class.create();

Custompackage.prototype = {
    initialize: function(packages){
	this.dropdowns = $$('.select-box-packaging');
	this.packages = eval(packages);
	this.prepareDropdowns(this);
    },
    prepareDropdowns: function(custompackage) {
	this.dropdowns.each(function(el, index) {
	    if(el.id.match(/box_(\d+)_packaging/)) {
		Event.observe(el, 'change', function(){
		    custompackage.ondropchange(el, index+1);
		})
	    } else {
		custompackage.dropdowns.remove(el);
	    }
	});
    },
    addDropdown: function(index) {
	var el = $('box_'+index+'_packaging');
	Event.observe(el, 'change', function() {
	    custompackage.ondropchange(el, index);
	})
    },
    ondropchange: function(el, index) {
	var selected = el.value; 
	if(selected.match(/^custom/)) {
	    var pack = custompackage.packages[selected];
	    $('box_'+index+'_length').value = pack.length.slice(0, (pack.length.indexOf("."))+3);
	    $('box_'+index+'_width').value  = pack.width.slice(0,  (pack.width.indexOf(".")) +3);
	    $('box_'+index+'_height').value = pack.height.slice(0, (pack.height.indexOf("."))+3);
	    $('box_'+index+'_measure_unit').value  = pack.measure_unit;
	    fireEvent($('box_'+index+'_measure_unit'), 'change');
	    if(pack.weight_unit) {
		$('box_'+index+'_weight').value  = pack.weight.slice(0,  (pack.weight.indexOf(".")) +3);
		$('box_'+index+'_weight_unit').value  = pack.weight_unit;
	    }
	}
	return false;
    }
}

function fireEvent(element,event){
    if (document.createEventObject){
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt);
    }
    else{
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
}