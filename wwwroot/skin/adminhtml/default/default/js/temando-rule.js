function showHide(value) {
    switch(value) {
	case '1': //flat rate
	    $("rule_actions_static_fieldset").show();
	    $("rule_actions_dynamic_fieldset").hide();
	    $("rule_actions_restrict_fieldset").hide();
	    $("rule_action_static_value").enable();
	    break;
	case '2': //free shipping
	    $("rule_actions_static_fieldset").show();
	    $("rule_actions_dynamic_fieldset").hide();
	    $("rule_actions_restrict_fieldset").hide();
	    $("rule_action_static_value").setValue(0).disable();
	    break;
	case '3': //dynamic
	    $("rule_actions_dynamic_fieldset").show();
	    $("rule_actions_static_fieldset").hide();
	    $("rule_actions_restrict_fieldset").hide();
	    break;
	case '4': //restrict
	    $("rule_actions_restrict_fieldset").show();
	    $("rule_actions_static_fieldset").hide();
	    $("rule_actions_dynamic_fieldset").hide();
	    break;
    }
}

Event.observe(window, "load", function () {
    var ruleType = $("rule_action_rate_type");
    showHide(ruleType.options[ruleType.options.selectedIndex].value);
    
    $("rule_action_rate_type").observe("change", function () {
        showHide(this.options[this.options.selectedIndex].value);
    });
});


