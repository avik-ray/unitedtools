/*
 *override review next step to handle reposnses 
 * 
 */
var SecureframeReview = Class.create(Review, {
    /*
    initialize: function($super,saveUrl, successUrl, agreementsForm){
            $super(saveUrl, successUrl, agreementsForm);                 
    },
    */
    nextStep: function($super,transport){
        if (transport && transport.responseText) {
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
            if (response.redirect) {
                this.isSuccess = true;
                location.href = response.redirect;
                return;
            }
            if (response.success) {
                this.isSuccess = true;
                window.location=this.successUrl;
            }
            else{
                var msg = response.error_messages;
                if (typeof(msg)=='object') {
                    msg = msg.join("\n");
                }
                if (msg) {
                    alert(msg);
                }
            }

            if (response.secureframe) {
               //secure frame 
               for(i in checkout.steps){
                  var ele = $('opc-'+checkout.steps[i]);
                  if (ele){
                     if (ele.hasClassName('allow') )
                        ele.removeClassName('allow');
                  }
               }
               $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
               var sectionElement = $('opc-'+response.update_section.name);
               sectionElement.addClassName('allow');
               checkout.accordion.openSection('opc-'+response.update_section.name);
               
               
            }
            else {
                //normal flow
                if (response.update_section) {
                    $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
                }

                if (response.goto_section) {
                    checkout.gotoSection(response.goto_section);
                }
            }     
        }
    } 
});

