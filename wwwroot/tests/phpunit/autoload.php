<?php

ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR 
     . dirname(__FILE__) . '/../../app' . PATH_SEPARATOR . dirname(__FILE__));
//Set custom memory limit
ini_set('memory_limit', '512M');
//Include Magento libraries
require_once 'Mage.php';
//Start the Magento application
Mage::app('default');
//Avoid issues "Headers already send"
session_start();

class BaseTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->app = Mage::app('default');

        /* You will need a layout for block tests */
        $this->_layout = $this->app->getLayout();
    }
}
