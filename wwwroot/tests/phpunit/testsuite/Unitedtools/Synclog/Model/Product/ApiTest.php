<?php


class Unitedtools_Synclog_Model_ApiTest extends BaseTest
{
    private $SIMPLE_TEST_PRODUCT = 16963;
    private $CFG_TEST_PRODUCT = 16252;
    public function setUp() {
        Mage::register('isSecureArea', true);
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $this->helper = Mage::helper('synclog');
    }

    public function tearDown() {
        Mage::unregister('isSecureArea');
    }

    public function testGetAutolog() {
        $api = new Unitedtools_Synclog_Model_Product_Api();
        $r = $api->getAutolog($startWithId);
        $this->assertGreaterThan(2, strlen($r));
    }

}

