<?php


class Unitedtools_Synclog_Helper_DataTest extends BaseTest
{
    private $SIMPLE_TEST_PRODUCT = '__UNITTEST_1';
    #private $SIMPLE_TEST_PRODUCT = 16963;
    private $CFG_TEST_PRODUCT = 16252;
    public function setUp() {
        Mage::register('isSecureArea', true);
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $this->helper = Mage::helper('synclog');
        $this->_test_products = [];
    }

    public function tearDown() {
        foreach ($this->_test_products as $sku => $product) {
            $product->delete();
        }

        Mage::unregister('isSecureArea');
    }

    public function getTestProduct($sku) {
        if (!isset($this->_test_products[$sku])) {
            $id = Mage::getModel("catalog/product")->getIdBySku($sku);
            if ($id) {
                $product = Mage::getModel('catalog/product')->load($id);
                $product->delete();
            }
            $j = json_decode(file_get_contents(__DIR__ . '/data/' . $this->SIMPLE_TEST_PRODUCT . '.json'), TRUE);
            $j['sku'] = $sku;
            $this->helper->createOrUpdateProduct(['data' => $j], TRUE);
            $id = Mage::getModel("catalog/product")->getIdBySku($sku);
            $product = Mage::getModel('catalog/product')->load($id);
            $this->_test_products[$sku] = $product;
        }

        return $this->_test_products[$sku];
    }

    public function testNormalizeAttributeMediaGallery() {
        $r = $this->helper->normalizeAttribute('media_gallery', [
            'images' => [['file' => '/test/foo.png']]]);
        $base = Mage::getBaseUrl('media');
        $this->assertEquals($base . 'catalog/product/test/foo.png', $r['images'][0]['url']);
    }

    public function testNormalizeAttributeManufacturer() {
        $r = $this->helper->normalizeAttribute('manufacturer', 3);
        $this->assertEquals('Dewalt', $r);
    }

    public function testNormalizeAttributePrice() {
        $r = $this->helper->normalizeAttribute('price', 25.0);
        $this->assertEquals(25.0, $r);
    }

    public function testSerializeProductJson() {
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $r = $this->helper->serializeProductData($product->getData(), false, false, false);
        $j = $this->helper->serializeProductData($product->getData(), false, true, false);
        $j2 = json_encode($r);
        $this->assertEquals($j, $j2);
    }

    public function testSerializeSimpleProduct() {
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $r = $this->helper->serializeProductData($product->getData(), false, false, false);
        $j = json_encode($r, JSON_PRETTY_PRINT);

        # Uncomment to freeze serialization test
        file_put_contents(__DIR__ . '/data/' . $this->SIMPLE_TEST_PRODUCT . '.json', $j);
        $f = file_get_contents(__DIR__ . '/data/' . $this->SIMPLE_TEST_PRODUCT . '.json');
        $this->assertEquals($f, $j);
    }

    public function testSerializeConfigurableProduct() {
        return;
        $product = Mage::getModel('catalog/product')->load($this->CFG_TEST_PRODUCT);
        $r = $this->helper->serializeProduct($product, false, false, false);
        $j = json_encode($r, JSON_PRETTY_PRINT);

        # Uncomment to freeze serialization test
         file_put_contents(__DIR__ . '/data/' . $this->CFG_TEST_PRODUCT . '.json', $j);
        $f = file_get_contents(__DIR__ . '/data/' . $this->CFG_TEST_PRODUCT . '.json');
        $this->assertEquals($f, $j);
    }

    public function testChangeConfigurableProduct() {
        return;
        $product = Mage::getModel('catalog/product')->load($this->CFG_TEST_PRODUCT);
        $product->setData('27.0');
        $product->save();
    }

    public function testInsertSimpleProduct() {
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $this->assertEquals("Bahco Standard Thingy", $product->getName());
        $this->assertEquals("Standard Thingy", $product->getDescription());
    }

    public function testFlagModels() {
        $flag = Mage::getModel('unitedtools_synclog/synclog_flag_policies');
        $policies = $flag->loadSelf()->getFlagData();
        $this->assertNotEquals(false, $flag);
    }

    public function testGetChangesSimpleProduct() {
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $product->setName("NEW NAME");
        $product->save();
        $autolog = Mage::getModel('unitedtools_synclog/synclog_autolog')
            ->getCollection()
            ->setOrder('log_entry_id', 'DESC')
            ->getFirstItem();
        $l = json_decode($autolog->getData('json'), TRUE);
        $this->assertEquals('NEW NAME', $l['data']['name']);
    }

    public function testQueueUpdate() {
        $this->helper->queueUpdates('[{"id": 999999, "data": {}}]');
        Mage::getModel('unitedtools_synclog/synclog_pendingupdate')
            ->getCollection()->addFieldToFilter('log_entry_id', 999999)
            ->getFirstItem()->delete();
    }

    public function testQueueSize() {
        $this->helper->getUpdateQueueSize();
    }

    public function testSerPrices() {
        $this->helper->serializeAllPrices();
    }

    public function testSerAll() {
        json_decode($this->helper->serializeAllProductsAsJson());
    }

    public function testHandleCategoryImmutableAdd() {
        // Disable any category map
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => array());
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $product->setData('category_ids', [546]);
        $r = $this->helper->handleImmutableCategoryIds($product, [1, 2, 3]);
        $this->assertEquals([1, 2, 3, 546], $r);
    }

    public function testHandleCategoryImmutableRemove() {
        // Disable any category map
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => array());
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $product->setData('category_ids', []);
        $r = $this->helper->handleImmutableCategoryIds($product, [1, 2, 3, 546]);
        $this->assertEquals([1, 2, 3], $r);
    }

    public function testHandleCategoryImmutableNoOpKeep() {
        // Disable any category map
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => array());
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $product->setData('category_ids', [546]);
        $r = $this->helper->handleImmutableCategoryIds($product, [1, 2, 3, 546]);
        $this->assertEquals([1, 2, 3, 546], $r);
    }

    public function testHandleCategoryImmutableNoOp() {
        // Disable any category map
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => array());
        $product = $this->getTestProduct($this->SIMPLE_TEST_PRODUCT);
        $product->setData('category_ids', []);
        $r = $this->helper->handleImmutableCategoryIds($product, [1, 2, 3]);
        $this->assertEquals([1, 2, 3], $r);
    }

    public function testCategoryNoChange() {
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => [505 => 505]);
        $this->helper->updateCategory(json_decode('{
                    "id": "1011",
                    "action": "category_update",
                    "data": {
                        "id": "505",
                        "name": "Automotive",
                        "path": [
                            "1",
                            "2",
                            "505"
                        ],
                        "products": {
                            "D26414K-XE": "3",
                            "VISIBLITY-TEST-2": "2"
                        }
                    }
                }', TRUE));
        $cat = Mage::getModel("catalog/category")->load(505);
        $products = $cat->getProductCollection();
        $skus = array();
        foreach ($products as $p) {
            $skus[] = $p->getSku();
        }
        $this->assertEquals(["D26414K-XE", "VISIBLITY-TEST-2"], $skus, null, 0.0, 10, true);

        $this->helper->updateCategory(json_decode('{
                    "id": "1011",
                    "action": "category_update",
                    "data": {
                        "id": "505",
                        "name": "Automotive",
                        "path": [
                            "1",
                            "2",
                            "505"
                        ],
                        "products": {
                            "D26414K-XE": "3",
                            "VISIBLITY-TEST-2": "2"
                        }
                    }
                }', TRUE));
        $cat = Mage::getModel("catalog/category")->load(505);
        $products = $cat->getProductCollection();
        $skus = array();
        foreach ($products as $p) {
            $skus[] = $p->getSku();
        }
        $this->assertEquals(["D26414K-XE", "VISIBLITY-TEST-2"], $skus, null, 0.0, 10, true);
    }

    public function testCategoryAddRemove() {
        $this->helper->_policies = array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => [505 => 505]);
        $this->helper->updateCategory(json_decode('{
                    "id": "1011",
                    "action": "category_update",
                    "data": {
                        "id": "505",
                        "name": "Automotive",
                        "path": [
                            "1",
                            "2",
                            "505"
                        ],
                        "products": {
                            "D26414K-XE": "3",
                            "1-100-10": "1",
                            "VISIBLITY-TEST-2": "2"
                        }
                    }
                }', TRUE));
        $cat = Mage::getModel("catalog/category")->load(505);
        $products = $cat->getProductCollection();
        $skus = array();
        foreach ($products as $p) {
            $skus[] = $p->getSku();
        }
        $this->assertEquals(["D26414K-XE", "1-100-10", "VISIBLITY-TEST-2"], $skus, null, 0.0, 10, true);

        $this->helper->updateCategory(json_decode('{
                    "id": "1011",
                    "action": "category_update",
                    "data": {
                        "id": "505",
                        "name": "Automotive",
                        "path": [
                            "1",
                            "2",
                            "505"
                        ],
                        "products": {
                            "D26414K-XE": "3",
                            "VISIBLITY-TEST-2": "2"
                        }
                    }
                }', TRUE));
        $cat = Mage::getModel("catalog/category")->load(505);
        $products = $cat->getProductCollection();
        $skus = array();
        foreach ($products as $p) {
            $skus[] = $p->getSku();
        }
        $this->assertEquals(["D26414K-XE", "VISIBLITY-TEST-2"], $skus, null, 0.0, 10, true);
    }

}
