import unittest
import suds
import suds.plugin
from suds.client import Client

WSDL_FILE = 'http://utmaster.inklily.com/api/v2_soap?wsdl=1'
WSDL_USER = 'norwestapiro'
WSDL_PASSWORD = 'norwestapiropass'

class TestAutologApi(unittest.TestCase):
    def setUp(self):
        self.client = Client(WSDL_FILE)
        self.session = self.client.service.login(WSDL_USER, WSDL_PASSWORD,
                timeout=6000)

    #def test_dumpall(self):
    #    print(self.client.service.synclogProductDump(self.session))

    def test_get_autolog(self):
        print(self.client.service.synclogProductGetAutolog(self.session, 1000))

if __name__ == '__main__':
    unittest.main()

