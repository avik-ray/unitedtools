<?php
/**
 * SecurePay SecureFrame Payment Standard Method
 * Currently Supports Sale (Auth and Capture),Refund transactions
 * 
 */
class SecurePay_SecureFrame_Model_Standard extends Mage_Payment_Model_Method_Abstract {
    
    
    const SECUREPAY_SECUREFRAME_METHOD =  'secureframe';
    /**
     * initialize payment method varibles
     * according to the functions supported
     */
    protected $_code = self::SECUREPAY_SECUREFRAME_METHOD;
        
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = true;
    protected $_canRefundInvoicePartial = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;
    protected $_canSaveCc               = false;
    protected $_isInitializeNeeded      = true;
	
    /**
     * page that loads the SecurePay SecureFrame payment page
     * @return string
     */
    public function getOrderPlaceRedirectUrl() {
          return Mage::getUrl('secureframe/payment/redirect', array('_secure' => true));
    }

    /**
     * SecurePay url which request paramters are posted to
     * @param int $storeId
     * @return string
     */
    public function getActionUrl($storeId = null){
        return ( $this->getConfigData('test_mode',$storeId) == true 
                    ? $this->getConfigData('test_url',$storeId) 
                        : $this->getConfigData('live_url',$storeId) 
                ); 
                        
    }

    /**
     * SecurePay XML API url - this used for refunding payments
     * @param  int $storeId
     * @return string
     */
    public function getXmlApiUrl($storeId = null){
        return ( $this->getConfigData('test_mode',$storeId) == true 
                    ? $this->getConfigData('xml_test_url',$storeId) 
                        : $this->getConfigData('xml_live_url',$storeId) 
                ); 
                        
    }
    
    /**
     * Request paramters needed to loads the SecurePay SecureFrame payment page.
     * @param Mage_Sales_Model_Order $order 
     * @return array
     */
    public function getRequestParameters($order) {
               
        $storeId = $order->getStoreId();
        $request = array();
        
        $request = $this->_applyMandatoryFields($request, $order);
                                
        $request = $this->_applyTransactionFields($request, $order);
        
        $request = $this->_applyFlowFields($request, $storeId);
        
        $request = $this->_applySurchargeFields($request,$storeId);
        
        $request = $this->_applyLooknFeelFields($request,$storeId);
        //log return params
        Mage::helper('secureframe')->debug('request',$request,$storeId);
        
        return $request;
    }
    
    /**     
     * @param array $request
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _applyMandatoryFields($request,$order){
        
        $storeId            = $order->getStoreId();
        $orderId            = $order->getRealOrderId();        
        $merchantId         = $this->getConfigData('merchant_id',$storeId);                
        $txnType            = $this->getConfigData('transaction_type',$storeId);
        $txnPassword        = $this->getConfigData('transaction_password',$storeId);
        $time               = gmdate("YmdHis");        
        $amount             = round($order->getBaseGrandTotal(), 2) * 100;
        $fingerprint        = sha1($merchantId . '|' . $txnPassword . '|' . $txnType . '|' . $orderId . '|' . $amount . '|' . $time);
    
        $request["bill_name"]         = "transact";
        $request["merchant_id"]       = $merchantId;
        $request["txn_type"]          = $txnType;
        $request["amount"]            = $amount;
        $request["primary_ref"]       = $orderId;
        $request["fp_timestamp"]      = $time;
        $request["fingerprint"]       = $fingerprint;
        
        
        //$request["meta"]              = $this->getMetaData($order);
        
        return $request;
    }      
        
    
    
    /*
     * out of scope     
     protected function _applyCardStorageFields($request,$storeId){
     } 
     * 
    */   
    
    /**     
     * @param array $request
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _applyTransactionFields($request,$order){
        $storeId                            = $order->getStoreId();
        $request["currency"]                = $order->getBaseCurrencyCode();
        $request["display_cardholder_name"] = $this->getConfigData('display_cardholder_name',$storeId)?"yes":"no";
        
        return $request;
    }
    
    /**     
     * @param array $request
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _applyFlowFields($request,$storeId){
       
        $isSecure                       = $this->getConfigData('test_mode',$storeId) == true ? false : true;
        $request["display_receipt"]     = $this->getConfigData('display_receipt',$storeId)?"yes":"no";
        $request["return_url"]          = Mage::getUrl("secureframe/payment/return",  array('_secure' => $isSecure));
        
        $text = $this->getConfigData('button_text',$storeId);        
        if ($text && strlen($text)){
            $request["return_url_text"]  = $this->getConfigData('button_text',$storeId);
        }         
        
        $request["return_url_target"]   = "parent";
        $request["callback_url"]        = Mage::getUrl("secureframe/payment/callback",array('_secure' => $isSecure));
        $request["cancel_url"]          = Mage::getUrl("secureframe/payment/return",  array('_secure' => $isSecure));
        
        $text = $this->getConfigData('button_cancel_text',$storeId);        
        if ($text && strlen($text)){
            $request["cancel_url_text"]  = $this->getConfigData('button_cancel_text',$storeId);
        }
        
        $request["cancel_url_target"]   = "parent";
        $request["confirmation"]        = $this->getConfigData('confirmation',$storeId)?"yes":"no";
                
        return $request;
    }
    
    /**     
     * @param array $request
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _applyLooknFeelFields($request,$storeId){
          //look and feel
        $value = $this->getConfigData('template',$storeId);
        if ( $value && strlen($value)){
            $request['template'] = $value;
        }

        $value = $this->getConfigData('order_ref_name',$storeId);
        if ( $value && strlen($value)){
            $request['primary_ref_name'] = $value;
        }
        
        $value = $this->getConfigData('header_image_url',$storeId);
        if ( $value && strlen($value)){
            $request['page_header_image'] = $value;
        }
        
        $value = $this->getConfigData('footer_image_url',$storeId);
        if ( $value && strlen($value)){
            $request['page_footer_image'] = $value;
        }
        
        $value = $this->getConfigData('page_title',$storeId);
        if ( $value && strlen($value)){
            $request['page_title'] = $value;
        }
        
        $request['card_types'] = implode('|',$this->getAcceptedCardTypes($storeId));
       
        $value = $this->getConfigData('stylesheet_url',$storeId);
        if ( $value && strlen($value)){
            $request['page_style_url'] = $value;
        }
      
        return $request;
    }
    
    /**     
     * @param array $request
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _applySurchargeFields($request,$storeId){
        
        if ($this->getConfigData('surcharge',$storeId)){             
            $request["surcharge"] = "yes";
            $rate =  (float) $this->getConfigData('surcharge_rate',$storeId);
            $fee  =  (float) $this->getConfigData('surcharge_fee',$storeId);      
            
            if ($rate > 0){
                $request['surcharge_rate'] = $rate; 
             }
             if ($fee > 0) {
                $request['surcharge_fee'] = ($fee * 100); 
             } 
             
            $cardTypes = $this->getAcceptedCardTypes($storeId);
            
            foreach($cardTypes as $cardType){
                
                if ($cardType == 'PAYPAL')
                    continue;
                
                $card             = strtolower(substr($cardType, 0, 1));
                $surchargeRateKey = 'surcharge_rate_'.$card;
                $surchargeFeeKey  = 'surcharge_fee_'.$card;
                $rate =  (float) $this->getConfigData($surchargeRateKey,$storeId);
                $fee  =  (float) $this->getConfigData($surchargeFeeKey, $storeId);      
                if ($rate > 0){
                   $request[$surchargeRateKey] = $rate; 
                }
                if ($fee > 0) {
                    $request[$surchargeFeeKey] = ($fee * 100); 
                }
            }                        
        }
        return $request;
    }

    public function getAcceptedCardTypes($storeId){
        $cardTypes = $this->getConfigData('accepted_card_types',$storeId);         
        if ($cardTypes && strlen($cardTypes)){
            return explode(',', $cardTypes);
        }
        return array();
    }
    
    /*
    public function getMetaData($order) { 
        $shipping_meta = $order->getShippingDescription();
        $billing_meta  = "none";
        $delivery_meta = "none";	
        $customer_meta  = $order->getCustomerName().'|'.$order->getCustomerEmail();  
        
        $billing = $order->getBillingAddress();
        if (!empty($billing)) {
        	$billing_meta =       			
                               (is_array($billing->getStreetFull())?implode('\n', $billing->getStreetFull()):$billing->getStreetFull()). '|' . 
                                $billing->getCity()    . '|' .
        			$billing->getRegion()  . '|' .	
                                $billing->getPostcode(). '|' . 
                                $billing->getCountry() . '|' .
                                $billing->getTelephone();
        }                       
        
        $shipping = $order->getShippingAddress();
        if (!empty($shipping)) {        
            $delivery_meta =   (is_array($shipping->getStreetFull())?implode('\n', $shipping->getStreetFull()):$shipping->getStreetFull()). '|' . 
                                $shipping->getCity()    . '|' .
        			$shipping->getRegion()  . '|' .	
                                $shipping->getPostcode(). '|' . 
                                $shipping->getCountry() . '|' .
                                $shipping->getTelephone();        			
        }
                
        return 'cart_post_method_eq_'.$shipping_meta.'$'.
               'cart_billing_address_eq_'.$billing_meta.'$'.
               'cart_delivery_address_eq_'.$delivery_meta.'$'.
               'customer_detail_eq_'.$customer_meta;        
    }
    */
    

    /**
     * Instantiate state and set it to state object
     * @param string $paymentAction
     * @param Varien_Object
     */
    public function initialize($paymentAction, $stateObject)
    {
        $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
        $stateObject->setState($state);
        $stateObject->setStatus('pending_payment');
        $stateObject->setIsNotified(false);
    }
    
    /**
     * SecurePay refund differs from normal refund. SecurePay only allows full
     * refund.
     * Also surcharges are allowed in Sale transactions  and they are not reflected 
     * in the Order. So only way to reliablly refund is to take amount from the 
     * transaction raw details returned from SecurePay for the refunding Order. 
     * here amount value passed from credit memo is ignored.
     * 
     * @param Varien_Object $payment
     * @param float $amount
     * @return SecurePay_SecureFrame_Model_Standard
     */
    public function refund(Varien_Object $payment, $amount)
    {
        
    	if (!$this->canRefund()) {
            return $this;
    	}
    	    	
        $originalTxnId = $payment->getLastTransId();        
        
        if (!$originalTxnId){            
            Mage::throwException(Mage::helper('secureframe')->__('Invalid transaction Id'));
        }
        
        $data = $payment->getTransaction($originalTxnId)->getAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS);
        
        //get the full amount from transaction details
        if ( !isset($data["amount"]) || $data["amount"] <= 0  ){
            Mage::throwException(Mage::helper('secureframe')->__('Invalid amount for refund.'));
        }
        
        if ( !isset($data["txnid"]) || strlen($data["txnid"]) == 0   ){
            Mage::throwException(Mage::helper('secureframe')->__('Invalid Bank Transaction Id.'));
        }
                                          
        $order       = $payment->getOrder();
        
        if ($order->getBaseGrandTotal() != $amount){
            Mage::throwException(Mage::helper('secureframe')->__('Partial refunds or Amount modification not allowed!'));
        } 
        
        $realOrderId = $order->getRealOrderId();
        $storeId = $order->getStoreId();                
        $gatewayUrl = $this->getXmlApiUrl($storeId);
        
        //initialize refund request details
        $request = array();
        $request["messageID"]        = $this->_getUUIDV4();
        $request["messageTimestamp"] = $this->_getGMTTimeStamp();
        $request["merchantID"]       = $this->getConfigData('merchant_id',$storeId);
        $request["password"]         = $this->getConfigData('transaction_password',$storeId);
        $request["amount"]           = $data["amount"];
        $request["purchaseOrderNo"]  = $order->getRealOrderId();
        $request["txnID"]            = $data["txnid"];
                
        //send request
        $xmlRequest  = $this->_createXmlString($request);                
        Mage::helper("secureframe")->debug("Refund Request",$request,$storeId);
        $xmlResponse = $this->_sendRefundRequest($gatewayUrl, $xmlRequest);         
        
        //xml to array transformation
        $xml         =  simplexml_load_string($xmlResponse);       
        $response    =  $this->_objectToArray($xml);                
        $params      =  $this->_arrayFlatten($response);
        $params['xml_response'] = $xmlResponse;
        Mage::helper("secureframe")->debug("Refund Response",$params,$storeId);
       
        //initilize reponse states
        $approved           = isset($response['Payment']['TxnList']['Txn']['approved'])
                                        ?$response['Payment']['TxnList']['Txn']['approved']:false;
        $statusCode         = isset($response['Status']['statusCode'])
                                        ? $response['Status']['statusCode']:false;
        $statusDesc         = isset($response['Status']['statusDescription'])
                                        ? $response['Status']['statusDescription']:false;
        $responseCode        = isset($response['Payment']['TxnList']['Txn']['responseCode'])
                                        ?$response['Payment']['TxnList']['Txn']['responseCode']:false;
        $responseDesc       = isset($response['Payment']['TxnList']['Txn']['responseText'])
                                        ?$response['Payment']['TxnList']['Txn']['responseText']:false;
        $transactionId       =  isset($response['Payment']['TxnList']['Txn']['txnID'])
                                        ?$response['Payment']['TxnList']['Txn']['txnID']:false;
        
        //process accordingly to status returned        
        if ($approved && strcasecmp($approved, 'Yes') == 0 &&  $statusCode == "000" && $transactionId )
        {                               
            $payment->resetTransactionAdditionalInfo()
                     ->setPreparedMessage("SecurePay SecureFrame")	
                     ->setTransactionId($realOrderId.'_'.$transactionId)
                     ->setIsTransactionClosed(0);            
                                                             
             $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$params);
        }
         else {
             
            $message = Mage::helper("secureframe")->__('Error during refund online.');            
            if ($statusCode && $statusDesc && $responseCode === false  ){
                $message .= Mage::helper('secureframe')->__('Status Code: %s Description : %s',$statusCode,$statusDesc);
            }
            else if ($responseCode && $responseDesc){
                $message .= Mage::helper('secureframe')->__('Response Code: %s Description : %s',$responseCode,$responseDesc);           
                
            } 
            Mage::throwException($message);
    	}
        
        return $this;    	
    }
     
    /**
     * returns refund xml request string 
     * @param array $request
     * @return string
     */
    
    protected function _createXmlString($request) {
      return <<<EOD
<?xml version="1.0" encoding="UTF-8"?> 
<SecurePayMessage> 
    <MessageInfo> 
        <messageID>{$request["messageID"]}</messageID>                     
        <messageTimestamp>{$request["messageTimestamp"]}</messageTimestamp> 
        <timeoutValue>60</timeoutValue> 
        <apiVersion>xml-4.2</apiVersion> 
    </MessageInfo> 
    <MerchantInfo> 
        <merchantID>{$request["merchantID"]}</merchantID> 
        <password>{$request["password"]}</password> 
    </MerchantInfo> 
    <RequestType>Payment</RequestType> 
    <Payment> 
        <TxnList count="1"> 
            <Txn ID="1"> 
                <txnType>4</txnType> 
                <txnSource>23</txnSource> 
                <amount>{$request["amount"]}</amount> 
                <purchaseOrderNo>{$request["purchaseOrderNo"]}</purchaseOrderNo> 
                <txnID>{$request["txnID"]}</txnID>
                <CreditCardInfo>
                    <expiryDate></expiryDate>
                </CreditCardInfo>                             
            </Txn> 
        </TxnList> 
    </Payment> 
</SecurePayMessage>
EOD;
           
    }
    
    

    /*
     * Create unique (A universally unique identifier - UUID)string for used as Request message id.
     * uses  Version 4 (random) UUIDs. 
     * Refer: http://php.net/manual/en/function.uniqid.php
     * @return string
     */
    protected function _getUUIDV4() 
    {
      $uuid =  sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
      
     if (!$uuid || strlen($uuid) != 32)
          throw new Exception(Mage::helper('secureframe')->__('Request Message Id creation failed!'));
      
     return $uuid;
  }

  /**
    * getGMTTimeStamp:
    * 
    * this function creates a timestamp formatted as per requirement in the
    * SecureXML documentation
    *
    * @return string The formatted timestamp
    */
    protected function _getGMTTimeStamp()
    {
        /*   Format: YYYYDDMMHHNNSSKKK000sOOO
             YYYY is a 4-digit year
             DD is a 2-digit zero-padded day of month
             MM is a 2-digit zero-padded month of year (January = 01)
             HH is a 2-digit zero-padded hour of day in 24-hour clock format (midnight =0)
             NN is a 2-digit zero-padded minute of hour
             SS is a 2-digit zero-padded second of minute
             KKK is a 3-digit zero-padded millisecond of second
             000 is a Static 0 characters, as SecurePay does not store nanoseconds
             sOOO is a Time zone offset, where s is + or -, and OOO = minutes, from GMT.
        */
        $tz_minutes = date('Z') / 60;

        if ($tz_minutes >= 0)
        {
                $tz_minutes = '+' . sprintf("%03d",$tz_minutes); //Zero padding in-case $tz_minutes is 0
        }

        $stamp = date('YdmHis000000') . $tz_minutes; //In some locales, in some situations (i.e. Magento 1.4.0.1) some digits are missing. Added 5 zeroes and truncating to the required length. Terrible terrible hack.

        return $stamp;
    }
    
    /**
    * sendRedundRequest: 
    *
    * uses cURL to open a Secure Socket connection to the gateway,
    * sends the transaction request and then returns the response
    * data
    * 
    * @param $postURL The URL of the remote gateway to which the request is sent
    * @param $requestMessage
    */
   protected function _sendRefundRequest($postURL, $requestMessage)
   {
        $ch = curl_init();

        //Set up curl parameters
        curl_setopt($ch, CURLOPT_URL, $postURL);        // set remote address
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	// Make CURL pass the response as a curl_exec return value instead of outputting to STDOUT
        curl_setopt($ch, CURLOPT_POST, 1);	 	// Activate the POST method
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestMessage);	// add the request message itself

        //execute the connection
        $result = curl_exec($ch);

        $debugoutput = curl_getinfo($ch);
        $curl_error_message = curl_error($ch); // must retrieve an error message (if any) before closing the curl object 

        curl_close($ch);

        if ($result === false)
        {
            $msg = Mage::helper('Request failed with following error: ').$curl_error_message;
            Mage::throwException($msg);            
        }
       
        return $result;
   }
   
    /**
     * transform object to multi-dimensioal array
     * used in transforming xml to array
     * 
     * @param  object $obj
     * @return array
     */  
   private function _objectToArray($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->_objectToArray($val);
            }
        }
        else $new = $obj;
        return $new; 
    }
       
   /**
    * flattens multi-dimensional array to single dimensional array
    * this needed to save transaction details as key-value pairs
    * 
    * @param array $array
    * @return array
    */ 
  private  function _arrayFlatten($array) {
   $return = array();
   foreach ($array as $key => $value) {
       if (is_array($value)){ $return = array_merge($return, $this->_arrayFlatten($value));}
       else {$return[$key] = $value;}
   }
   return $return;
 }
       
}