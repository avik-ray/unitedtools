<?php
/**
 * The main classes the processes reponse from SecurePay
 * This class is called by all returining urls:return,callback,cancel
 * This class supports Payment type "SALE" 
 */
class SecurePay_SecureFrame_Model_Processor {

    /**
     * Summary reponse codes from SecurePay
     */
    const SECUREPAY_STATUS_APPROVED     =   1;
    const SECUREPAY_STATUS_FAIL_BANK    =   2;
    const SECUREPAY_STATUS_FAIL_OTHER   =   3;
    const SECUREPAY_STATUS_CANCEL       =   4;   
    

    /*
     * @var Mage_Sales_Model_Order
     */
    protected $_order         = null;

    /**
     * POSTed data from SecurePay
     * @var array
     */
    protected $_responseData  = array();

    /**
     * true if callback url called this class
     * @var boolean
     */
    private $_isCallback      = false;
            
   
    /**
     * used debug reponse/request variables
     */
    private $_debug           = false;
    
   
    /**
     * called callback url
     */
    public function setCallback(){
        $this->_isCallback = true;
    }
    
    /**
     * true if current executed by callback url
     * @return boolean
     */
    public function isCallback(){
        return $this->_isCallback;
    } 
    
    /**
     * Reponse data setter
     * @param array $data
     * @return SecurePay_SecureFrame_Model_Processor
     */
    public function setResponseData(array $data)
    {
        $this->_responseData = $data;
        return $this;
    }

    /**
     * Reponse data getter
     * @param string $key
     * @return array|string
     */
    public function getResponseData($key = null)
    {
        if (null === $key) {
            return $this->_responseData;
        }
        return isset($this->_responseData[$key]) ? $this->_responseData[$key] : null;
    }

    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }    
    
    /**
     * Process reponses from server
     * returns array contains status of processing and messages
     * @return array
     */
    public function process()
    {
        $msg = '';
        $event = '';
       
        try {            
            $params = $this->_validateResponseData();            
            switch($params['summarycode']) {                
                case self::SECUREPAY_STATUS_FAIL_BANK: //fail
                case self::SECUREPAY_STATUS_FAIL_OTHER:    
                    $msg = Mage::helper('secureframe')->__('Payment failed.');                                        
                    $this->_processCancel($msg,$params);
                    $event = 'fail';
                    break;
                
                case self::SECUREPAY_STATUS_CANCEL: //cancel
                    $msg = Mage::helper('secureframe')->__('Payment was canceled.');
                    $this->_processCancel($msg);
                    $event = 'cancel';
                    break;                
                
                case self::SECUREPAY_STATUS_APPROVED: //ok
                    $msg = Mage::helper('secureframe')->__('The amount has been authorized and captured by SecurePay.');
                    $this->_processSale($params, $msg);
                    $event ='success';
                    break;
            }
            
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $event = 'error';
            $msg = Mage::helper('secureframe')->__($e->getMessage());
        }
        
        return array($event,$msg);
    }

  
    /**
     * Processed order cancelation
     * @param string $msg Order history message
     * @param array  $params response parameters
     */
    protected function _processCancel($msg,$params = null)
    {
        //check already cancelled
        if ($this->_order->getState() == Mage_Sales_Model_Order::STATE_CANCELED ){
            return;
        } 
        
        //attach error message bank declines
        if ($params && $params['summarycode'] != self::SECUREPAY_STATUS_CANCEL && $params['rescode'] ){
            $msg .=  ' '.Mage::helper('secureframe')->__('Error Code: %s | Error: %s.',$params['rescode'],$params['restext']);
        }        
       
        $this->_order
                ->registerCancellation($msg, false)
                ->save();
        
    }

  
    /**
     * Processes payment confirmation, creates invoice if necessary, updates order status,
     * sends order confirmation to customer
     * @param string $msg Order history message
     */
    protected function _processSale($params, $msg)
    {        
        $payment = $this->_order->getPayment();
        $transactionId = $params['refid'] . '_' . $params["txnid"];
        //already processed
        if ($payment->getLastTransId() == $transactionId){
            return;
        }        
        
        //for surcharged amount other wise payment is marked as fraud        
        if (isset($params["baseamount"])){ 
            $amount = $params["baseamount"] / 100;
        }
        else{
            $amount = $params["amount"] / 100;
        }
        
        //surcharge amount handling       
        if (isset($params["suramount"]) && $params["suramount"] > 0){
            //value returned from SecurePay is base currency since we sent
            //base currency amounts
            $surchargeBaseAmount = $params["suramount"]/100;
            
            //we need find the order currency amount
            $surchargeAmount     = 0;
            
            $orderCurrencyCode   = $this->_order->getOrderCurrencyCode();
            $baseCurrencyCode    = $this->_order->getBaseCurrencyCode();
            $surchargeAmount     = Mage::helper('directory')->currencyConvert(
                                            $surchargeBaseAmount, 
                                            $baseCurrencyCode, 
                                            $orderCurrencyCode
                                   ); 
            $this->_order->setSecureframeBaseSurchargeAmount($surchargeBaseAmount);
            $this->_order->setSecureframeSurchargeAmount($surchargeAmount);                    
        }
        
        $payment = $this->_order->getPayment();
        $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$params);
        $payment->setPreparedMessage(Mage::helper('secureframe')->__('SecurePay SecureFrame'))
                ->setTransactionId($transactionId)
    		->setIsTransactionClosed(0)
                ->registerCaptureNotification($amount);        
    	$this->_order->save();
       
        // notify customer
        $invoice = $payment->getCreatedInvoice();        
        if ($invoice && !$this->_order->getEmailSent()) {
            $this->_order->sendNewOrderEmail()->addStatusHistoryComment(
                Mage::helper('secureframe')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
            )
            ->setIsCustomerNotified(true)
            ->save();            
        }
        
    }

    

    
    /**
     * Checking returned parameters
     * Thorws Mage_Core_Exception if error
     * 
     *
     * @return array  $params response params
     */
    protected function _validateResponseData()
    {
               
        // get request variables
        $params = $this->_responseData;
        if (empty($params)) {
            Mage::throwException('Request does not contain any elements.');
        }

        // check order ID
        if (empty($params['refid'])) {
            Mage::throwException('Missing order ID.');
        }        
        
         // load order for further validation
        $this->_order = Mage::getModel('sales/order')->loadByIncrementId($params['refid']);
        if (!$this->_order->getId()) {
            Mage::throwException('Order not found.');
        }
        
        //log return params
        Mage::helper('secureframe')->debug('response',$params,$this->_order->getStoreId());
        
        //check current order session the reutrn url not the callback url
        if (!$this->isCallback()){
             if($this->_getCheckout()->getLastRealOrderId() != $params['refid']){
                 Mage::throwException('Invalid order ID.');
             }
        }
            
        $paymentMethod = $this->_order->getPayment()->getMethodInstance();
        
        if ($paymentMethod->getCode() != 'secureframe' ) {
            Mage::throwException('Unknown payment method.');
        }

         $active = Mage::getStoreConfig(
                            SecurePay_SecureFrame_Helper_Data::XML_PATH_ACTIVE,
                    $this->_order->getStoreId());
        
        if (!$active){
            Mage::throwException('Method "%s" is not available.',$paymentMethod->getTitle());
        }
        
                
        // check payment status
        if (empty($params['summarycode'])) {
            Mage::throwException('Unknown payment status.');
        }

        // check transaction signature
        if (empty($params['fingerprint'])) {
            Mage::throwException('Invalid transaction fingerprint.');
        }

        $txnPassword = Mage::getStoreConfig(
                            SecurePay_SecureFrame_Helper_Data::XML_PATH_TRANSACTION_PASSWORD,
                    $this->_order->getStoreId()
                );

        $amount = $params['amount'];

        /* gotcha: when performing a 3D txn, if declined, the amount seems to come back decimal formatted; 
         * this will undoubtedly break fingerprint matching!
         */
        $params['amount'] = (strrpos($amount, ".") === false) ? $amount : ($amount*100); 

        $fingerprint = sha1($params["merchant"] . '|' . $txnPassword  . 
                             '|' . $params['refid'] . '|' . $params['amount'] . '|' . $params["timestamp"] .
                                    '|' . $params["summarycode"]
                       );

        if ($fingerprint != $params['fingerprint']){
             Mage::throwException('Fingerprint is not valid.');
        }
                                   
        
        return $params;
    }
            
}
