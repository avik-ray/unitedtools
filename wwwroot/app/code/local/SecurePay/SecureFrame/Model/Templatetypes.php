<?php
/**
 * Source class for template system configuration
 */
class SecurePay_SecureFrame_Model_Templatetypes
{
  public function toOptionArray()
  {
          
    $options = array(
        array('value'=>'iframe', 'label'=>'IFrame'),
        array('value'=>'default', 'label'=>'Default'),        
    );
    return $options;
  }
}

