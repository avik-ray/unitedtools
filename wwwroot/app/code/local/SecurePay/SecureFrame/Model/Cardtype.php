<?php

/**
 * Card types available in Payment Configuration
 */

class SecurePay_SecureFrame_Model_Cardtype
{
  /**
   * addtional PAYPAL type triggers Paypal payment via SecurePay
   * 
   * @return array
   */
  public function toOptionArray()
  {
     //array('value'=>'PAYPAL', 'label'=>'PAYPAL')
    $options = array(
        array('value'=>'VISA', 'label'=>'Visa'),
        array('value'=>'AMEX', 'label'=>'American Express'),
        array('value'=>'MASTERCARD', 'label'=>'MasterCard'),
        array('value'=>'DINERS', 'label'=>'Diners'),
        array('value'=>'JCB', 'label'=>'JCB')
    );
    return $options;

  }
}
