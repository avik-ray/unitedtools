<?php
class SecurePay_SecureFrame_Block_Surcharge  extends Mage_Sales_Block_Order_Totals{
    
    public function initTotals(){
        $order = $this->getParentBlock()->getOrder();
        if($order->getSecureframeBaseSurchargeAmount() > 0){
            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code' => 'secureframe_surcharge',
                'value' => $order->getSecureframeSurchargeAmount(),
                'base_value' => $order->getSecureframeBaseSurchargeAmount(),
                'label' => Mage::helper('secureframe')->__('Added Surchage (not included in Grand Total)'),
                'area'      => 'footer'
            )),'grand_total');
        }
    }
}