<?php
class SecurePay_SecureFrame_Block_Onestepcheckout_Checkout extends Idev_OneStepCheckout_Block_Checkout {
        
    private $_redirectUrl;
    
    public function handlePost($controller){
        $result = array();         
        $result['redirect'] = Mage::getUrl('onestepcheckout/index/index',array('_secure'=>true));
         
        try {
            if ($this->_validation()){
                $this->_init();
                $this->_handlePostData();
                
                if (!$this->errorsExist()){
                   
                    //secure payment                        
                    $orderId = $this->getOnepage()->getCheckout()->getLastOrderId();
                    $order   = Mage::getModel('sales/order')->load($orderId);

                    if ($order->getPayment()->getMethodInstance()->getCode() == 
                              SecurePay_SecureFrame_Model_Standard::SECUREPAY_SECUREFRAME_METHOD
                    ){    
                        unset($result['redirect']);
                        $result['secureframe'] = true;
                        $result['update_section'] = array(
                            'name' => 'secureframe_payment',
                            'html' => $controller->getSecureframePaymentMethodsHtml()
                        );
                    }
                    //other payment
                    else{
                        $result['redirect'] = $this->_redirectUrl; 
                    }
                    
                }
                else{                    
                    $result['redirect'] = Mage::getUrl('onestepcheckout/index/index',array('_secure'=>true));
                    $this->getOnepage()->getCheckout()->setSecureFrameErrors($this->formErrors);
                }                
            }else {
                $result['redirect'] = Mage::getUrl('checkout/cart');
            }
        } catch (Exception $ex) {
            Mage::logException($ex);
            $result['error'] = true;
            $result['error_messages'] = $ex->getMessage();
        }  
        return $result;
    }
    
    public function setErrors(){
        if (!$this->errorsExist()){
            $errors =  $this->getOnepage()->getCheckout()->getSecureFrameErrors();
            if (!empty($errors)){
                $this->formErrors = $errors;
            }
            $this->getOnepage()->getCheckout()->unsSecureFrameErrors();
        }
    } 
    
    public function errorsExist(){
        if (is_array($this->formErrors)){
            foreach($this->formErrors as $error){
                if (!empty($error) || (is_array($error) && count($error)) ){
                    return true;
                }
            }
        }
        return false;
    }
    
    protected function _init(){
        $this->getQuote()->setIsMultiShipping(false);

        $this->email = false;
        $this->customer_after_place_order = false;

        $this->_loadConfig();

        if($this->_isLoggedIn())    {
            $helper = Mage::helper('customer');
            $customer = $helper->getCustomer();
            $this->email = $customer->getEmail();
        }
    }
    
    protected function _validation(){
        
        $routeName = $this->getRequest()->getRouteName();
        
        if (!Mage::helper('onestepcheckout')->isRewriteCheckoutLinksEnabled() && $routeName != 'onestepcheckout'){
            return false;
        }

        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            return false;
            
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message');
            Mage::getSingleton('checkout/session')->addError($error);            
            return false;
        }
        
        return true;
    }
    
     protected function _saveOrder()
     {
        // Hack to fix weird Magento payment behaviour
        $payment = $this->getRequest()->getPost('payment', false);
        if($payment) {
            $payment = $this->filterPaymentData($payment);
            $this->getOnepage()->getQuote()->getPayment()->importData($payment);

            $ccSaveAllowedMethods = array('ccsave');
            $method = $this->getOnepage()->getQuote()->getPayment()->getMethodInstance();

            if(in_array($method->getCode(), $ccSaveAllowedMethods)){
                $info = $method->getInfoInstance();
                $info->setCcNumberEnc($info->encrypt($info->getCcNumber()));
            }

        }

        try {            
            if(!$this->getOnepage()->getQuote()->isVirtual() && !$this->getOnepage()->getQuote()->getShippingAddress()->getShippingDescription()){
                Mage::throwException(Mage::helper('checkout')->__('Please choose a shipping method'));
            }

            if(!Mage::helper('customer')->isLoggedIn()){
                $this->getOnepage()->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
            }
           $order = $this->getOnepage()->saveOrder();
        } catch(Exception $e)   {
            //need to activate
            $this->getOnepage()->getQuote()->setIsActive(true);
            //need to recalculate
            $this->getOnepage()->getQuote()->getShippingAddress()->setCollectShippingRates(true)->collectTotals();
            $error = $e->getMessage();
            $this->formErrors['unknown_source_error'] = $error;
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $error);
            return;
            //die('Error: ' . $e->getMessage());
        }

        $this->afterPlaceOrder();

        $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();

        if($redirectUrl)    {
            $this->_redirectUrl = $redirectUrl;
        } else {
            $this->getOnepage()->getQuote()->setIsActive(false);
            $this->getOnepage()->getQuote()->save();
            $this->_redirectUrl = $this->getUrl('checkout/onepage/success');
            //$this->_redirect('checkout/onepage/success', array('_secure'=>true));
        }
        return;
    }
    
}