<?php
/**
 * Block used in redirect.phtml provides nescessary variables to post to Securepay
 */
class SecurePay_SecureFrame_Block_Iframe extends Mage_Core_Block_Template {
    
    /**
     *
     * @var SecurePay_SecureFrame_Model_Standard 
     */
    private $_paymentMethod = null;
    
    /**
     * 
     * @return SecurePay_SecureFrame_Model_Standard
     */
    protected function _getPaymentMethod() {
        if (is_null($this->_paymentMethod))
            $this->_paymentMethod = Mage::getSingleton('secureframe/standard');
        return $this->_paymentMethod;
    }
    
    /**
     * SecurePay url
     * @return string
     */
    public function getActionUrl(){
        return $this->_getPaymentMethod()->getActionUrl();
    }
    
    /**
     * returns request paramters needed to post to securepay 
     * @return array
     */
    public function getRequestParameters(){
        $order = Mage::getModel('sales/order')
                ->loadByIncrementId(Mage::getSingleton('checkout/session')
                ->getLastRealOrderId());                
        return $this->_getPaymentMethod()->getRequestParameters($order);
    }
   
        
        
}
