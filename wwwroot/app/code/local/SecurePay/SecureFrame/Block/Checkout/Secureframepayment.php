<?php 
class SecurePay_SecureFrame_Block_Checkout_Secureframepayment extends Mage_Checkout_Block_Onepage_Abstract
{
    protected function _construct()
    {
        $storeId = Mage::app()->getStore()->getId();
        $securepayStepTitle = Mage::getStoreConfig(SecurePay_SecureFrame_Helper_Data::XML_PATH_TITLE_STEP,$storeId);
        $this->getCheckout()->setStepData('secureframe_payment', array(
            'label'     => Mage::helper('secureframe')->__($securepayStepTitle),
            'is_show'   => true
        ));
        parent::_construct();
        
    }
       
    
    public function isShow(){
        return Mage::getStoreConfig(SecurePay_SecureFrame_Helper_Data::XML_PATH_ACTIVE);                    
    }
}
