<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute('order', 'secureframe_base_surcharge_amount', array('type' => 'decimal', 'visible' => false, 'default' => 0));
$installer->addAttribute('order', 'secureframe_surcharge_amount', array('type' => 'decimal', 'visible' => false, 'default' => 0));
$installer->endSetup();
