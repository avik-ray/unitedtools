<?php
/**
 * All callback Urls from SecurePapy are processed here
 * 
 */
class SecurePay_SecureFrame_PaymentController extends Mage_Core_Controller_Front_Action {
    
    /**
     *
     * @var SecurePay_SecureFrame_Model_Standard
     */
    private $_paymentMethod = null;
    
    /**
     * 
     * @return SecurePay_SecureFrame_Model_Standard
     */
    protected function _getPaymentMethod() {
        if (is_null($this->_paymentMethod))
            $this->_paymentMethod = Mage::getSingleton('secureframe/standard');
        return $this->_paymentMethod;
    }
    
    //Build secureframe request and display secureframe to customer.        
    public function redirectAction() {                        
        $session = Mage::getSingleton('checkout/session');
        $session->setSecureframeQuoteId($session->getQuoteId());        
        $this->loadLayout();                
        $this->renderLayout();
        $session->unsQuoteId();
        
    }
	
    /**
     * SecurePay call-back url
     * @return type
     */
    public function callbackAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }
        try {
            
            $processor = Mage::getModel('secureframe/processor')
                                ->setReponseData($this->getRequest()->getPost());
            
            $processor->setCallback();
            
            $processor->process();
                    
        } catch (Exception $e) {
            Mage::logException($e);
        }
        
        die;
    }
     
    /**
     * action taken after Customer is redirected to merchant site.
     * @return type
     */ 
    public function returnAction(){
        
         $session = Mage::getSingleton('checkout/session');      

         if ($session->getSecureframeQuoteId(true)) {   
            $session->setQuoteId($session->getSecureframeQuoteId(true));   
         }   
               
         $processor = Mage::getModel('secureframe/processor')
            ->setResponseData($this->getRequest()->getPost());                                   
         $event = $processor->process();
                  
         switch($event[0]){
             case 'fail'   :
             case 'error'  :
                 $session->setErrorMessage($event[1]);
                 $this->_redirect('checkout/onepage/failure');
                 return;
             case 'cancel' :
                 $session->addWarning($event[1]);
                 $this->_redirect('checkout/cart');
                 return;
             case 'success':
                 $session->getQuote()->setIsActive(false)->save();
                 Mage::getSingleton('checkout/type_onepage')->getQuote()->save();
                 $this->_redirect('checkout/onepage/success', array('_secure'=>true));
                 return;
         }         
         $this->_redirect('checkout/cart');
     }
}
