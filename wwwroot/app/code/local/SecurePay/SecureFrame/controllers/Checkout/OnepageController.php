<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class SecurePay_SecureFrame_Checkout_OnepageController extends Mage_Checkout_OnepageController{ 
    
    /**
     *   Create order action
    */
    public function saveOrderAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = array();
        try {
            if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }
            if ($data = $this->getRequest()->getPost('payment', false)) {
                $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }
            $this->getOnepage()->saveOrder();
            $result   =  $this->getOrderResult($result);

        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if( !empty($message) ) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'payment';
            $result['update_section'] = array(
                'name' => 'payment-method',
                'html' => $this->_getPaymentMethodsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            if ($gotoSection = $this->getOnepage()->getCheckout()->getGotoSection()) {
                $result['goto_section'] = $gotoSection;
                $this->getOnepage()->getCheckout()->setGotoSection(null);
            }

            if ($updateSection = $this->getOnepage()->getCheckout()->getUpdateSection()) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepage()->getCheckout()->setUpdateSection(null);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $dataPayment = $this->getRequest()->getPost('payment', false);
        if (isset($dataPayment) && is_array($dataPayment) && $dataPayment['method'] =='secureframe') {
        }
        else{
            $this->getOnepage()->getQuote()->save();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    private function getOrderResult($result){
        
        $orderId = $this->getOnepage()->getCheckout()->getLastOrderId();
        $order   = Mage::getModel('sales/order')->load($orderId);
        
        if ($order->getPayment()->getMethodInstance()->getCode() == 
                SecurePay_SecureFrame_Model_Standard::SECUREPAY_SECUREFRAME_METHOD
         ){            
            $result['secureframe']    = true;
            $result['goto_section']   = 'secureframe_payment';
            $result['update_section'] = array(
                'name' => 'secureframe_payment',
                'html' => $this->getSecureframePaymentMethodsHtml()
            );
        }
        else {
            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;
            if ($redirectUrl){
                $result['redirect']  = $redirectUrl;
            }
            
        }        
        return $result;
    }
   
    public function onestepcheckoutAction(){
        if( 
             !(  
                Mage::helper('core')->isModuleEnabled('Idev_OneStepCheckout') &&
                    Mage::helper('onestepcheckout')->isRewriteCheckoutLinksEnabled()
              )
         ){
            $this->getResponse()
                 ->setHeader('HTTP/1.1', '403 Session Expired')
                 ->setHeader('Login-Required', 'true')
                 ->sendResponse();
            return ;
        }
        $block = new SecurePay_SecureFrame_Block_Onestepcheckout_Checkout();
        $result = $block->handlePost($this);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    /**
     * Get payment method step html
     *
     * @return string
     */
    public function getSecureframePaymentMethodsHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_secureframe_paymentmethod');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;
    }
    
}

