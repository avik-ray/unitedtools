<?php
class SecurePay_SecureFrame_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    /**
     * config xml constants
     */
    const XML_PATH_TRANSACTION_PASSWORD        = 'payment/secureframe/transaction_password';
    const XML_PATH_ACTIVE                      = 'payment/secureframe/active';
    const XML_PATH_DEBUG                       = 'payment/secureframe/debug';
    const XML_PATH_TITLE_STEP                  = 'payment/secureframe/title_step';

    
    /**
     * this is used in processer class and payment method class to 
     * log request and reposnses from SecurePay
     * 
     * @param string $title
     * @param array|object $data
     * @param int $storeId
     */
    public function debug($title,$data,$storeId)
    {
        $debug = Mage::getStoreConfig(
                            SecurePay_SecureFrame_Helper_Data::XML_PATH_DEBUG,
                    $storeId);
        if ($debug) {
            Mage::getModel('core/log_adapter', 'payment_secureframe.log')->log($title.' : '.print_r($data,true));
        }        
    }
}