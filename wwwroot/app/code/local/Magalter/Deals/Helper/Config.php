<?php

class Magalter_Deals_Helper_Config extends Mage_Core_Helper_Abstract {
    
    const URL_REWRITES = 'magalter_deals/configuration/url_rewrites';
    
    const APP_ENABLED = 'magalter_deals/configuration/enable';
    
    const APP_DISABLED_ADVANCED = 'advanced/modules_disable_output/Magalter_Deals';
    
    const ADD_TO_CART = 'magalter_deals/configuration/add_to';
    
    const PRODUCT_PRICE = 'magalter_deals/configuration/product_price';
    
    const DISPLAY_FUTURE = 'magalter_deals/configuration/display_future';
    
    const COUNTER = 'magalter_deals/configuration/livecounter';    
    
    
    public static function getConfig( $path, $storeId = null ) {        
        
        if(!$storeId) {
            
            $storeId = Mage::app()->getStore()->getId();
        }
        
        return Mage::getStoreConfig( $path );        
        
    }
    
    public static function shouldRenderApp() {
        
        return self::getConfig(self::APP_ENABLED) && !self::getConfig(self::APP_DISABLED_ADVANCED);
        
        
    }
   
}