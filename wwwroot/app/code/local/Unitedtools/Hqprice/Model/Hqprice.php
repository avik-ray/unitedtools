<?php

class Unitedtools_Hqprice_Model_Hqprice extends Mage_Catalog_Model_Product_Type_Price
{

    /**
     * Get base price with apply Group, Tier, Special, Hq prises
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float|null $qty
     *
     * @return float
     */
    public function getBasePrice($product, $qty = null)
    {
        $price = (float)$product->getPrice();
        $finalPrice = min($this->_applyGroupPrice($product, $price), 
                $this->_applyTierPrice($product, $qty, $price),
                $this->_applySpecialPrice($product, $price));
        return $this->_applyHqPrice($product, $finalPrice);
    }

    
    /**
     * Apply hq price for product if not return price that was before
     *
     * @param   Mage_Catalog_Model_Product $product
     * @param   float $finalPrice
     * @return  float
     */
    protected function _applyHqPrice($product, $finalPrice)
    {
        return $this->calculateHqPrice($finalPrice, $product->getHqPrice(), 
            $product->getHqPriceFrom(), $product->getHqPriceTo(), 
            $product->getStore());
    }


    /**
     * Calculate and apply hq price
     *
     * @param float $finalPrice
     * @param float $hqPrice
     * @param string $hqPriceFrom
     * @param string $hqPriceTo
     * @param mixed $store
     * @return float
     */
    public static function calculateHqPrice($finalPrice, $hqPrice, $hqPriceFrom, $hqPriceTo,
            $store = null)
    {
        if (!is_null($hqPrice) && $hqPrice != false) {
            if (Mage::app()->getLocale()
                    ->isStoreDateInInterval($store, $hqPriceFrom, $hqPriceTo)) {
                $finalPrice = $hqPrice;
            }
        }
        return $finalPrice;
    }
}
