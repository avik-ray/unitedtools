<?php

class Unitedtools_Hqprice_Model_Indexer extends Mage_Catalog_Model_Product_Indexer_Price
{
    /**
     * Retrieve attribute list has an effect on product price
     *
     * @return array
     */
    protected function _getDependentAttributes()
    {
        return array(
            'price',
            'special_price',
            'special_from_date',
            'special_to_date',
            'hq_price',
            'hq_from_date',
            'hq_to_date',
            'tax_class_id',
            'status',
            'required_options',
            'force_reindex_required'
        );
    }
}
