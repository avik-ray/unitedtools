<?php 
require_once 'Mage/Customer/controllers/AccountController.php';

class Unitedtools_Disableregister_AccountController extends Mage_Customer_AccountController
{
    public function createAction()
    {
        if (Mage::getStoreConfig('customer/disableregister/disableregister'))
            $this->_redirect('*/*');
        else
            parent::createAction();
    }

    public function createPostAction()
    {
        if (Mage::getStoreConfig('customer/disableregister/disableregister'))
            $this->_redirect('*/*');
        else
            parent::createAction();
    }
}
