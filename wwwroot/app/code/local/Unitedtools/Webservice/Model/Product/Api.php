<?php

class Unitedtools_Webservice_Model_Product_Api extends Mage_Catalog_Model_Product_Api
{
    /**
     * Retrieve list of products with basic info (id, sku, type, set, name)
     *
     * @param null|object|array $filters
     * @param string|int $store
     * @return array
     */
    public function items($filters = null, $store = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
        ->addStoreFilter($this->_getStoreId($store))
        ->addAttributeToSelect('name')
        ->addAttributeToSelect('price')
        ->addAttributeToSelect('special_price')
        ->addAttributeToSelect('created_at')
        ->addAttributeToSelect('updated_at');
    
        /** @var $apiHelper Mage_Api_Helper_Data */
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters, $this->_filtersMap);
        try {
            foreach ($filters as $field => $value) {
                $collection->addFieldToFilter($field, $value);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        $result = array();
        foreach ($collection as $product) {
            $result[] = array(
                    'product_id' => $product->getId(),
                    'sku'        => $product->getSku(),
                    'name'       => $product->getName(),
                    'set'        => $product->getAttributeSetId(),
                    'type'       => $product->getTypeId(),
                    'category_ids' => $product->getCategoryIds(),
                    'website_ids'  => $product->getWebsiteIds(),
                    'price'      => $product->getPrice(),
                    'created_at' => $product->getCreatedAt(),
                    'updated_at' => $product->getUpdatedAt(),
                    'special_price' => $product->getSpecialPrice(),
            );
        }
        return $result;
    }
}
