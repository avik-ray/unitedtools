<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin edit tabs
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('storelocation_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('unitedtools_localproducts')->__('Store'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_storelocation',
            array(
                'label'   => Mage::helper('unitedtools_localproducts')->__('Store'),
                'title'   => Mage::helper('unitedtools_localproducts')->__('Store'),
                'content' => $this->getLayout()->createBlock(
                    'unitedtools_localproducts/adminhtml_storelocation_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_storelocation',
                array(
                    'label'   => Mage::helper('unitedtools_localproducts')->__('Store views'),
                    'title'   => Mage::helper('unitedtools_localproducts')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'unitedtools_localproducts/adminhtml_storelocation_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve store entity
     *
     * @access public
     * @return Unitedtools_Localproducts_Model_Storelocation
     * @author Ultimate Module Creator
     */
    public function getStorelocation()
    {
        return Mage::registry('current_storelocation');
    }
}
