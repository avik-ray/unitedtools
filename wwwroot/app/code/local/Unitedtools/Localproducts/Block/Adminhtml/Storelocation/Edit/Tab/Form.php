<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store edit form tab
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('storelocation_');
        $form->setFieldNameSuffix('storelocation');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'storelocation_form',
            array('legend' => Mage::helper('unitedtools_localproducts')->__('Store'))
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('unitedtools_localproducts')->__('Name'),
                'name'  => 'name',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'base_url',
            'text',
            array(
                'label' => Mage::helper('unitedtools_localproducts')->__('Base URL'),
                'name'  => 'base_url',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'latitude',
            'text',
            array(
                'label' => Mage::helper('unitedtools_localproducts')->__('Latitude'),
                'name'  => 'latitude',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'longitude',
            'text',
            array(
                'label' => Mage::helper('unitedtools_localproducts')->__('Longitude'),
                'name'  => 'longitude',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
        $fieldset->addField(
            'product_lookup',
            'select',
            array(
                'label'  => Mage::helper('unitedtools_localproducts')->__('Look Up Products in Store'),
                'name'   => 'product_lookup',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('unitedtools_localproducts')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('unitedtools_localproducts')->__('No, redirect to base url'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'toolmart_lookup',
            'select',
            array(
                'label'  => Mage::helper('unitedtools_localproducts')->__('Use Toolmart-Style Lookup'),
                'name'   => 'toolmart_lookup',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('unitedtools_localproducts')->__('Yes'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('unitedtools_localproducts')->__('No'),
                    ),
                ),
            )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('unitedtools_localproducts')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('unitedtools_localproducts')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('unitedtools_localproducts')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_storelocation')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_storelocation')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getStorelocationData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getStorelocationData());
            Mage::getSingleton('adminhtml/session')->setStorelocationData(null);
        } elseif (Mage::registry('current_storelocation')) {
            $formValues = array_merge($formValues, Mage::registry('current_storelocation')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
