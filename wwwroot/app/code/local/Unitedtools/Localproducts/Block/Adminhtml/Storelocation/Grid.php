<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin grid block
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('storelocationGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('unitedtools_localproducts/storelocation')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('unitedtools_localproducts')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('unitedtools_localproducts')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('unitedtools_localproducts')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('unitedtools_localproducts')->__('Enabled'),
                    '0' => Mage::helper('unitedtools_localproducts')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'base_url',
            array(
                'header' => Mage::helper('unitedtools_localproducts')->__('Base URL'),
                'index'  => 'base_url',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'latitude',
            array(
                'header' => Mage::helper('unitedtools_localproducts')->__('Latitude'),
                'index'  => 'latitude',
                'type'=> 'number',

            )
        );
        $this->addColumn(
            'longitude',
            array(
                'header' => Mage::helper('unitedtools_localproducts')->__('Longitude'),
                'index'  => 'longitude',
                'type'=> 'number',

            )
        );
        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('unitedtools_localproducts')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('unitedtools_localproducts')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('unitedtools_localproducts')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('unitedtools_localproducts')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('unitedtools_localproducts')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('unitedtools_localproducts')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('storelocation');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('unitedtools_localproducts')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('unitedtools_localproducts')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('unitedtools_localproducts')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('unitedtools_localproducts')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('unitedtools_localproducts')->__('Enabled'),
                            '0' => Mage::helper('unitedtools_localproducts')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Unitedtools_Localproducts_Model_Storelocation
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param Unitedtools_Localproducts_Model_Resource_Storelocation_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
