<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin edit form
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Block_Adminhtml_Storelocation_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'unitedtools_localproducts';
        $this->_controller = 'adminhtml_storelocation';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('unitedtools_localproducts')->__('Save Store')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('unitedtools_localproducts')->__('Delete Store')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('unitedtools_localproducts')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_storelocation') && Mage::registry('current_storelocation')->getId()) {
            return Mage::helper('unitedtools_localproducts')->__(
                "Edit Store '%s'",
                $this->escapeHtml(Mage::registry('current_storelocation')->getName())
            );
        } else {
            return Mage::helper('unitedtools_localproducts')->__('Add Store');
        }
    }
}
