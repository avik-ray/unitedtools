<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store abstract REST API handler model
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
abstract class Unitedtools_Localproducts_Model_Api2_Storelocation_Rest extends Unitedtools_Localproducts_Model_Api2_Storelocation
{
    /**
     * current store
     */
    protected $_storelocation;

    /**
     * retrieve entity
     *
     * @access protected
     * @return array|mixed
     * @author Ultimate Module Creator
     */
    protected function _retrieve() {
        $storelocation = $this->_getStorelocation();
        $this->_prepareStorelocationForResponse($storelocation);
        return $storelocation->getData();
    }

    /**
     * get collection
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _retrieveCollection() {
        $collection = Mage::getResourceModel('unitedtools_localproducts/storelocation_collection');
        $entityOnlyAttributes = $this->getEntityOnlyAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ
        );
        $availableAttributes = array_keys($this->getAvailableAttributes(
            $this->getUserType(),
            Mage_Api2_Model_Resource::OPERATION_ATTRIBUTE_READ)
        );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $store = $this->_getStore();
        $collection->addStoreFilter($store->getId());
        $this->_applyCollectionModifiers($collection);
        $storelocations = $collection->load();
        $storelocations->walk('afterLoad');
        foreach ($storelocations as $storelocation) {
            $this->_setStorelocation($storelocation);
            $this->_prepareStorelocationForResponse($storelocation);
        }
        $storelocationsArray = $storelocations->toArray();
        $storelocationsArray = $storelocationsArray['items'];

        return $storelocationsArray;
    }

    /**
     * prepare store for response
     *
     * @access protected
     * @param Unitedtools_Localproducts_Model_Storelocation $storelocation
     * @author Ultimate Module Creator
     */
    protected function _prepareStorelocationForResponse(Unitedtools_Localproducts_Model_Storelocation $storelocation) {
        $storelocationData = $storelocation->getData();
    }

    /**
     * create store
     *
     * @access protected
     * @param array $data
     * @return string|void
     * @author Ultimate Module Creator
     */
    protected function _create(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * update store
     *
     * @access protected
     * @param array $data
     * @author Ultimate Module Creator
     */
    protected function _update(array $data) {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete store
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    protected function _delete() {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * delete current store
     *
     * @access protected
     * @param Unitedtools_Localproducts_Model_Storelocation $storelocation
     * @author Ultimate Module Creator
     */
    protected function _setStorelocation(Unitedtools_Localproducts_Model_Storelocation $storelocation) {
        $this->_storelocation = $storelocation;
    }

    /**
     * get current store
     *
     * @access protected
     * @return Unitedtools_Localproducts_Model_Storelocation
     * @author Ultimate Module Creator
     */
    protected function _getStorelocation() {
        if (is_null($this->_storelocation)) {
            $storelocationId = $this->getRequest()->getParam('id');
            $storelocation = Mage::getModel('unitedtools_localproducts/storelocation');
            $storelocation->load($storelocationId);
            if (!($storelocation->getId())) {
                $this->_critical(self::RESOURCE_NOT_FOUND);
            }
            if ($this->_getStore()->getId()) {
                $isValidStore = count(array_intersect(array(0, $this->_getStore()->getId()), $storelocation->getStoreId()));
                if (!$isValidStore) {
                    $this->_critical(self::RESOURCE_NOT_FOUND);
                }
            }
            $this->_storelocation = $storelocation;
        }
        return $this->_storelocation;
    }
}
