<?php

class Unitedtools_Localproducts_Model_Observer {
    protected function greatCircleDistance($latitudeFrom, $longitudeFrom, 
        $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    private function get_locations($lat, $long) {
        $locations = Mage::getModel('unitedtools_localproducts/storelocation')
            ->getCollection()
            ->addFieldToFilter('status', 1)
            ->getItems();

        # Sort locations by distance
        # Mage::log("Sort vs: $lat, $long");
        usort($locations, function($a, $b) use ($lat, $long) {
            $da = $this->greatCircleDistance($lat, $long,
                $a->getData('latitude'), $a->getData('longitude'));
            $db = $this->greatCircleDistance($lat, $long, 
                $b->getData('latitude'), $b->getData('longitude'));
            return $da < $db ? -1 : 1;
        });

        return $locations;
    }

    public function redirectToLocalStore($observer) {
        $lat = Mage::app()->getRequest()->getParam('lat');
        $long = Mage::app()->getRequest()->getParam('long');
        $postalCode = Mage::app()->getRequest()->getParam('postalcode');
        if (($lat && $long) || $postalCode) {
            if ($postalCode) {
                $codes = Mage::getModel('unitedtools_localproducts/postcode')
                    ->getCollection()->addFieldToFilter('postcode', $postalCode);
                if ($codes->getSize() > 0) {
                    $pc = $codes->getFirstItem();
                    $lat = $pc->getData('latitude');
                    $long = $pc->getData('longitude');
                } else {
                    // Invalid postalCode
                    Mage::register('unitedtools_localproducts_postcode_not_found', true);
                    return;
                }
            }

            $locations = $this->get_locations($lat, $long);

            $product = $observer->getEvent()->getProduct();

            # Check the locations and redirect to the closest store
            foreach ($locations as $location) {
                # Mage::log($location->getData('latitude') . ',' . $location->getData('longitude') . ' : ' . $location->getName());
                $base_url = $location->getData('base_url');
                if ($location->getData('product_lookup') == 0) {
                    Mage::app()->getResponse()->setRedirect($base_url)->sendResponse();
                    exit(0);
                } else {
                    $sku= rawurlencode($product->getData('sku'));
                    $lookup_url = $base_url .  '/localproducts/productexists/?sku=' . urlencode($sku);
                    if ($location->getData('toolmart_lookup') == 1) {
                        $lookup_url .= '&toolmart=1';
                    }

                    $product_url = file_get_contents($lookup_url);
                    if ($product_url !== FALSE && $product_url != ''
                            && count($product_url) < 1000
                            && substr($product_url, 0, 4) == 'http' ) {
                        Mage::app()->getResponse()->setRedirect($product_url)->sendResponse();
                        exit(0);
                    }
                }
            }

            # product not found
            Mage::register('unitedtools_localproducts_product_not_found', true);
        }
    }

    public function changeLayoutEvent($observer) {
        $action = $observer->getEvent()->getAction();
        if ($action instanceof Mage_Catalog_ProductController && $action->getRequest()->getRequestedActionName() == 'view') {
            if (Mage::getStoreConfig('unitedtools_localproducts/localproducts/enable')) {
                $observer->getEvent()->getLayout()->getUpdate()
                    ->addHandle('REDIRECT_TO_LOCAL_STORE');
            }
        }
    }
}
