<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
class Unitedtools_Localproducts_Model_Storelocation_Api extends Mage_Api_Model_Resource_Abstract
{


    /**
     * init store
     *
     * @access protected
     * @param $storelocationId
     * @return Unitedtools_Localproducts_Model_Storelocation
     * @author      Ultimate Module Creator
     */
    protected function _initStorelocation($storelocationId)
    {
        $storelocation = Mage::getModel('unitedtools_localproducts/storelocation')->load($storelocationId);
        if (!$storelocation->getId()) {
            $this->_fault('storelocation_not_exists');
        }
        return $storelocation;
    }

    /**
     * get stores
     *
     * @access public
     * @param mixed $filters
     * @return array
     * @author Ultimate Module Creator
     */
    public function items($filters = null)
    {
        $collection = Mage::getModel('unitedtools_localproducts/storelocation')->getCollection();
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters);
        try {
            foreach ($filters as $field => $value) {
                $collection->addFieldToFilter($field, $value);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        $result = array();
        foreach ($collection as $storelocation) {
            $result[] = $this->_getApiData($storelocation);
        }
        return $result;
    }

    /**
     * Add store
     *
     * @access public
     * @param array $data
     * @return array
     * @author Ultimate Module Creator
     */
    public function add($data)
    {
        try {
            if (is_null($data)) {
                throw new Exception(Mage::helper('unitedtools_localproducts')->__("Data cannot be null"));
            }
            $data = (array)$data;
            $storelocation = Mage::getModel('unitedtools_localproducts/storelocation')
                ->setData((array)$data)
                ->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
        return $storelocation->getId();
    }

    /**
     * Change existing store information
     *
     * @access public
     * @param int $storelocationId
     * @param array $data
     * @return bool
     * @author Ultimate Module Creator
     */
    public function update($storelocationId, $data)
    {
        $storelocation = $this->_initStorelocation($storelocationId);
        try {
            $data = (array)$data;
            $storelocation->addData($data);
            $storelocation->save();
        }
        catch (Mage_Core_Exception $e) {
            $this->_fault('save_error', $e->getMessage());
        }

        return true;
    }

    /**
     * remove store
     *
     * @access public
     * @param int $storelocationId
     * @return bool
     * @author Ultimate Module Creator
     */
    public function remove($storelocationId)
    {
        $storelocation = $this->_initStorelocation($storelocationId);
        try {
            $storelocation->delete();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('remove_error', $e->getMessage());
        }
        return true;
    }

    /**
     * get info
     *
     * @access public
     * @param int $storelocationId
     * @return array
     * @author Ultimate Module Creator
     */
    public function info($storelocationId)
    {
        $result = array();
        $storelocation = $this->_initStorelocation($storelocationId);
        $result = $this->_getApiData($storelocation);
        return $result;
    }

    /**
     * get data for api
     *
     * @access protected
     * @param Unitedtools_Localproducts_Model_Storelocation $storelocation
     * @return array()
     * @author Ultimate Module Creator
     */
    protected function _getApiData(Unitedtools_Localproducts_Model_Storelocation $storelocation)
    {
        return $storelocation->getData();
    }
}
