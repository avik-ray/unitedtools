<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
class Unitedtools_Localproducts_Model_Storelocation_Api_V2 extends Unitedtools_Localproducts_Model_Storelocation_Api
{
    /**
     * Store info
     *
     * @access public
     * @param int $storelocationId
     * @return object
     * @author Ultimate Module Creator
     */
    public function info($storelocationId)
    {
        $result = parent::info($storelocationId);
        $result = Mage::helper('api')->wsiArrayPacker($result);
        return $result;
    }
}
