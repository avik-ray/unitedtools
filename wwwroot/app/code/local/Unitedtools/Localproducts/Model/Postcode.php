<?php
class Unitedtools_Localproducts_Model_Postcode extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'unitedtools_localproducts_postcode';
    const CACHE_TAG = 'unitedtools_localproducts_postcode';

    protected $_eventPrefix = 'unitedtools_localproducts_postcode';
    protected $_eventObject = 'postcode';

    public function _construct()
    {
        parent::_construct();
        $this->_init('unitedtools_localproducts/postcode');
    }
}
