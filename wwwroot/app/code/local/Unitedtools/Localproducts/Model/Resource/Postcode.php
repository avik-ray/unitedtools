<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 */
/**
 * Postcode resource model
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 */
class Unitedtools_Localproducts_Model_Resource_Postcode extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     */
    public function _construct()
    {
        $this->_init('unitedtools_localproducts/postcode', 'entity_id');
    }
}

