<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 */
/**
 * Postcode resource model
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 */
class Unitedtools_Localproducts_Model_Resource_Postcode_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * constructor
     *
     * @access public
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('unitedtools_localproducts/postcode');
    }
}
