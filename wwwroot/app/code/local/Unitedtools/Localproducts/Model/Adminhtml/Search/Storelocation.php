<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Model_Adminhtml_Search_Storelocation extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Unitedtools_Localproducts_Model_Adminhtml_Search_Storelocation
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('unitedtools_localproducts/storelocation_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $storelocation) {
            $arr[] = array(
                'id'          => 'storelocation/1/'.$storelocation->getId(),
                'type'        => Mage::helper('unitedtools_localproducts')->__('Store'),
                'name'        => $storelocation->getName(),
                'description' => $storelocation->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/localproducts_storelocation/edit',
                    array('id'=>$storelocation->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
