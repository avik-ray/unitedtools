<?php 

$this->startSetup();
$table = $this->getConnection()
    ->addColumn(
        $this->getTable('unitedtools_localproducts/storelocation'),
        'product_lookup',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'default' => 0,
            'nullable'  => false,
            'comment' => 'Lookup Products'
        )
    );
$this->endSetup();

