<?php

class Unitedtools_Localproducts_ProductexistsController 
        extends Mage_Core_Controller_Front_Action {        

    private function toolmart_lookup($sku) {
        // Remove spaces, dots, dashes
        $sku = preg_replace('/\.|\-| /', '', $sku);
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToFilter(array(
            array('attribute'=>'name','eq'=>$sku),
            array('attribute'=>'name2','eq'=>$sku),
            array('attribute'=>'name3','eq'=>$sku),
        )); 

        $collection->getSelect()->limit(1);
        if ($collection->getSize() > 0) {
            return $collection->getFirstItem();
        } else {
            return null;
        }
    }

    public function indexAction() {
        $sku = $this->getRequest()->getParam('sku');
        $toolmart = $this->getRequest()->getParam('toolmart');
        if (!$sku) {
            echo '';
            return;
        }

        $product = null;
        if ($toolmart) {
            $product = $this->toolmart_lookup($sku);
        } else {
            $pm = Mage::getModel('catalog/product');
            $id = $pm->getIdBySku($sku);
            if ($id) {
                $pm->load($id);
                $product = $pm;
            }
        }

        if ($product !== null && $product->isAvailable()) {
            echo $product->getProductUrl();
        } else {
            echo '';
        }
    }
}
