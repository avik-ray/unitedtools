<?php
/**
 * Unitedtools_Localproducts extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Unitedtools
 * @package        Unitedtools_Localproducts
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin controller
 *
 * @category    Unitedtools
 * @package     Unitedtools_Localproducts
 * @author      Ultimate Module Creator
 */
class Unitedtools_Localproducts_Adminhtml_Localproducts_StorelocationController extends Unitedtools_Localproducts_Controller_Adminhtml_Localproducts
{
    /**
     * init the store
     *
     * @access protected
     * @return Unitedtools_Localproducts_Model_Storelocation
     */
    protected function _initStorelocation()
    {
        $storelocationId  = (int) $this->getRequest()->getParam('id');
        $storelocation    = Mage::getModel('unitedtools_localproducts/storelocation');
        if ($storelocationId) {
            $storelocation->load($storelocationId);
        }
        Mage::register('current_storelocation', $storelocation);
        return $storelocation;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('unitedtools_localproducts')->__('Local Products'))
             ->_title(Mage::helper('unitedtools_localproducts')->__('Stores'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $storelocationId    = $this->getRequest()->getParam('id');
        $storelocation      = $this->_initStorelocation();
        if ($storelocationId && !$storelocation->getId()) {
            $this->_getSession()->addError(
                Mage::helper('unitedtools_localproducts')->__('This store no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getStorelocationData(true);
        if (!empty($data)) {
            $storelocation->setData($data);
        }
        Mage::register('storelocation_data', $storelocation);
        $this->loadLayout();
        $this->_title(Mage::helper('unitedtools_localproducts')->__('Local Products'))
             ->_title(Mage::helper('unitedtools_localproducts')->__('Stores'));
        if ($storelocation->getId()) {
            $this->_title($storelocation->getName());
        } else {
            $this->_title(Mage::helper('unitedtools_localproducts')->__('Add store'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new store action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('storelocation')) {
            try {
                $storelocation = $this->_initStorelocation();
                $storelocation->addData($data);
                $storelocation->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('unitedtools_localproducts')->__('Store was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $storelocation->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setStorelocationData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('unitedtools_localproducts')->__('There was a problem saving the store.')
                );
                Mage::getSingleton('adminhtml/session')->setStorelocationData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('unitedtools_localproducts')->__('Unable to find store to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $storelocation = Mage::getModel('unitedtools_localproducts/storelocation');
                $storelocation->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('unitedtools_localproducts')->__('Store was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('unitedtools_localproducts')->__('There was an error deleting store.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('unitedtools_localproducts')->__('Could not find store to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $storelocationIds = $this->getRequest()->getParam('storelocation');
        if (!is_array($storelocationIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('unitedtools_localproducts')->__('Please select stores to delete.')
            );
        } else {
            try {
                foreach ($storelocationIds as $storelocationId) {
                    $storelocation = Mage::getModel('unitedtools_localproducts/storelocation');
                    $storelocation->setId($storelocationId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('unitedtools_localproducts')->__('Total of %d stores were successfully deleted.', count($storelocationIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('unitedtools_localproducts')->__('There was an error deleting stores.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $storelocationIds = $this->getRequest()->getParam('storelocation');
        if (!is_array($storelocationIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('unitedtools_localproducts')->__('Please select stores.')
            );
        } else {
            try {
                foreach ($storelocationIds as $storelocationId) {
                $storelocation = Mage::getSingleton('unitedtools_localproducts/storelocation')->load($storelocationId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d stores were successfully updated.', count($storelocationIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('unitedtools_localproducts')->__('There was an error updating stores.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'storelocation.csv';
        $content    = $this->getLayout()->createBlock('unitedtools_localproducts/adminhtml_storelocation_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'storelocation.xls';
        $content    = $this->getLayout()->createBlock('unitedtools_localproducts/adminhtml_storelocation_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'storelocation.xml';
        $content    = $this->getLayout()->createBlock('unitedtools_localproducts/adminhtml_storelocation_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('unitedtools_localproducts/storelocation');
    }
}
