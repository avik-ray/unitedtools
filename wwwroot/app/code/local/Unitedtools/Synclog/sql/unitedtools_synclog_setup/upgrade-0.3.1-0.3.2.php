<?php
$installer = $this;
 
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/synclog_pendinglocalupdate'))
    ->addColumn('entry_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
            'auto_increment' => true,
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('json', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable'  => false,
        ));
$installer->getConnection()->createTable($table);

$installer->endSetup();

