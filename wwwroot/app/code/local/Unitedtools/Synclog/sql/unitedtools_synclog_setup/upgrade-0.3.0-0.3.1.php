<?php
 
$installer = $this;
 
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/synclog_pendingupdate'))
    ->addColumn('log_entry_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('json', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable'  => false,
        ));
$installer->getConnection()->createTable($table);

$installer->endSetup();

