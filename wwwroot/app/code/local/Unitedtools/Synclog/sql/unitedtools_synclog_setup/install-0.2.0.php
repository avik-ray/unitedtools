<?php
 
$installer = $this;
 
$installer->startSetup();
 
$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/synclog_status'))
    ->addColumn('status_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('needs_sync', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'nullable'  => false,
        ));
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/product_deleted'))
    ->addColumn('product_deleted_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ))
    ->addColumn('deleted_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable'  => false,
        ))
    ->addColumn('completed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable'  => true,
        ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
            'nullable'  => false,
        ))
    ->addForeignKey('unitedtools_synclog_product_deleted_status_fk', 'status', 'unitedtools_synclog_synclog_status', 'status_id');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/product_skuupdate'))
    ->addColumn('product_sku_update_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('old_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ))
    ->addColumn('new_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ))
    ->addColumn('changed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable'  => false,
        ))
    ->addColumn('completed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable'  => true,
        ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
            'nullable'  => false,
        ))
    ->addForeignKey('unitedtools_synclog_product_sku_update_status_fk', 'status', 'unitedtools_synclog_synclog_status', 'status_id');
$installer->getConnection()->createTable($table);

$installer->endSetup();


