<?php
 
$installer = $this;
 
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/product_jsoncache'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('json', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable'  => false,
        ))
    ->addForeignKey(
        $this->getFkName(
            'unitedtools_synclog/product_jsoncache',
            'product_id',
            'catalog/product',
            'entity_id'
        ),
        'product_id',
        $this->getTable('catalog/product'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
        );
$installer->getConnection()->createTable($table);
 
$table = $installer->getConnection()
    ->newTable($installer->getTable('unitedtools_synclog/synclog_autolog'))
    ->addColumn('log_entry_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
            'auto_increment' => true,
            'nullable'  => false,
            'primary'   => true,
        ))
    ->addColumn('json', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable'  => false,
        ));
$installer->getConnection()->createTable($table);


$installer->endSetup();
