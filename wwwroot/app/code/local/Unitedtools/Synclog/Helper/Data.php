<?php

class Unitedtools_Synclog_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $MAX_UPDATES_PER_PROCESS = 500;
    private $IMMUTABLE_CATEGORIES = array('Specials', 'Shop By Product');
    private $SPECIAL_ATTRIBUTES = array(
        'media_gallery' => true,
        'video_gallery' => true,
        'configurable_attributes_data' => true,
        'configurable_products_data' => true
    );
    private $IMMUTABLE_ATTRIBUTES = array(
        'special_price' => true,
        'special_from_date' => true,
        'special_to_date' => true
    );
    private $PRICING_ATTRIBUTES = array(
        'price' => true,
        'hq_price' => true,
        'hq_price_from' => true,
        'hq_price_to' => true
    );
    private $EXCLUDE_ATTRS = array(
        'entity_id',
        'updated_at',
        'is_in_stock', 'qty',
        'stock_data', 'stock_item', 'media_attributes',
        'type_has_options', 'type_has_required_options',
        'custom_design_from_is_formated', 'parent_id',
        'custom_design_to_is_formated', 'news_from_date_is_formated',
        'news_to_date_is_formated', 'special_from_date_is_formated',
        'special_to_date_is_formated', 'is_changed_websites'
    );

    public function array_get($data, $key, $default = null) {
        if (!is_array($data)) {
            return $default;
        }
        return isset($data[$key]) ? $data[$key]: $default;
    }


    public function normalizeAttribute($k, $val) 
    {
        if ($k == 'group_price') {
            return $val;
        } else if ($k == 'visibility') {
            return $val;
        } else if ($k == 'tax_class_id') {
            return $val;
        } else if ($k == 'media_attributes') {
            return $val;
        } else if ($k == 'video_gallery') {
            return $val;
        } else if ($k == 'media_gallery') {
            foreach ($val['images'] as &$img) {
                $img['url'] = Mage::getModel('catalog/product_media_config')
                    ->getMediaUrl($img['file']);
            }
            return $val;
        }

        $map = $this->getAttributeMap();
        if (!is_numeric($val) && !is_string($val))
            return $val;

        if (isset($map[$k]) && isset($map[$k][$val]))
            return $map[$k][$val];
        else
            return $val;
    }

    private function excludeAttr($k) {
        if ($k[0] == '_')
            return true;;
        foreach ($this->EXCLUDE_ATTRS as $attr) {
            if ($k == $attr)
                return true;
        }
        return false;
    }

    public function makeConfigurableProductsData($product)
    {
        /*
            Note that magento only actually needs the ids; all the other 
            included information appears to be optional, so that's all we include.
         */
        $data = array();
        $products = $product->getTypeInstance()->getUsedProducts($product);
        foreach ($products as $product) {
            $data[$product->getId()] = array();
        }
        return $data;
    }

    public function serializeProduct($product)
    {
        $id = $product->getId();
        $json = $product->getData('json');
        if (!$json) {
            $full_product = Mage::getModel('catalog/product')->load($product->getId());
            $data = $full_product->getData();
            $data['category_ids'] = $product->getCategoryIds();
            if ($product->getTypeId() === 'configurable') {
                $data['configurable_attributes_data'] = $product->getTypeInstance()
                    ->getConfigurableAttributesAsArray();
                $data['configurable_products_data'] = $this->makeConfigurableProductsData($product);
            }
            $json = $this->serializeProductData($data, true, true, true);
        }
        $sku = json_encode($product->getData('sku'), JSON_PRETTY_PRINT);
        return '{"sku":' . $sku . ',"action":"product_update","data":' . $json . '}';
    }

    public function serializeProductData($data, $use_cache=FALSE, $as_json=FALSE, $update_cache=FALSE)
    {
        if (!$data) {
            if ($as_json)
                return '{}';
            else
                return array();
        }
        $cache = Mage::getModel('unitedtools_synclog/product_jsoncache');
        if ($use_cache && !$update_cache) {
            $sData = $cache->load($data['entity_id']);
            $json = $sData->getJson();
            if ($json != '') {
                if ($as_json)
                    return $json;
                else
                    return json_decode($json, true);
            }
        }

        $sData = array();
        foreach ($data as $k => $v) {
            if ($this->excludeAttr($k))
                continue;
            $sData[$k] = $this->normalizeAttribute($k, $v);
        }

        // Deal with complex products
        if (isset($sData['configurable_attributes_data'])) {
            $map = $this->getAttributeMap();
            $new_attrs = array();
            foreach ($sData['configurable_attributes_data'] as $attr) {
                $attr['attribute_code'] = $this->getAttributeCode($attr['attribute_id']);
                $new_attrs[] = $attr;
            }
            $sData['configurable_attributes_data'] = $new_attrs;
        }

        if (isset($sData['configurable_products_data'])) {
            $new_cpd = array();
            $map = $this->getAttributeMap();
            foreach ($sData['configurable_products_data'] as $id => $attrs) {
                $product = Mage::getModel('catalog/product')->load($id);
                $new_attrs = array();
                foreach ($attrs as $attr) {
                    $attr_code = $this->getAttributeCode($attr['attribute_id']);
                    $new_attrs[] = array(
                        'attribute_code' => $attr_code,
                        'attribute_id' => $attr['attribute_id'],
                        'label' => $attr['label'],
                        'value_index' => $attr['value_index']
                    );
                }
                $new_cpd[$product->getSku()] = $new_attrs;
            }
            $sData['configurable_products_data'] = $new_cpd;
        }

        // Update cache
        if ($as_json || $update_cache)
            $json = json_encode($sData);

        if ($update_cache) {
            $cache = Mage::getModel('unitedtools_synclog/product_jsoncache');
            $cache->setData('product_id', $data['entity_id']);
            $cache->setData('json', $json);
            $cache->save();
        }

        if ($as_json)
            return $json;
        else
            return $sData;
    }

    public function serializeAllPrices() 
    {
        $results = array();
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('hq_price')
            ->addAttributeToSelect('hq_price_from')
            ->addAttributeToSelect('hq_price_to');
        foreach ($products as $product) {
            $results[] = array(
                    'sku' => $product->getData('sku'),
                    'action' => 'product_update',
                    'data' => array(
                        'price' => $product->getData('price'),
                        'hq_price' => $product->getData('hq_price'),
                        'hq_price_from' => $product->getData('hq_price_from'),
                        'hq_price_to' => $product->getData('hq_price_to')
                    )
                );
        }
        return $results;
    }

    public function serializeAllProductsAsJson($page, $category_id) 
    {
        $products = Mage::getModel('catalog/product')->getCollection()
            ->setPageSize(500)
            ->setCurPage($page)
            ->setOrder('id')
            ->setOrder("FIELD(type_id, 'configurable', 'grouped') DESC")
            ->joinTable(
                array('json_cache' => 'unitedtools_synclog/product_jsoncache'), 
                'product_id=entity_id',
                array('json' => 'json'),
                null,
                'left'
            );

        if ($category_id) {
            $products->joinField('category_id', 'catalog/category_product', 
                'category_id', 'product_id=entity_id', null, 'left');
            $products->addAttributeToFilter(
                'category_id', array(
                    array('in' => array($category_id))
                )
            );
        }

        if ($products->getLastPageNumber() < $page)
            return '[]';

        $json = array();
        foreach ($products as $product) {
            $json[] = $this->serializeProduct($product);
        }

        return '[' . implode(',', $json) . ']';
    }

    public function isNodeDifferent($a, $b) {
        if (is_array($a) && is_array($b)) {
            $checked = array();
            foreach ($a as $k => $v) {
                $checked[$k] = true;
                if (array_key_exists($k, $b)) {
                    if ($this->isNodeDifferent($b[$k], $v))
                        return true;
                } else {
                    return true;
                }
            }
            foreach ($b as $k => $v) {
                if (!isset($checked[$k])) {
                    if (array_key_exists($k, $a)) {
                        if ($this->isNodeDifferent($a[$k], $v))
                            return true;
                    } else {
                        return true;
                    }
                }
            }
            return false;
        } else if (!is_array($a) && !is_array($b)) {
            return $a != $b;
        }
        return true;
    }

    public function diffProductData($new, $old) 
    {
        $diff = array();
        $sOld = $this->serializeProductData($old, true, false, false);
        $sNew = $this->serializeProductData($new, false, false, true);
        foreach ($sNew as $k => $v) {
            if ($v === null) 
                continue;

            if (!isset($sOld[$k])) {
                $diff[$k] = $v;
            } else if ($this->isNodeDifferent($sOld[$k], $v)) {
                $diff[$k] = $v;
            }
        }
        return $diff;
    }

    public function getAttributeCode($id)
    {
        $this->getAttributeMap();
        return $this->_attributeCodes[$id];
    }

    public function getAttributeId($code)
    {
        $this->getAttributeMap();
        return $this->_attributeIds[$code];
    }

    public function getAttributeMap()
    {
        if (!isset($this->_attributes)) {
            $this->_attributes = array();
            $this->_attributeCodes = array();
            $this->_attributeIds = array();
            $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                    ->getItems();
            foreach ($attributes as $attr) {
                $code = $attr->getData('attribute_code');
                $model = $attr->getData('source_model');
                if ($model === NULL || $model == 'eav/entity_attribute_source_table') {
                    $this->_attributeCodes[$attr->getId()] = $code;
                    $this->_attributeIds[$code] = $attr->getId();
                    $options = array();
                    foreach($attr->getSource()->getAllOptions(false, false) as $option) {
                        if ($option['value'] && is_numeric($option['value']))
                            $options[$option['value']] = $option['label'];
                    }
                    $this->_attributes[$code] = $options;
                }
            }
        }
        return $this->_attributes;
    }

    public function addOrGetAttributeValue($code, $value)
    {
        if ($value === null)
            return null;

        if ($code == 'status') {
            if ($value == 'Enabled') {
                return Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
            } else if ($value == 'Disabled') {
                return Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
            } else {
                return $value;
            }
        } else if ($code == 'visibility') {
            return $value;
        } else if ($code == 'tax_class_id') {
            return $value;
        }

        $config = Mage::getModel('eav/config');

        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_id = $attribute_model->getIdByCode('catalog_product', $code);
        if (!$attribute_id) {
            return $value;
        }

        $attribute = $attribute_model->load($attribute_id);
        $type = $attribute->getFrontendInput();
        $model = $attribute->getSourceModel();
        if ($model == 'eav/entity_attribute_source_boolean') {
            return $value != '0';
        }
        if ($type != 'select' && $type != 'multiselect') {
            return $value;
        }
        if ($model != 'eav/entity_attribute_source_table' && $model !== null) {
            return $value;
        }

        if(!$option_id = $this->getOptionId($attribute, $value)) {
            /* @var Mage_Eav_Model_Entity_Setup $setup */
            $setup = Mage::getModel('eav/entity_setup', 'core_setup');
            $option['attribute_id'] = $attribute_id;
            $option['value']['option_'.$attribute_id][0] = $value;
            $setup->addAttributeOption($option);
            $option_id = $this->getOptionId($attribute, $value);
        }
        return $option_id;
    } 

    public function getOptionId(Mage_Eav_Model_Entity_Attribute $attribute, 
        $value)
    {
        /* @var Mage_Eav_Model_Entity_Attribute_Source_Table $attribute_options_model */
        $attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
        $attribute_options_model->setAttribute($attribute);
        return $attribute_options_model->getOptionId($value);
    } 

    public function update_media_gallery($product, $media) {
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
        $add = array();
        foreach ($media['images'] as $image) {
            $add[$image['file']] = $image;
        }

        // Remove existing media
        $items = $mediaApi->items($product->getId());
        foreach($items as $item) {
            if (isset($add[$item['file']])) {
                unset($add[$item['file']]);
            } else {
                $mediaApi->remove($product->getId(), $item['file']);
            }
        }

        $assignments = $media['values'];
        $lookup = array();

        // Update new images
        foreach ($add as $f => $image) {
            $media_types = array();
            foreach ($assignments as $media_type => $afile) {
                if ($afile == $image['file']) {
                    $media_types[] = $media_type;
                }
            }
            $ext = substr($image['url'], strrpos($image['url'], '.') + 1);
            if ($ext == 'jpg' || $ext == 'jpe') {
                $ext = 'jpeg';
            }
            $spos = strrpos($image['file'], '/') + 1;
            $dpos = strrpos($image['file'], '.');
            $name = substr($image['file'], $spos, $dpos - $spos);
            $file = array(
                'file' => array(
                    'content' => base64_encode(file_get_contents($image['url'])),
                    'mime' => "image/$ext",
                    'name' => $name,
                ),
                'label' => $image['label'],
                'position' => $image['position'],
                'exclude' => $image['disabled'],
                'types' => $media_types
            );
            try {
                $mediaApi->create($product->getId(), $file);
            } catch (Mage_Api_Exception $e) {
                Mage::log("Error syncing image for product {$product->getSku()}: " . $e);
            }
        }
    }

    protected function update_configurable_products_data($product, $data) {
        $newData = array();
        foreach ($data as $sku => $attribute) {
            $product_id = Mage::getModel("catalog/product")->getIdBySku($sku);
            $newData[$product_id] = $attribute;
        }
        $product->setData('configurable_products_data', $newData);
    }

    public function isCategoryImmutableById($id) {
        if (!isset($this->_immutableCategoryIds)) {
            $this->_immutableCategoryIds = [];
            foreach ($this->IMMUTABLE_CATEGORIES as $catname) {
                $categories = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', $catname);   
                foreach ($categories as $cat) {
                    $this->_immutableCategoryIds[$cat->getId()] = true;
                }
            }
        }
        return array_key_exists($id, $this->_immutableCategoryIds);
    }

    /* Don't change membership of products in immutable categories.
       Takes a list of proposed category ids, returns a list of 
       ids that should be set to preserve.
    */
    public function handleImmutableCategoryIds($product, $ids) {
        $final_ids = [];

        // Remove immutables
        foreach ($ids as $id) {
            if (!$this->isCategoryImmutableById($id)) {
                $final_ids[] = $id;
            }
        }
        
        // Add if immutable set
        $existing = $product->getCategoryIds();
        foreach ($existing as $id) {
            if ($this->isCategoryImmutableById($id)) {
                $final_ids[] = $id;
            }
        }
        
        return $final_ids;
    }

    protected function update_configurable_attributes_data($product, $data) {
        $existing = array();
        foreach ($product->getTypeInstance()->getConfigurableAttributesAsArray() as $attr) {
            $existing[$attr['attribute_id']] = $attr['id'];
        }

        $used = array();
        $newData = array();
        foreach ($data as $attribute) {
            $attribute['attribute_id'] = $this->getAttributeId($attribute['attribute_code']);
            if ($attribute['attribute_id'] === null)
                continue;
            $super_id = $this->array_get($existing, $attribute['attribute_id'], null);
            $attribute['id'] = $super_id;
            $used[] = $attribute['attribute_id'];
            foreach ($attribute['values'] as &$value) {
                $value['attribute_id'] = $attribute['attribute_id'];
                $value['value_index'] = $this->addOrGetAttributeValue($attribute['attribute_code'],
                    $value['label']);
                $value['product_super_attribute_id'] = $super_id;
            }
            $newData[] = $attribute;
        }
        $product->getTypeInstance()->setUsedProductAttributeIds($used);
        $product->setCanSaveConfigurableAttributes(true);
        $product->setConfigurableAttributesData($newData);
    }

    public function getMappedCategory($remote_id) {
        $policies = $this->policies();
        if ($policies && isset($policies['category_map']) && !empty($policies['category_map'])) {
            return $this->array_get($policies['category_map'], $remote_id, NULL);
        } else {
            return NULL;
        }
    }

    public function updateAttribute($product, $k, $v) {
        if (!is_array($v)) {
            $v = $this->addOrGetAttributeValue($k, $v);
            $product->setData($k, $v);
        } else if ($k == 'website_ids') {
            $product->setWebsiteIds($v);
        } else if ($k == 'category_ids') {
            // Respect category map
            $cats = [];
            foreach($v as $c) {
                $nc = $this->getMappedCategory($v);
                if ($nc !== NULL)
                    $cats[] = $nc;
            }
            $product->setData($k, 
                $this->handleImmutableCategoryIds($product, $cats));
        }
    }

    public function updateCategory($update)
    {
        $data = $update['data'];
        $cat_id = $this->getMappedCategory($data['id']);
        if ($cat_id === null) {
            // TODO: communicate error back to utsync 
            Mage::log($this->policies());
            Mage::log("BAD CATEGORY UPDATE");
        } else {
            $category = Mage::getModel('catalog/category')->load($cat_id);
            $products = Mage::getModel("catalog/product")->getCollection()->
                addAttributeToFilter(
                    'sku', array('in', array_keys($data['products'])));
            $add = [];
            foreach ($products as $p) {
                $add[$p->getId()] = true;
            }

            $existing = $category->getProductCollection();
            $remove = [];
            foreach ($existing as $p) {
                $id = $p->getId();
                if (array_key_exists($id, $add)) {
                    unset($add[$id]);
                } else {
                    $remove[$id] = true;
                }
            }

            foreach ($add as $p => $_) {
                Mage::getSingleton('catalog/category_api')
                      ->assignProduct($cat_id, $p);
            }

            foreach ($remove as $p => $_) {
                Mage::getSingleton('catalog/category_api')
                      ->removeProduct($cat_id, $p);
            }
        }

    }

    public function createOrUpdateProduct($update)
    {
        $id = Mage::getModel("catalog/product")->getIdBySku($update['sku']);
        $product = Mage::getModel('catalog/product');
        $data = $update['data'];
        $update_pricing = $this->policies()['update_pricing'];
        if ($id) {
            # Exists
            $product = Mage::getModel('catalog/product')->load($id);
            unset($data['status']);
        } else {
            $update_pricing = true;
            if ($update['action'] == 'product_update_partial') {
                return; // We can't create a product with partial data
            }
        }

        # don't update if this is an excluded brand
        if (isset($data['manufacturer'])) {
            $brand = $data['manufacturer'];
        } else {
            $brand = $this->normalizeAttribute('manufacturer', $product->getData('manufacturer'));
        }

        if ($brand) {
            if ($this->array_get($this->policies()['exclude_brands'], $brand))
                return;
        }

        // Get it an id so that media updates work
        foreach ($data as $k => $v) {
            #print($k . "\n");
            if (array_key_exists($k, $this->IMMUTABLE_ATTRIBUTES))
                continue;
            if (array_key_exists($k, $this->SPECIAL_ATTRIBUTES))
                continue;

            if (!$update_pricing && array_key_exists($k, $this->PRICING_ATTRIBUTES))
                continue;

            $this->updateAttribute($product, $k, $v);
        }


        if ($product->getTypeId() === 'configurable') {
            if ($this->array_get($data, 'configurable_attributes_data')) {
                $this->update_configurable_attributes_data($product, 
                    $data['configurable_attributes_data']);
            }

            if ($this->array_get($data, 'configurable_products_data')) {
                $this->update_configurable_products_data($product, 
                    $data['configurable_products_data']);
            }
        }

        $product->save();

        if (isset($data['media_gallery'])) {
            $this->update_media_gallery($product, $data['media_gallery']);
        }
    }

    public function processUpdate($update) {
        $update_data = json_decode($update->getData('json'), TRUE);

        try {
            if ($update_data['action'] == 'product_delete') {
                $product_id = Mage::getModel("catalog/product")->getIdBySku($update_data['sku']);
                if ($product_id) {
                    Mage::getModel('catalog/product')->load($product_id)->delete();
                }
            } else if ($update_data['action'] == 'product_update' 
                    || $update_data['action'] == 'product_update_partial' ) {
                $product = $this->createOrUpdateProduct($update_data);
            } else if ($update_data['action'] == 'category_update' ) {
                $this->updateCategory($update_data);
            }
        } catch (Exception $e) {
            Mage::log($e);
        }
    }

    private function policies()
    {
        if (!isset($this->_policies)) {
            $this->_policies = Mage::getModel('unitedtools_synclog/synclog_flag_policies')
                ->loadSelf()->getFlagData();
            $this->_policies = array_merge(array(
                'update_pricing' => true,
                'exclude_brands' => array(),
                'category_map' => array()),
                $this->_policies);

            # make exclude_brands into a faux set
            $this->_policies['exclude_brands'] = array_fill_keys($this->_policies['exclude_brands'], true);
        }
        return $this->_policies;
    }
    public function processUpdates()
    {
        # Check the update lock
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        
        $free = $writeConnection->fetchOne("SELECT IS_FREE_LOCK('synclog_update')");
        if ($free != 1) {
            print("Failed to get lock");
            return;
        }

        $obtained = $writeConnection->fetchOne("SELECT GET_LOCK('synclog_update', 0)");
        if ($obtained != 1) {
            print("Failed to get lock");
            return;
        }
        
        # Limit to N updates to keep php memory use under control
        $n = $this->MAX_UPDATES_PER_PROCESS;

        $last_processed = Mage::getModel('unitedtools_synclog/synclog_flag_lastprocessed')
            ->loadSelf();

        $pending_local = Mage::getModel('unitedtools_synclog/synclog_pendinglocalupdate')
            ->getCollection()
            ->setOrder('entry_id', 'ASC');
        $pending_local->getSelect()->limit($n);
        foreach ($pending_local as $update) {
            $writeConnection->beginTransaction();
            $this->processUpdate($update);
            $update->delete();
            $writeConnection->commit();
            $n--;
            if ($n == 0)
                break;
        }

        $pending = Mage::getModel('unitedtools_synclog/synclog_pendingupdate')
            ->getCollection()
            ->setOrder('log_entry_id', 'ASC');
        $pending->getSelect()->limit($n);
        foreach ($pending as $update) {
            $writeConnection->beginTransaction();
            $this->processUpdate($update);
            $last_processed->setFlagData($update->getData('log_entry_id'))->save();
            $update->delete();
            $writeConnection->commit();
            $n--;
            if ($n == 0)
                break;
        }


        $writeConnection->fetchOne("SELECT RELEASE_LOCK('synclog_update')");
    }

    public function queueUpdates($raw_updates)
    {
        $lp_model = Mage::getModel('unitedtools_synclog/synclog_flag_lastprocessed');
        $last_processed = $lp_model->loadSelf()->getFlagData();
        if ($last_processed === null)
            $last_processed = 0;
        $updates = json_decode($raw_updates, true);
        foreach ($updates as $update) {
            if (!isset($update['id'])) {
                $pending = Mage::getModel('unitedtools_synclog/synclog_pendinglocalupdate');
                $pending->setData('json', json_encode($update));
                $pending->save();
            } else {
                if ($update['id'] <= $last_processed)
                    continue;
                $pending = Mage::getModel('unitedtools_synclog/synclog_pendingupdate');
                $pending->setData('log_entry_id', $update['id']);
                $pending->setData('json', json_encode($update));
                $pending->save();
            }
        }
    }

    public function getUpdateQueueSize()
    {
        return Mage::getModel('unitedtools_synclog/synclog_pendingupdate')
                ->getCollection()->getSize()
            + Mage::getModel('unitedtools_synclog/synclog_pendinglocalupdate')
                ->getCollection()->getSize();
    }

    public function serializeCategory($category)
    {
        $products = $category->getProductCollection();
        $product_positions = $category->getProductsPosition();
        $product_data = array();
        foreach ($products as $p) {
            $product_data[$p->getSku()] = $product_positions[$p->getId()];
        }
        $data = array(
            'id' => $category->getId(),
            'name' => $category->getName(),
            'path' => $category->getPathIds(),
            'products' => $product_data
        );
        return $data;
    }
}
