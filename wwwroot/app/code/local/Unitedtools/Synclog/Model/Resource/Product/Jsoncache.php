<?php
class Unitedtools_Synclog_Model_Resource_Product_Jsoncache extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('unitedtools_synclog/product_jsoncache', 'product_id');
        $this->_isPkAutoIncrement = false;
    }
} 


