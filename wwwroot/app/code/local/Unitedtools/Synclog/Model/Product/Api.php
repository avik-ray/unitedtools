<?php

class Unitedtools_Synclog_Model_Product_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     * @return array
     */
    public function deletionList() 
    {
        $deletions = Mage::getModel('unitedtools_synclog/product_deleted')->getCollection()
            ->addFieldToFilter('status', 'pending');

        $result = array();
        foreach($deletions as $deletion) {
            $result[] = array(
                'deleteId' => $deletion->getId(),
                'sku' => $deletion->getSku(),
                'deletedAt' => $deletion->getDeletedAt(),
            );
            $deletion->setStatus('notified');
            $deletion->save();
        }
        Mage::log($result);
        return $result;
    }

    public function skuUpdateList() 
    {
        $updates = Mage::getModel('unitedtools_synclog/product_skuupdate')->getCollection()
            ->addFieldToFilter('status', 'pending');

        $result = array();
        foreach($updates as $update) {
            $result[] = array(
                'updateId' => $update->getId(),
                'newSku' => $update->getNewSku(),
                'oldSku' => $update->getOldSku(),
                'changedAt' => $update->getChangedAt(),
            );
            $update->setStatus('notified');
            $update->save();
        }
        return $result;
    }

    public function confirmAllReceived() 
    {
        $updates = Mage::getModel('unitedtools_synclog/product_skuupdate')
            ->getCollection()
            ->addFieldToFilter('status', 'notified');
        $deletions = Mage::getModel('unitedtools_synclog/product_deleted')
            ->getCollection()
            ->addFieldToFilter('status', 'notified');

        foreach($updates as $update) {
            $update->setStatus('finished');
            $update->save();
        }
        foreach($deletions as $deletion) {
            $deletion->setStatus('finished');
            $deletion->save();
        }

        return true;
    }

    public function getAutolog($startWithId) 
    {
        $result = array();
        $autolog = Mage::getModel('unitedtools_synclog/synclog_autolog')
            ->getCollection()
            ->addFieldToFilter('log_entry_id', array('gt' => $startWithId))
            ->setOrder('log_entry_id', 'ASC');
        foreach ($autolog as $entry) {
            $result[] = $entry->getData('json');
        }
        return '[' . implode(',', $result) . ']';
    }

    public function dump($page, $category_id) 
    {
        $helper = Mage::helper('synclog');
        $products = $helper->serializeAllProductsAsJson($page, $category_id);
        return $products;
    }

    public function dumpPrices() 
    {
        $helper = Mage::helper('synclog');
        $products = json_encode($helper->serializeAllPrices());
        return $products;
    }

    public function updateFromLog($updates) 
    {
        $helper = Mage::helper('synclog');
        $helper->queueUpdates($updates);
        $n = $helper->getUpdateQueueSize();

        return $n;
    }

    public function setAutosyncPolicy($newPolicy)
    {
        $flag = Mage::getModel('unitedtools_synclog/synclog_flag_policies');

        // Update policy; keep any not updated
        $policies = $flag->loadSelf()->getFlagData();
        foreach (json_decode($newPolicy, true) as $name => $policy) {
            $policies[$name] = $policy;
        }

        $flag->setFlagData($policies)->save();
        return true;
    }

    public function autosyncPosition()
    {
        $helper = Mage::helper('synclog');
        $n = $helper->getUpdateQueueSize();
        return $n;
    }
}
