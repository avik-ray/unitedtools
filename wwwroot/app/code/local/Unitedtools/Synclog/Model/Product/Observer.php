<?php

class Unitedtools_Synclog_Model_Product_Observer
{

    public function __construct()
    {
    }

    /**
     * Keep a record of product deletions for later local site sync.
     * @param Varien_Event_Observer $observer
     * @return Unitedtools_Catalog_Model_Product_Observer
     */
    public function product_record_delete($observer)
    {
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $sku = $product->getSku();
        $synclog_deletion = Mage::getModel('unitedtools_synclog/product_deleted');
        $synclog_deletion->setSku($sku);
        $synclog_deletion->setStatus('pending');
        $synclog_deletion->setDeletedAt(date('Y-m-d H:i:s'));
        $synclog_deletion->save();
        $autolog_enabled = Mage::getStoreConfig('system/synclog/autosync_enabled');
        if ($autolog_enabled) {
            $json = array(
                'sku' => $sku,
                'action' => 'product_delete'
            );
            $autolog = Mage::getModel('unitedtools_synclog/synclog_autolog');
            $autolog->setData('json', '');
            $autolog->save();
            $json['id'] = $autolog->getId();
            $autolog->setData('json', json_encode($json));
            $autolog->save();
        }

        return $this;
    }

    /**
     * Keep a record of product changes
     * @param Varien_Event_Observer $observer
     * @return Unitedtools_Catalog_Model_Product_Observer
     */
    public function product_record_changed($observer)
    {
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $oldSku = $product->getOrigData('sku');
        $newSku = $product->getSku();
        if ($oldSku !== NULL && $newSku !== $oldSku) {
            $sku_update = Mage::getModel('unitedtools_synclog/product_skuupdate');
            $sku_update->setOldSku($oldSku);
            $sku_update->setNewSku($newSku);
            $sku_update->setStatus('pending');
            $sku_update->setChangedAt(date('Y-m-d H:i:s'));
            $sku_update->save();
        }
        $autolog_enabled = Mage::getStoreConfig('system/synclog/autosync_enabled');
        if ($autolog_enabled) {
            $helper = Mage::helper('synclog');
            $diff = $helper->diffProductData($product->getData(), $product->getOrigData());
            $diff['attribute_set_id'] = $product->getAttributeSetId();

            if (count($diff) > 0) {

                if (!$oldSku) {
                    $oldSku = $newSku;
                    $action = 'product_update';
                } else {
                    $action = 'product_update_partial';
                }
                $json = array(
                    'sku' => $oldSku,
                    'action' => $action,
                    'data' => $diff
                );
                $autolog = Mage::getModel('unitedtools_synclog/synclog_autolog');
                $autolog->setData('json', '');
                $autolog->save();
                $json['id'] = $autolog->getId();
                $autolog->setData('json', json_encode($json));
                //Mage::log(json_encode($json, JSON_PRETTY_PRINT));
                $autolog->save();
            }
        }
        return $this;
    }
}
