<?php

class Unitedtools_Synclog_Model_Category_Observer
{
    /**
     * Keep a record of category changes
     * @param Varien_Event_Observer $observer
     * @return Unitedtools_Catalog_Model_Product_Observer
     */
    public function category_record_changed($observer)
    {
        Mage::log("Category changed!");
        $event = $observer->getEvent();
        $category = $event->getCategory();
        $oldName = $category->getOrigData('name');
        $autolog_enabled = Mage::getStoreConfig('system/synclog/autosync_enabled');
        
        $helper = Mage::helper('synclog');
        if ($helper->isCategoryImmutableById($category->getId()))
            return $this;

        if (!$autolog_enabled)
            return $this;

        $action = 'category_update';
        $json = array(
            'id' => $category->getId(),
            'action' => $action,
            'data' => $helper->serializeCategory($category)
        );

        $autolog = Mage::getModel('unitedtools_synclog/synclog_autolog');
        $autolog->setData('json', '');
        $autolog->save();
        $json['id'] = $autolog->getId();
        $autolog->setData('json', json_encode($json));
        Mage::log(json_encode($json, JSON_PRETTY_PRINT));
        $autolog->save();

        return $this;
    }
}

