<?php
$statuses = array(
    array(
        'status_id' => 'pending',
        'needs_sync' => true,
    ),
    array(
        'status_id' => 'notified',
        'needs_sync' => false,
    ),
    array(
        'status_id' => 'finished',
        'needs_sync' => false,
    ),
);
foreach ($statuses as $status) {
    $model = Mage::getModel('unitedtools_synclog/synclog_status')
        ->setData($status)
        ->save();
}
