<?php

class Temando_Temando_Model_Rule_Condition_Address extends Mage_SalesRule_Model_Rule_Condition_Address
{
    public function loadAttributeOptions()
    {
        $attributes = array(
            'base_subtotal' => Mage::helper('temando')->__('Subtotal'),
            'total_qty' => Mage::helper('temando')->__('Total Items Quantity'),
            'weight' => Mage::helper('temando')->__('Total Weight'),
            'postcode' => Mage::helper('temando')->__('Shipping Postcode'),
            'region' => Mage::helper('temando')->__('Shipping Region'),
            'region_id' => Mage::helper('temando')->__('Shipping State/Province'),
            'country_id' => Mage::helper('temando')->__('Shipping Country'),
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getInputType()
    {
        switch ($this->getAttribute()) {
            case 'base_subtotal': case 'weight': case 'total_qty':
                return 'numeric';

            case 'country_id': case 'region_id':
                return 'select';
        }
        return 'string';
    }

    public function getValueElementType()
    {
        switch ($this->getAttribute()) {
            case 'country_id': case 'region_id':
                return 'select';
        }
        return 'text';
    }

    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            switch ($this->getAttribute()) {
                case 'country_id':
                    $options = Mage::getModel('adminhtml/system_config_source_country')
                        ->toOptionArray();
                    break;

                case 'region_id':
                    $options = Mage::getModel('adminhtml/system_config_source_allregion')
                        ->toOptionArray();
                    break;

                default:
                    $options = array();
            }
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $address = $object;
        if (!$address instanceof Mage_Sales_Model_Quote_Address) {
            if ($object->isVirtual()) {
                $address = $object->getBillingAddress();
            }
            else {
                $address = $object->getShippingAddress();
            }
        }
	//recalculate combined product weight - take from temando package attributes
	if(Mage::registry('temando_address_weight')) {
	    $addressWeight = Mage::registry('temando_address_weight');
	} else {
	    $addressWeight = Mage::helper('temando')->getTotalArticleWeight($address->getQuote());
	    Mage::register('temando_address_weight', $addressWeight);
	}
	$address->setWeight($addressWeight);
        return parent::validate($address);
    }
}