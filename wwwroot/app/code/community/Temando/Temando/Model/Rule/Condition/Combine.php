<?php

class Temando_Temando_Model_Rule_Condition_Combine extends Mage_Rule_Model_Condition_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('temando/rule_condition_combine');
    }

    public function getNewChildSelectOptions()
    {
        $addressCondition = Mage::getModel('temando/rule_condition_address');
        $addressAttributes = $addressCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();
        foreach ($addressAttributes as $code=>$label) {
            $attributes[] = array('value'=>'temando/rule_condition_address|'.$code, 'label'=>$label);
        }

        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive($conditions, array(
            array('value'=>'temando/rule_condition_product_found', 'label'=>Mage::helper('temando')->__('Product attribute combination')),
            array('value'=>'temando/rule_condition_product_subselect', 'label'=>Mage::helper('temando')->__('Products subselection')),
            array('value'=>'temando/rule_condition_combine', 'label'=>Mage::helper('temando')->__('Conditions combination')),
            array('label'=>Mage::helper('temando')->__('Cart Attribute'), 'value'=>$attributes),
        ));

        $additional = new Varien_Object();
        if ($additionalConditions = $additional->getConditions()) {
            $conditions = array_merge_recursive($conditions, $additionalConditions);
        }

        return $conditions;
    }
}
