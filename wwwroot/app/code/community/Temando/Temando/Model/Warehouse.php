<?php
/**
 * @method Temando_Temando_Model_Warehouse setName()
 * @method Temando_Temando_Model_Warehouse setIsActive()
 * @method Temando_Temando_Model_Warehouse setCompanyName()
 * @method Temando_Temando_Model_Warehouse setStreet()
 * @method Temando_Temando_Model_Warehouse setCity()
 * @method Temando_Temando_Model_Warehouse setRegion()
 * @method Temando_Temando_Model_Warehouse setPostcode()
 * @method Temando_Temando_Model_Warehouse setCountry()
 * @method Temando_Temando_Model_Warehouse setContactName()
 * @method Temando_Temando_Model_Warehouse setContactEmail()
 * @method Temando_Temando_Model_Warehouse setContactPhone1()
 * @method Temando_Temando_Model_Warehouse setContactPhone2()
 * @method Temando_Temando_Model_Warehouse setContactFax()
 * @method Temando_Temando_Model_Warehouse setPriority()
 * @method Temando_Temando_Model_Warehouse setLocationType()
 * @method Temando_Temando_Model_Warehouse setStoreIds()
 * @method Temando_Temando_Model_Warehouse setZoneIds()
 * @method Temando_Temando_Model_Warehouse setLoadingFacilities()
 * @method Temando_Temando_Model_Warehouse setDock()
 * @method Temando_Temando_Model_Warehouse setForklift()
 * @method Temando_Temando_Model_Warehouse setLimitedAccess()
 * @method Temando_Temando_Model_Warehouse setPostalBox()
 * @method Temando_Temando_Model_Warehouse setLabelType()
 * 
 * @method string getName()
 * @method int    getIsActive()
 * @method string getCompanyName()
 * @method string getStreet()
 * @method string getCity()
 * @method string getRegion()
 * @method string getPostcode()
 * @method string getCountry()
 * @method string getContactName()
 * @method string getContactEmail()
 * @method string getContactPhone1()
 * @method string getContactPhone2()
 * @method string getContactFax()
 * @method int    getPriority()
 * @method string getLocationType()
 * @method string getStoreIds()
 * @method string getZoneIds()
 * @method string getLoadingFacilities()
 * @method string getDock()
 * @method string getForklift()
 * @method string getLimitedAccess()
 * @method string getPostalBox()
 * @method string getLabelType()
 */
class Temando_Temando_Model_Warehouse extends Mage_Core_Model_Abstract {
    
    public function _construct() {
	parent::_construct();
	$this->_init('temando/warehouse');
    }
    
    
    /**
     * Returns warehouse title (combined name + address)
     * 
     * @return string 
     */
    public function getTitle() {
	return $this->getName() . ' (' . $this->getStreet() . ', ' . 
		$this->getCity() . ', ' . $this->getCountry() . ')';
    }
    
    /**
     * Returns true if warehouse serves given postal code, false otherwise.
     * (Supports numeric ranges only.)
     * 
     * @param string $countryId
     * @param string|int $postcode
     * @return boolean 
     */
    public function servesArea($postcode, $countryId) {
	$zones = explode(',', $this->getZoneIds());
	
	if(is_array($zones) && !empty($zones)) {
	    foreach($zones as $zoneId) {
		$zone = Mage::getModel('temando/zone')->load($zoneId);
		/* @var $zone Temando_Temando_Model_Zone */
		
		if($zone->getCountry() != $countryId)
		    continue;

		$ranges = explode(',', $zone->getRanges());
		if(count($ranges) === 1) {
		    $range = trim($ranges[0]);
		    if(empty($range)) {
			//range not specified - all country
			return true;
		    }
		}
		
		foreach($ranges as $range) {
		    $minmax = explode(':', $range);
		    if(count($minmax) == 2) {
			//range specified as a:b
			if($postcode >= $minmax[0] && $postcode <= $minmax[1])
			    return true;
		    } else if(count($minmax) == 1) {
			//single value
			if($postcode == $minmax[0])
			    return true;
		    } 
		}
	    }
	}
	
	return false;
    }
    
    /**
     * Returns request array used to create location via API
     * 
     * @return array
     */
    public function toCreateLocationRequestArray()
    {
	return array(
	    'description' => $this->getName(),
	    'type' => 'Origin',
	    'contactName' => $this->getContactName(),
	    'companyName' => $this->getCompanyName(),
	    'street' => $this->getStreet(),
	    'suburb' => $this->getCity(),
	    'state' => $this->getRegion(),
	    'code' => $this->getPostcode(),
	    'country' => $this->getCountry(),
	    'phone1' => $this->getData('contact_phone_1'),
	    'phone2' => $this->getData('contact_phone_2'),
	    'fax' => $this->getContactFax(),
	    'email' => $this->getContactEmail(),
	    'loadingFacilities' => $this->getLoadingFacilities(),
	    'forklift' => $this->getForklift(),
	    'dock' => $this->getDock(),
	    'limitedAccess' => $this->getLimitedAccess(),
	    'postalBox' => $this->getPostalBox(),
	);
    }
    
}


