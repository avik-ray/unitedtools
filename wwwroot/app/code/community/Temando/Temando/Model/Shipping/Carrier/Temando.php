<?php
/**
 * @method Temando_Temando_Model_Shipping_Carrier_Temando setIsProductPage(boolean $flag)
 * @method Temando_Temando_Model_Shipping_Carrier_Temando setIsCartPage(boolean $flag)
 * 
 * @method boolean getIsProductPage()
 * @method boolean getIsCartPage()
 */


class Temando_Temando_Model_Shipping_Carrier_Temando 
    extends Mage_Shipping_Model_Carrier_Abstract 
	implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * Error Constants
     */
    const ERR_INVALID_COUNTRY = 'To and From addresses must be within Australia';
    const ERR_INVALID_DEST    = 'Please enter a delivery address to view available shipping methods';
    const ERR_NO_METHODS      = 'No shipping methods available';
    const ERR_INTERNATIONAL   = 'International delivery is not available at this time.';
    const ERR_NO_ORIGIN	      = 'Unable to fullfil the order at this time due to missing origin data.';	
    
    /**
     * Error Map
     * 
     * @var array 
     */
    protected static $_errors_map = array(
        "The 'destinationCountry', 'destinationCode' and 'destinationSuburb' elements (within the 'Anywhere' type) do not contain valid values.  These values must match with the predefined settings in the Temando system."
                => "Invalid suburb / postcode combination."
    );
    
    /**
     * Carrier's code
     */
    const CARRIER_CODE = 'temando';
    
    /**
     * Carrier's title
     *
     * @var string
     */
    const CARRIER_TITLE = 'Temando';

    /**
     * Rates result
     *
     * @var array|null
     */
    protected $_rates;
    
    /**
     * @var Mage_Shipping_Model_Rate_Request
     */
    protected $_rate_request;

    /**
     * Check if carrier has shipping tracking option available
     *
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        return true;
    }
    
    /**
     * Check if carrier has shipping label option available
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        return false;
    }
    
    
    public function __construct()
    {
        parent::__construct();
        $this->setIsProductPage(("etemando" == Mage::app()->getRequest()->getModuleName()) && ("pcs" == Mage::app()->getRequest()->getControllerName()));
	$this->setIsCartPage(("checkout" == Mage::app()->getRequest()->getModuleName()) && ("cart" == Mage::app()->getRequest()->getControllerName()));
    }
    
    /**
     * Checks if the to address is within allowed countries
     *
     * @return boolean
     */
    protected function _canShip(Mage_Shipping_Model_Rate_Request $request)
    {
        return array_key_exists($request->getDestCountryId(), Mage::helper('temando')->getAllowedCountries());
    }
    
    /**
     * Creates the flat rate method, with the price set in the config. An
     * optional parameter allows the price to be overridden.
     *
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getFlatRateMethod($price = false, $free = false)
    {   
        if ($price === false) {
            $cost = $this->getConfigData('pricing/shipping_fee');
	    $price = $this->getFinalPriceWithHandlingFee($cost);
        } else {
            $cost = $price;
        }

        $title = $free ? 'Free Shipping' : 'Flat Rate';
        $method = Mage::getModel('shipping/rate_result_method')
            ->setCarrier(self::CARRIER_CODE)
            ->setCarrierTitle(self::getTitle())
            ->setMethodTitle($title)
            ->setMethod($free ? Temando_Temando_Model_Carrier::FREE . '_0' : Temando_Temando_Model_Carrier::FLAT_RATE . '_0')
            ->setPrice($price)
            ->setCost($cost);
            
        return $method;
    }

    /**
     * Returns shipping rate result error method 
     * 
     * @param string $errorText
     * @return Mage_Shipping_Model_Rate_Result_Error
     */
    protected function _getErrorMethod($errorText)
    {
        $error = Mage::getModel('shipping/rate_result_error');
        $error->setCarrier(self::CARRIER_CODE);
        $error->setCarrierTitle(self::getTitle());
        if (isset(self::$_errors_map[$errorText])) {
            $errorText = self::$_errors_map[$errorText];
        }
        $error->setErrorMessage($errorText);

        return $error;
    }
    
    /**
     * Creates a string describing the applicable elements of a rate request.
     *
     * This is used to determine if the quotes fetched last time should be
     * refreshed, or if they can remain valid.
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @param int $salesQuoteId
     *
     * @return boolean
     */
    protected function _createRequestString(Mage_Shipping_Model_Rate_Request $request, $salesQuoteId)
    {
        $requestString = $salesQuoteId . '|';
        foreach ($request->getAllItems() as $item) {
            $requestString .= $item->getProductId() . 'x' . $item->getQty();
        }
        
        $requestString .= '|' . $request->getDestCity();
        $requestString .= '|' . $request->getDestCountryId();
        $requestString .= '|' . $request->getDestPostcode();
       
        return $requestString;
    }
    
    /**
     * Returns available shipping methods for current request based on pricing method
     * specified in Temando Settings
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result|Mage_Shipping_Model_Rate_Result_Error
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
	$result = Mage::getModel('shipping/rate_result');
	/* @var $result Mage_Shipping_Model_Rate_Result */
	
	//check origin/destination country
        if (!$this->_canShip($request)) { 
	    return $this->_getErrorMethod(self::ERR_INVALID_COUNTRY);
	}

	//OneStepCheckout inserts '-' in city/pcode if no default configured
        if (!$request->getDestCountryId() || !$request->getDestPostcode() || !$request->getDestCity() ||
		$request->getDestPostcode() == '-' || $request->getDestCity() == '-') {
	    return $this->_getErrorMethod(self::ERR_INVALID_DEST);
        }
	
	//check if eligible for free shipping
        if ($this->isFreeShipping($request)) {
            $result->append($this->_getFlatRateMethod('0.00', true));
	    return $result;
        }
	
	//get available origin location
	$origin = Mage::getResourceModel('temando/warehouse_collection')
			->getOriginByPostcode($request->getDestPostcode(), $request->getDestCountryId(), Mage::app()->getStore()->getId());
	/* @var $origin Temando_Temando_Model_Warehouse */
	if(!$origin) {
	    //did not find any origins which could serve current request
	    return $this->_getErrorMethod(self::ERR_NO_ORIGIN);
	}

	//prepare extras
        $insurance = Mage::getModel('temando/option_insurance')->setSetting(Mage::getStoreConfig('temando/insurance/status'));
        $carbon = Mage::getModel('temando/option_carbonoffset')->setSetting(Mage::getStoreConfig('temando/carbon/status'));
	$footprints = Mage::getModel('temando/option_footprints')->setSetting(Mage::getStoreConfig('temando/footprints/status'));
        
        if ($this->getIsProductPage() || $this->getIsCartPage()) 
	{
            if (!in_array($insurance->getForcedValue(), array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO))) {
                $insurance->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
            }

            if (!in_array($carbon->getForcedValue(), array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO))) {
                $carbon->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
            }
	    
	    if (!in_array($footprints->getForcedValue(), array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO))) {
                $footprints->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
            }
        }
        /* @var Temando_Temando_Model_Options $options */
        $options = Mage::getModel('temando/options')->addItem($insurance)->addItem($carbon)->addItem($footprints);

	//get magento sales quote & id
	$salesQuote = Mage::getSingleton('checkout/session')->getQuote();
	/* @var $salesQuote Mage_Sales_Model_Quote */
	if (!$salesQuote->getId() && Mage::app()->getStore()->isAdmin()) {
	    $salesQuote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
	}
	if (!$salesQuote->getId() && $this->getIsProductPage()) {
	    $salesQuote = Mage::helper('temando')->getDummySalesQuoteFromRequest($request);
	}
	$salesQuoteId = $salesQuote->getId();

	//save current extras
        if (is_null(Mage::registry('temando_current_options'))) {
            Mage::register('temando_current_options', $options);
        }
	
	//init rule engine, preload applicable rules
	$ruleEngine = Mage::getModel('temando/hybrid')->loadRules($salesQuote);
	/* @var $ruleEngine Temando_Temando_Model_Hybrid */
	if (!$ruleEngine->hasValidRules()) {
	    //no valid rules => no available shipping methods
	    return $this->_getErrorMethod(self::ERR_NO_METHODS);
	}
	if ($ruleEngine->hasRestrictive()) {
	    //shipping to current destination is restricted, show custom message
	    return $this->_getErrorMethod($ruleEngine->getRestrictiveNote());
	}

	//get available quotes from the API for current request
	$quotes = array();
	if ($ruleEngine->hasDynamic()) {
	    //check if current request same as previous & dynamic rules configured
	    $lastRequest = Mage::getSingleton('checkout/session')->getTemandoRequestString();
	    if ($lastRequest == $this->_createRequestString($request, $salesQuoteId)) 
	    {
		//request is the same as previous, load existing quotes from DB
		$quotes = Mage::getModel('temando/quote')->getCollection()
			->addFieldToFilter('magento_quote_id', $salesQuoteId)
			->getItems();
	    } 
	    else 
	    {  
		try {
		    $apiRequest = Mage::getModel('temando/api_request');
		    /* @var $apiRequest Temando_Temando_Model_Api_Request */
		    $apiRequest
			->setUsername($this->getConfigData('general/username'))
			->setPassword($this->getConfigData('general/password'))
			->setSandbox($this->getConfigData('general/sandbox'))
			->setMagentoQuoteId($salesQuoteId)
			->setDestination(
			    $request->getDestCountryId(),
			    $request->getDestPostcode(),
			    $request->getDestCity(),
			    $request->getDestStreet())
			->setOrigin($origin->getName())
			->setItems($request->getAllItems())
			->setReady()
			->setAllowedCarriers($this->getAllowedMethods());
		    
		    $coll = $apiRequest->getQuotes();
		    if ($coll instanceof Temando_Temando_Model_Mysql4_Quote_Collection) {
			$quotes = $coll->getItems();
		    }
		} catch (Exception $e) {
		    switch(Mage::helper('temando')->getConfigData('options/error_process')) {
			case Temando_Temando_Model_System_Config_Source_Errorprocess::VIEW:
			    return $this->_getErrorMethod($e->getMessage());
			    break;
			case Temando_Temando_Model_System_Config_Source_Errorprocess::FLAT:
			    $result->append($this->_getFlatRateMethod());
			    return $result;
			    break;
		    }
		}
	    }
	}
	//get shipping methods
	$shippingMethods = $ruleEngine->getShippingMethods($options, $quotes);
	//set hasDynamic for checkout shipping methods template
	if ($ruleEngine->hasDynamic() && !empty($quotes)) {
	    Mage::register('temando_has_dynamic_shipping_methods', true, true);    
	}
	if (empty($shippingMethods)) {
	    return $this->_getErrorMethod(self::ERR_NO_METHODS);
	} else {
	    foreach ($shippingMethods as $method) {
		$result->append($method);
	    }
	}
	
        Mage::getSingleton('checkout/session')->setTemandoRequestString($this->_createRequestString($request, $salesQuoteId));
        return $result;
    }
    
    /**
     * Returns true if request is elegible for free shipping, false otherwise
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return boolean
     */
    public function isFreeShipping($request)
    {
	//check if all items have free shipping or free shipping over amount enabled and valid for this request
	$allItemsFree = true; $total = 0;
        foreach ($request->getAllItems() as $item) {
	    /* @var $item Mage_Sales_Model_Quote_Item */
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) { continue; }
            if ($item->getFreeShipping()) { continue; }
	    
	    $value = $item->getValue();
            if (!$value) { $value = $item->getRowTotalInclTax(); }
	    if (!$value) { $value = $item->getRowTotal(); }	    
            if (!$value) {
                $qty = $item->getQty() ? $item->getQty() : $item->getQtyOrdered();
                $value = $item->getPrice() * $qty;
            }
	    $total += $value;
	    //not all items with free shipping if here
            $allItemsFree = false;
        }
	
	if($allItemsFree ||
		($this->getConfigData('free_shipping_enable') && $total >= $this->getConfigData('free_shipping_subtotal'))) {
	     return true;
	}
	
	return false;
    }

    /**
     * Return list of allowed carriers
     * 
     * @return array
     */
    public function getAllowedMethods()
    {
        return explode(',', Mage::getStoreConfig('carriers/temando/allowed_methods'));
    }

    public function getTrackingInfo($tracking_number)
    {
        $api = Mage::getModel('temando/api_client');
        $api->connect(
            Mage::helper('temando')->getConfigData('general/username'),
            Mage::helper('temando')->getConfigData('general/password'),
            Mage::helper('temando')->getConfigData('general/sandbox'));

        $_t = explode('Request Id: ', $tracking_number);
        if (isset($_t[1])) {
            $tracking_number = $_t[1];
        }

        $status = $api->getRequest(array('requestId' => $tracking_number));
        
        $result = Mage::getModel('shipping/tracking_result_abstract')
            ->setTracking($tracking_number);
        /* @var $result Mage_Shipping_Model_Tracking_Result_Abstract */
        if ($status && $status->request->quotes && $status->request->quotes->quote) {
            if (isset($status->request->quotes->quote->carrier->companyName)) {
                $result->setCarrierTitle($status->request->quotes->quote->carrier->companyName);
            }

            if (isset($status->request->quotes->quote->trackingStatus)) {
                $result->setStatus($status->request->quotes->quote->trackingStatus);
            } else {
                $result->setStatus(Mage::helper('temando')->__('Unavailable'));
            }
            
            $text = '';
            if (isset($status->request->quotes->quote->trackingFurtherDetails)) {
                $text .= $status->request->quotes->quote->trackingFurtherDetails;
            }
            if (isset($status->request->quotes->quote->trackingLastChecked)) {
                $text .= 'Last Update: ' . date('Y-m-d h:ia', strtotime($status->request->quotes->quote->trackingLastChecked));
            }
            
            if ($text) {
                $result->setTrackSummary($text);
            }
        } else {
            $result->setErrorMessage(Mage::helper('temando')->__('An error occurred while fetching the shipment status.'));
        }
        
        return $result;
    }
    
    /**
     * Retrieve information from carrier configuration
     *
     * @param   string $field
     * @return  mixed
     */
    public function getConfigData($field)
    {
	$temando = Mage::helper('temando')->getConfigData($field);
        $parent = parent::getConfigData($field);
        return $temando !== null ? $temando : $parent; 
    }
    
    /**
     * Returns Temando carrier code
     * 
     * @return string
     */
    static public function getCode()
    {
        return self::CARRIER_CODE;
    }
    
    /**
     * Returns Temando carrier title
     * 
     * @return string
     */
    static public function getTitle()
    {
	return self::CARRIER_TITLE;
    }

    /**
     * Is state province required
     *
     * @return bool
     */
    public function isStateProvinceRequired()
    {
        return false;
    }

    /**
     * Check if city option required
     *
     * @return boolean
     */
    public function isCityRequired()
    {
        return true;
    }

    /**
     * Determine whether zip-code is required for the country of destination
     *
     * @param string|null $countryId
     * @return bool
     */
    public function isZipCodeRequired($countryId = null)
    {
        return true;
    }
    
}
