<?php

class Temando_Temando_Model_Hybrid extends Mage_Core_Model_Abstract
{
    /**
     * Pricing Method Code 
     */
    const METHOD_CODE  = 'engine';
    
    /**
     * All valid rules filtered by current conditions
     * @var array 
     */
    protected $_validRules;
    
    
    /**
     * Flag if any of the valid rules are dynamic
     * @var boolean 
     */
    protected $_hasDynamic = null;
    
    
    /**
     * Flag if any of the valid rules is restrictive (shipping not allowed)
     * @var boolean 
     */
    protected $_hasRestrictive = null;
    
    
    /**
     * Text to display to customer when restrictive rule setup
     * @var string 
     */
    protected $_restrictiveNote = '';
    
    
    /**
     * @var Temando_Temando_Helper_Data 
     */
    protected $_helper;
       

    public function _construct()
    {
        parent::_construct();
        $this->_init('temando/hybrid');
        $this->_validRules = array();
	$this->_helper = Mage::helper('temando');
    }
    
    /**
     * Loads all rules based on the current request conditions
     * 
     * @param Mage_Sales_Model_Quote $quote
     * @return \Temando_Temando_Model_Hybrid 
     */
    public function loadRules(Mage_Sales_Model_Quote $quote)
    {
	$collection = Mage::getModel('temando/rule')->getCollection();
	/* @var $collection Temando_Temando_Model_Mysql4_Rule_Collection */
	
	$collection->addFieldToFilter('is_active', '1')
		   ->setOrder('priority', 'ASC');
	
	//admin - manual order creation
	if(Mage::app()->getStore()->isAdmin()) {
	    $store_id = Mage::app()->getRequest()->getParam('store_id');
	} else {
	    $store_id = Mage::app()->getStore(null)->getId();
	}
	$group_id = Mage::getSingleton('customer/session')->getCustomerGroupId();
	foreach($collection->getItems() as $rule) {
	    /* @var $rule Temando_Temando_Model_Rule */
	    if(!$rule->validateDate()) {
		continue;
	    }
	    
	    $store_ids = explode(',', $rule->getStoreIds());
	    $group_ids = explode(',', $rule->getGroupIds());
	    if(in_array($store_id, $store_ids) && in_array($group_id, $group_ids) && $rule->isValid($quote))
	    {
		$this->_validRules[] = $rule;
	    }
	}
	
	return $this;
    }
    
    /**
     * Is there a valid shipping rule which can return shipping method?
     * 
     * @return boolean
     */
    public function hasValidRules()
    {
	return !empty($this->_validRules);
    }
    
    /**
     * Checks if there is a dynamic rule which needs to be processed
     * 
     * @return boolean true if dynamic rule exist, 
     * false otherwise or when rules are not loaded
     */
    public function hasDynamic()
    {
	if(is_null($this->_hasDynamic))
	{
	    $this->_hasDynamic = false;
	    if(!empty($this->_validRules)) {
		foreach($this->_validRules as $rule) {
		    if($rule->isDynamic())
		    {
			$this->_hasDynamic = true;
			break;
		    }
		}
	    }
	}	
	return $this->_hasDynamic;
    }
    
    /**
     * Checks if there is a restrictive rule which cancels all
     * 
     * @return boolean true if restrictive rule exist, 
     * false otherwise or when rules are not loaded
     */
    public function hasRestrictive()
    {
	if(is_null($this->_hasRestrictive))
	{
	    $this->_hasRestrictive = false;
	    if(!empty($this->_validRules)) {
		foreach($this->_validRules as $rule) {
		    if($rule->isRestrictive())
		    {
			$this->_hasRestrictive = true;
			$this->_restrictiveNote = $rule->getActionRestrictNote();
			break;
		    }
		}
	    }
	}	
	return $this->_hasRestrictive;
    }
    
    /**
     * Returns the custom message configured on restrictive rule
     * 
     * @return string
     */
    public function getRestrictiveNote()
    {
	return $this->_restrictiveNote;
    }
    
    /**
     * Returns list of available shipping methods (Mage_Shipping_Model_Rate_Result_Method)
     * 
     * @param Temando_Temando_Model_Options $options
     * @param array $quotes
     * @return array List of available shipping method rates
     */
    public function getShippingMethods($options, $quotes)
    {
	$methods = array(); $stopOnNext = false; $stopPriorityAfter = null;
	foreach($this->_validRules as $rule) {
	    /* @var $rule Temando_Temando_Model_Rule */
	    $priority = $rule->getPriority();
	    //stop if previous rule has stopOther and higher priority
	    if($stopOnNext && $priority > $stopPriorityAfter) break;
	    if($rule->isDynamic()) {
		//can't build ship method from dynamic rule without quotes
		if(empty($quotes)) continue;
		$ruleQuotes = $this->_processDynamicRule($rule, $quotes);
		//check if we have any quotes left after we've applied filters
		if(empty($ruleQuotes)) continue;	
		//process quotes - apply extras and create methods
		foreach($ruleQuotes as $ruleQuote) {
		    /* @var $ruleQuote Temando_Temando_Model_Quote */
		    $permutations = $options->applyAll($ruleQuote);   
		    foreach ($permutations as $permutation_id => $permutation) {
			$title = $permutation->getDescription($rule->getActionDynamicShowCarrierName(), $rule->getActionDynamicShowCarrierMethod(),
							      $rule->getActionDynamicShowCarrierTime(), $rule->getActionDynamicLabel());
			$methods[]  = Mage::getModel('shipping/rate_result_method')
				    ->setCarrier(Temando_Temando_Model_Shipping_Carrier_Temando::getCode())
				    ->setCarrierTitle(Temando_Temando_Model_Shipping_Carrier_Temando::getTitle())
				    ->setMethodTitle($title)
				    ->setMethod($ruleQuote->getId().'_'.$rule->getId().'_'. $permutation_id)
				    ->setPrice($permutation->getTotalPrice())
				    ->setCost($permutation->getTotalPrice());
		    }
		}		
	    } else {
		switch($rule->getActionRateType()) {
		    case Temando_Temando_Model_System_Config_Source_Rule_Type::FLATRATE:
			$methodCode = Temando_Temando_Model_Carrier::FLAT_RATE . '_' . $rule->getId(); 
			$price = $rule->getActionStaticValue();
			break;
		    default:
			$methodCode = Temando_Temando_Model_Carrier::FREE . '_' . $rule->getId();
			$price = 0;
			break;
		}
		$methods[] = Mage::getModel('shipping/rate_result_method')
			    ->setCarrier(Temando_Temando_Model_Shipping_Carrier_Temando::getCode())
			    ->setCarrierTitle(Temando_Temando_Model_Shipping_Carrier_Temando::getTitle())
			    ->setMethodTitle($rule->getActionStaticLabel())
			    ->setMethod($methodCode)
			    ->setPrice($price)
			    ->setCost($price);
	    }
	    //stop further rules processing check
	    if($rule->getStopOther()) {
		$stopOnNext = true;	
		$stopPriorityAfter = $priority;
	    }
	}
	return $methods;
    }
    
    /**
     * Applies rule filters and adjustments to available shipping quotes (carrier quotes from API)
     * 
     * @param Temando_Temando_Model_Rule $rule
     * @param array $quotes
     * @return array
     */
    protected function _processDynamicRule(Temando_Temando_Model_Rule $rule, $quotes)
    {
	//apply carrier filter
	$carriers = explode(',', $rule->getActionDynamicCarriers());
	$cleanQuotes = array();
	foreach($quotes as $quote) { 
	    /* @var $quote Temando_Temando_Model_Quote */
	    if(in_array($quote->getCarrier()->getCarrierId(), $carriers))
		$cleanQuotes[] = clone $quote;
	}
	if(empty($cleanQuotes)) return array();
	
	//apply pricing method filter
	switch($rule->getActionDynamicFilter()) {
	    case Temando_Temando_Model_System_Config_Source_Rule_Action_Filter::DYNAMIC_CHEAPEST:
		$cleanQuotes = Mage::helper('temando/functions')->getCheapestQuote($cleanQuotes);
		break;
	    case Temando_Temando_Model_System_Config_Source_Rule_Action_Filter::DYNAMIC_FASTEST:
		$cleanQuotes = Mage::helper('temando/functions')->getFastestQuote($cleanQuotes);
		break;	    
	    case Temando_Temando_Model_System_Config_Source_Rule_Action_Filter::DYNAMIC_FASTEST_AND_CHEAPEST:
		$cleanQuotes = Mage::helper('temando/functions')->getCheapestAndFastestQuotes($cleanQuotes);
		break;
	}
	if(!is_array($cleanQuotes)) { $cleanQuotes = array($cleanQuotes); }
	
	//process price adjustments
	$adjustment = $rule->getActionDynamicAdjustmentType();
	$value = $rule->getActionDynamicAdjustmentValue();
	if($adjustment) {
	    foreach($cleanQuotes as $cleanQuote) {
		/* @var $quote Temando_Temando_Model_Quote */
		$cleanQuote->processAdjustment((int)$adjustment, $value);
	    }
	}
	return $cleanQuotes;	
    }
}