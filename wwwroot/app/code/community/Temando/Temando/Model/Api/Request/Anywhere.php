<?php
/**
 * @method Temando_Temando_Model_Api_Request_Anywhere setDestinationCountry()
 * @method Temando_Temando_Model_Api_Request_Anywhere setDestinationPostcode()
 * @method Temando_Temando_Model_Api_Request_Anywhere setDestinationCity()
 * @method Temando_Temando_Model_Api_Request_Anywhere setDestinationStreet()
 * @method Temando_Temando_Model_Api_Request_Anywhere setOriginDescription()
 * 
 * @method string getDestinationCountry()
 * @method string getDestinationPostcode()
 * @method string getDestinationCity()
 * @method string getDestinationStreet()
 * @method string getOriginDescription()
 */
class Temando_Temando_Model_Api_Request_Anywhere extends Mage_Core_Model_Abstract
{
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('temando/api_request_anywhere');
    }
    
    public function toRequestArray()
    {
        if (!$this->validate()) {
            return false;
        }

        $data = array(
            'itemNature' => 'Domestic',
            'itemMethod' => 'Door to Door',
            'destinationCountry' => $this->getDestinationCountry(),
            'destinationCode' => $this->getDestinationPostcode(),
            'destinationSuburb' => $this->getDestinationCity(),
            'destinationIs' => 'Residence',
            'destinationBusNotifyBefore' => 'N',
            'destinationBusLimitedAccess' => 'N',
            'originBusNotifyBefore' => 'Y',
	    'originDescription' => $this->getOriginDescription()
        );

        if (Mage::helper('temando')->isStreetWithPO($this->getDestinationStreet())) {
            $data['destinationResPostalBox'] = 'Y';
        }

        return $data;
    }
    
    public function validate()
    {
        return
            $this->getDestinationCountry() &&
            $this->getDestinationPostcode() &&
            $this->getDestinationCity() &&
	    $this->getOriginDescription();
    }
    
}
