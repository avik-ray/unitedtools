<?php

class Temando_Temando_Model_Api_Request_Anything extends Mage_Core_Model_Abstract
{
    const GOODS_CLASS    = 'General Goods';
    const GOODS_SUBCLASS = 'Household Goods';
    const PALLET_TYPE	 = 'Plain';
    const PALLET_NATURE  = 'Not Required';
    
    
    /**
     * @var Mage_Sales_Model_Order_Item
     */
    protected $_item = null;
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('temando/api_request_anything');
    }
    
    public function setItem($item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item || $item instanceof Mage_Sales_Model_Order_Item || 
		$item instanceof Mage_Sales_Model_Quote_Address_Item || $item instanceof Temando_Temando_Model_Box) {
            $this->_item = $item;
        }
        return $this;
    }
    
    /**
     * Gets the order item for this Anything object.
     *
     * @return Mage_Sales_Model_Order_Item
     */
    public function getItem()
    {
        if ($this->_item) {
            return $this->_item;
        }
        return false;
    }
    
    public function toRequestArray()
    {
        if (!$this->validate()) {
            return false;
        }
        
	$anythings = array();
        if ($this->_item instanceof Temando_Temando_Model_Box) {
            $anythings[] = array(
                'class'         => self::GOODS_CLASS,
                'subclass'      => self::GOODS_SUBCLASS,
                'packaging'     => Mage::getModel('temando/system_config_source_shipment_packaging')->getOptionLabel($this->_item->getPackaging()),
                'quantity'      => (int)($this->_item->getQty()),
                'distanceMeasurementType' => Temando_Temando_Model_System_Config_Source_Unit_Measure::CENTIMETRES,
                'weightMeasurementType'   => Temando_Temando_Model_System_Config_Source_Unit_Weight::GRAMS,
                'weight'        => Mage::helper('temando')->getWeightInGrams($this->_item->getWeight(), $this->_item->getWeightUnit()),
                'length'        => Mage::helper('temando')->getDistanceInCentimetres($this->_item->getLength(), $this->_item->getMeasureUnit()),
                'width'         => Mage::helper('temando')->getDistanceInCentimetres($this->_item->getWidth(), $this->_item->getMeasureUnit()),
                'height'  	=> Mage::helper('temando')->getDistanceInCentimetres($this->_item->getHeight(), $this->_item->getMeasureUnit()),
                'qualifierFreightGeneralFragile'    => $this->_item->getFragile() == '1' ? 'Y' : 'N',
                'description'   => $this->_item->getComment()
            );

            if($this->_item->getPackaging() == Temando_Temando_Model_System_Config_Source_Shipment_Packaging::PALLET) {
		$anythings[0]['palletType']   = self::PALLET_TYPE;
		$anythings[0]['palletNature'] = self::PALLET_NATURE;
	    }
	    
        } else {
	    Mage::helper('temando')->applyTemandoParamsToItem($this->_item);
	    $articles = Mage::helper('temando')->getProductArticles($this->_item);
	    foreach ($articles as $article) {
		$anythings[] = array(
		    'class'			=> self::GOODS_CLASS,
		    'subclass'			=> self::GOODS_SUBCLASS,
		    'packaging'			=> Mage::getModel('temando/system_config_source_shipment_packaging')->getOptionLabel($article['packaging']),
		    'quantity'			=> (int)($this->_item->getQty() ? $this->_item->getQty() : $this->_item->getQtyOrdered()),
		    'distanceMeasurementType'	=> Temando_Temando_Model_System_Config_Source_Unit_Measure::CENTIMETRES,
		    'weightMeasurementType'	=> Temando_Temando_Model_System_Config_Source_Unit_Weight::GRAMS,
		    'weight'			=> Mage::helper('temando')->getWeightInGrams($article['weight'],Mage::helper('temando')->getConfigData('units/weight')),
		    'length'			=> Mage::helper('temando')->getDistanceInCentimetres($article['length'], Mage::helper('temando')->getConfigData('units/measure')),
		    'width'			=> Mage::helper('temando')->getDistanceInCentimetres($article['width'], Mage::helper('temando')->getConfigData('units/measure')),
		    'height'			=> Mage::helper('temando')->getDistanceInCentimetres($article['height'], Mage::helper('temando')->getConfigData('units/measure')),
		    'qualifierFreightGeneralFragile' => $article['fragile'] == '1' ? 'Y' : 'N',
		    'description'		=> $article['description']
		);
		if($article['packaging'] == Temando_Temando_Model_System_Config_Source_Shipment_Packaging::PALLET) {
		    $anythings[count($anythings)-1]['palletType']   = self::PALLET_TYPE;
		    $anythings[count($anythings)-1]['palletNature'] = self::PALLET_NATURE;
		}
	    }
	} 
        return $anythings;
    }    
    public function validate()
    {
        return $this->_item instanceof Mage_Sales_Model_Quote_Item ||
            $this->_item instanceof Mage_Sales_Model_Order_Item ||
	    $this->_item instanceof Mage_Sales_Model_Quote_Address_Item ||
            $this->_item instanceof Temando_Temando_Model_Box;
    }

}
