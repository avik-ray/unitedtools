<?php

class Temando_Temando_Model_Mysql4_Rule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    
    /**
     * List of product attribute codes currently used within temando rules
     * 
     * @var array
     */
    protected $_usedProductAttributes = null;
    
    
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('temando/rule');
    }
    
    /**
     * Returns list of currently used attributes from all active rules
     * 
     * @return type 
     */
    public function getUsedProductAttributes()
    {
	if(!$this->_usedProductAttributes) {
	    $this->_usedProductAttributes = array();
	    $this->addFieldToFilter('is_active', '1')->setOrder('priority', 'ASC')->load();
	    foreach($this->getItems() as $rule) {
		/* @var $rule Temando_Temando_Model_Rule */
		if($rule->getConditionsSerialized())
		{
		    $attribCodes = $this->getProductAttributes($rule->getConditionsSerialized());
		    $this->_usedProductAttributes = array_merge($this->_usedProductAttributes, $attribCodes);
		}
	    }
	}
	return $this->_usedProductAttributes;
    }
    
    /**
     * Collect all product attributes used in serialized rule's action or condition
     *
     * @param string $serializedString
     *
     * @return array
     */
    public function getProductAttributes($serializedString)
    {
        $result = array();
        if (preg_match_all('~s:30:"temando/rule_condition_product";s:9:"attribute";s:\d+:"(.*?)"~s',
            $serializedString, $matches)){
            foreach ($matches[1] as $offset => $attributeCode) {
                $result[] = $attributeCode;
            }
        }

        return $result;
    }
    
    
}