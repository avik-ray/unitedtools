<?php

class Temando_Temando_Model_Rule extends Mage_Rule_Model_Abstract
{
    /**
     * Formatted Conditions of this rule
     * @var array 
     */
    protected $_conditions;
    
    
    public function _construct()
    {
	$this->_init('temando/rule');
    }
    
    /**
     * Get rule condition combine model instance
     *
     * @return Temando_Temando_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('temando/rule_condition_combine');
    }
    
    /**
     * Get rule condition product combine model instance
     * 
     * @return Temando_Temando_Model_Rule_Condition_Product_Combine
     */
    public function getActionsInstance()
    {
        return Mage::getModel('temando/rule_condition_product_combine');
    }	
    
    /**
     * Check if rule satisfies current conditions
     * 
     * @param Varien_Object $object
     * @return boolean 
     */
    public function isValid(Varien_Object $object)
    {
	return parent::validate($object) && $this->_validateDayCondition() && $this->_validateTimeCondition();
    }
    
    /**
     * Check if time condition is valid for current quote. If time is set as a 
     * condition of this rule, it's compared to the current store time.
     * 
     * @return boolean
     */
    private function _validateTimeCondition()
    {
	$return = false;
	if($this->getData('condition_time_type')) {
	    //time condition set - validate
	    $current_time = Mage::app()->getLocale()->storeTimeStamp(Mage::app()->getStore()->getId());
	    $config = new Zend_Date($current_time);
	    $config->settime(str_replace(',', ':', $this->getData('condition_time_value')));
	    $config_time = $config->getTimestamp();
	    
	    switch($this->getData('condition_time_type')) {
		case Temando_Temando_Model_System_Config_Source_Rule_Condition_Time::BEFORE:
		    if($current_time < $config_time)
			$return = true;
		    break;
		case Temando_Temando_Model_System_Config_Source_Rule_Condition_Time::AFTER:
		    if($current_time >= $config_time)
			$return = true;
		    break;
		
		default:
		    $return = true;
		    break;
	    }
	} else {
	    //no condition set
	    $return = true;
	}	
	return $return;
    }
    
    /**
     * Check if day condition is valid for current quote. If day is set as a 
     * condition of this rule, it's compared to the current store day.
     * 
     * @return boolean
     */
    protected function _validateDayCondition()
    {
	$current_date = Mage::app()->getLocale()->storeDate(Mage::app()->getStore()->getId());
	$today = new Zend_Date($current_date, Varien_Date::DATE_INTERNAL_FORMAT);	

	
	$validDays = explode(',', $this->getData('condition_day'));
	
	if(is_array($validDays) && count($validDays) === 1 && !strlen(trim($validDays[0]))) 
	    return true;

	foreach($validDays as $validDay) {
	    if($today->get(Zend_Date::WEEKDAY_DIGIT) == $validDay)
		return true;
	}
	
	return false;
    }
    
    /**
     * Validates current date against the configured range (from/to)
     * of this rule
     * 
     * @return boolean true if now is within dates, false otherwise
     */
    public function validateDate()
    {
	$current_date = Mage::app()->getLocale()->storeDate(Mage::app()->getStore()->getId());
	/* @var $current_date Zend_Date */
	if($fromDate = $this->getData('from_date')) {
	    $from = new Zend_Date($fromDate, Varien_Date::DATE_INTERNAL_FORMAT);
	    if($current_date->compareDate($from) === -1)
		return false;
	}
	
	if($toDate = $this->getData('to_date')) {
	    $to = new Zend_Date($toDate, Varien_Date::DATE_INTERNAL_FORMAT);
	    if($current_date->compareDate($to) === 1)
		return false;
	}
	
	return true;
    }
    
    /**
     * Does this rule return flat rate or free shipping?
     * 
     * @return boolean
     */
    public function isStatic()
    {
	return $this->getActionRateType() == Temando_Temando_Model_System_Config_Source_Rule_Type::FLATRATE ||
	       $this->getActionRateType() == Temando_Temando_Model_System_Config_Source_Rule_Type::FREE;
    }
    
    /**
     * Does this rule return dynamic carrier quote?
     * 
     * @return boolean
     */
    public function isDynamic()
    {
	if($this->getActionRateType() == Temando_Temando_Model_System_Config_Source_Rule_Type::DYNAMIC) {
	    return true;
	}
	return false;
    }
    
    /**
     * Does this rule restrict shipping?
     * 
     * @return boolean
     */
    public function isRestrictive()
    {
	if($this->getActionRateType() == Temando_Temando_Model_System_Config_Source_Rule_Type::RESTRICT) {
	    return true;
	}
	return false;
    }
    
    /**
     * Validates rule data. Runs on model save.
     * Returns array of errors if data not valid, true otherwise.
     *
     * @param Varien_Object $object
     * @return bool|array
     */
    public function validateData(Varien_Object $object)
    {
        $result   = array();
        $fromDate = $toDate = null;

        if ($object->hasFromDate() && $object->hasToDate()) {
            $fromDate = $object->getFromDate();
            $toDate = $object->getToDate();
        }

        if ($fromDate && $toDate) {
            $fromDate = new Zend_Date($fromDate, Varien_Date::DATE_INTERNAL_FORMAT);
            $toDate = new Zend_Date($toDate, Varien_Date::DATE_INTERNAL_FORMAT);
	    
            if ($fromDate->compare($toDate) === 1) {
                $result[] = Mage::helper('rule')->__('End Date must be greater than Start Date.');
            }
        }

        if ($object->hasWebsiteIds()) {
            $websiteIds = $object->getWebsiteIds();
            if (empty($websiteIds)) {
                $result[] = Mage::helper('rule')->__('Websites must be specified.');
            }
        }
        if ($object->hasCustomerGroupIds()) {
            $customerGroupIds = $object->getCustomerGroupIds();
            if (empty($customerGroupIds)) {
                $result[] = Mage::helper('rule')->__('Customer Groups must be specified.');
            }
        }

        return !empty($result) ? $result : true;
    }
}