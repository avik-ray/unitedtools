<?php

class Temando_Temando_Block_Adminhtml_Package_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setDefaultSort('name');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('temando/package')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
	
	$this->addColumn('name', array(
            'header'    => Mage::helper('temando')->__('Name'),
            'index'     => 'name',
        ));
	
        $this->addColumn('packaging', array(
            'header' => Mage::helper('temando')->__('Packaging Type'),
            'index' => 'packaging',
	    'renderer' => new Temando_Temando_Block_Adminhtml_Package_Grid_Renderer_Type()
        ));
	
	$this->addColumn('length', array(
            'header'    => Mage::helper('temando')->__('Length'),
            'index'     => 'length',
        ));
	
	$this->addColumn('width', array(
            'header'    => Mage::helper('temando')->__('Width'),
            'index'     => 'width',
        ));
	
	$this->addColumn('height', array(
            'header'    => Mage::helper('temando')->__('Height'),
            'index'     => 'height',
        ));
	
	$this->addColumn('measure_unit', array(
            'header'    => Mage::helper('temando')->__('Measure Unit'),
            'index'     => 'measure_unit',
        ));
	
	$this->addColumn('weight', array(
            'header'    => Mage::helper('temando')->__('Weight'),
            'index'     => 'weight',
        ));
	
	$this->addColumn('weight_unit', array(
            'header'    => Mage::helper('temando')->__('Weight Unit'),
            'index'     => 'weight_unit',
        ));

        return parent::_prepareColumns();
    }    
    

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}

