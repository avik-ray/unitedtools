<?php

class Temando_Temando_Block_Adminhtml_Warehouse extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function _construct()
    {
        $this->_blockGroup = 'temando';
        $this->_controller = 'adminhtml_warehouse';
        $this->_headerText = $this->__('Manage Warehouses');
	$this->_addButtonLabel = $this->__('Add New Warehouse');
        parent::_construct();	
    }
}


