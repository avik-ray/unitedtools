<?php

class Temando_Temando_Block_Adminhtml_Package_Edit_Tab_General
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('temando')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('temando')->__('General');
    }

    /**
     * Returns status flag about this tab can be showed or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('current_temando_package');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('package_');

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend' => Mage::helper('temando')->__('Package Information'))
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => Mage::helper('temando')->__('Name'),
            'title' => Mage::helper('temando')->__('Name'),
            'required' => true,
        ));
	
	$fieldset->addField('packaging', 'select', array(
	    'name' => 'packaging',
	    'label' => Mage::helper('temando')->__('Packaging Type'),
	    'title' => Mage::helper('temando')->__('Packaging Type'),
	    'options' => Mage::getSingleton('temando/system_config_source_shipment_packaging')->getOptions(),
	    'required' => true,
	));
	
	$fieldset->addField('length', 'text', array(
            'name' => 'length',
            'label' => Mage::helper('temando')->__('Length'),
            'title' => Mage::helper('temando')->__('Length'),
	    'class' => 'validate-number',
            'required' => true,
        ));
	
	$fieldset->addField('width', 'text', array(
            'name' => 'width',
            'label' => Mage::helper('temando')->__('Width'),
            'title' => Mage::helper('temando')->__('Width'),
	    'class' => 'validate-number',
            'required' => true,
        ));
	
	$fieldset->addField('height', 'text', array(
            'name' => 'height',
            'label' => Mage::helper('temando')->__('Height'),
            'title' => Mage::helper('temando')->__('Height'),
	    'class' => 'validate-number',
            'required' => true,
        ));
	
	$fieldset->addField('measure_unit', 'select', array(
            'name' => 'measure_unit',
            'label' => Mage::helper('temando')->__('Measure Unit'),
            'title' => Mage::helper('temando')->__('Measure Unit'),
            'options' => Mage::getModel('temando/system_config_source_unit_measure')->getOptions(),
	    'required' => true,
        ));
	
	$fieldset->addField('weight', 'text', array(
            'name' => 'weight',
            'label' => Mage::helper('temando')->__('Weight'),
            'title' => Mage::helper('temando')->__('Weight'),
	    'class' => 'validate-number',
        ));
	
	$fieldset->addField('weight_unit', 'select', array(
            'name' => 'weight_unit',
            'label' => Mage::helper('temando')->__('Weight Unit'),
            'title' => Mage::helper('temando')->__('Weight Unit'),
            'options' => Mage::getModel('temando/system_config_source_unit_weight')->getOptions(true),
        ));
	
	
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
