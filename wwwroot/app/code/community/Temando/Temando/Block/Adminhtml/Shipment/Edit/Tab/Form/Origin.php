<?php

class Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Form_Origin extends Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Abstract
{
    
    public function getWarehouseOptions() {
	
	$return = array('' => ' -- ');
	$warehouseCollection = Mage::getResourceModel('temando/warehouse_collection');
	foreach($warehouseCollection as $warehouse) {
	    $return[$warehouse->getId()] = $warehouse->getTitle();
	}
	
	return $return;
    }
    
}