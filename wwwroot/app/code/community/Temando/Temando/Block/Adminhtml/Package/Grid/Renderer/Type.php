<?php
/**
 * Custom grid renderer for temando packaging type 
 */
class Temando_Temando_Block_Adminhtml_Package_Grid_Renderer_Type extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text {
    
    public function _getValue(Varien_Object $row)
    {
        $package = Mage::getModel('temando/package')->load($row->getId());
	
	return Mage::getModel('temando/system_config_source_shipment_packaging')
                                       ->getOptionLabel($package->getPackaging());
    }   
}
