<?php

class Temando_Temando_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('temando/rule')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header' => Mage::helper('temando')->__('Rule Name'),
	    'width' => '350px',
            'index' => 'name',
        ));

        $this->addColumn('is_active', array(
            'header' => Mage::helper('temando')->__('Status'),
	    'width' => '50px',
	    'index' => 'is_active',
	    'type' => 'options',
	    'options'   => array(
                1 => 'Enabled',
                0 => 'Disabled',
            ),           
        ));
	
	$this->addColumn('from_date', array(
            'header'    => Mage::helper('salesrule')->__('Date Start'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'index'     => 'from_date',
        ));

        $this->addColumn('to_date', array(
            'header'    => Mage::helper('salesrule')->__('Date Expire'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'to_date',
        ));
        
        $this->addColumn('action_rate_type', array(
            'header' => Mage::helper('temando')->__('Type'),
            'index' => 'action_rate_type',
            'type'  => 'options',
            'width' => '50px',
            'options' => Mage::getSingleton('temando/system_config_source_rule_type')->getOptions(),
        ));
	
	$this->addColumn('priority', array(
	    'header' => Mage::helper('temando')->__('Priority'),
	    'index' => 'priority',
	    'type'  => 'text',
	    'width' => '50px',
	));
	
	$this->addColumn('stop_other', array(
	    'header' => Mage::helper('temando')->__('Stop Other Rules'),
	    'index' => 'stop_other',
	    'type'  => 'options',
	    'width' => '50px',
	    'options' => array(
		0 => 'No',
		1 => 'Yes',
	    ),
	));
        
        return parent::_prepareColumns();
    }    

    protected function _prepareMassaction() {
	parent::_prepareMassaction();
	
	$this->getMassactionBlock()->addItem('enable_all', array(
            'label'=> Mage::helper('temando')->__('Enable Rules'),
            'url'  => $this->getUrl('*/*/massEnable'),
        ));
	
	$this->getMassactionBlock()->addItem('disable_all', array(
            'label'=> Mage::helper('temando')->__('Disable Rules'),
            'url'  => $this->getUrl('*/*/massDisable'),
        ));
	
	$this->getMassactionBlock()->addItem('delete_all', array(
            'label'=> Mage::helper('temando')->__('Delete Rules'),
            'url'  => $this->getUrl('*/*/massDelete'),
	    'confirm' => Mage::helper('temando')->__('Are you sure you want to delete selected shipping rules?'),
        ));
	
	$this->setMassactionIdField('id');	
	$this->getMassactionBlock()->setUseSelectAll(false);

        return $this;
    }    

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}

