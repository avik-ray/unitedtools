<?php

class Temando_Temando_Block_Adminhtml_Warehouse_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function _construct()
    {
	parent::_construct();
	$this->setDefaultSort('name');
        $this->setDefaultDir('DESC');
	$this->setSaveParametersInSession(true);
    }
    
    protected function _prepareCollection() {
	$collection = Mage::getModel('temando/warehouse')->getCollection();
	$this->setCollection($collection);
	parent::_prepareCollection();
    }
    
    protected function _prepareColumns() {
	$this->addColumn('name', array(
	    'header' => Mage::helper('temando')->__('Name'),
	    'index'  => 'name',
	));
	
	$this->addColumn('is_active', array(
            'header' => Mage::helper('temando')->__('Status'),
	    'width' => '50px',
	    'index' => 'is_active',
	    'type' => 'options',
	    'options'   => array(
                1 => 'Enabled',
                0 => 'Disabled',
            ),           
        ));
	
	$this->addColumn('street', array(
	    'header' => Mage::helper('temando')->__('Street'),
	    'index'  => 'street',
	));
	
	$this->addColumn('city', array(
	    'header' => Mage::helper('temando')->__('City'),
	    'index'  => 'city',
	));
	
	$this->addColumn('postcode', array(
	    'header' => Mage::helper('temando')->__('ZIP/Postcode'),
	    'index'  => 'postcode',
	));
	
	$this->addColumn('region', array(
	    'header' => Mage::helper('temando')->__('Region'),
	    'index'  => 'region',
	));
	
	$this->addColumn('country', array(
	    'header' => Mage::helper('temando')->__('Country'),
	    'index'  => 'country',
	));
	
	parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }    
    
    
}
