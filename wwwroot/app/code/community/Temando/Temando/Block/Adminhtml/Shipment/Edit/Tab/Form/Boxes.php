<?php

class Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Form_Boxes extends Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Abstract
{
    
    /**
     * Array of all available packaging types (default & custom)
     * 
     * @var array 
     */
    protected $_packagingTypes = null;   
    
    /**
     * Array of all custom packaging types with definitions
     * 
     * @var array 
     */
    protected $_customPackages = null;
    
    /**
     * Returns array of all available packaging types 
     * 
     * @return array
     */
    public function getPackagingTypes()
    {
	if(is_null($this->_packagingTypes)) {
	    $this->_packagingTypes = Mage::getModel('temando/system_config_source_shipment_packaging')->toOptionArray();
	    $customPackageCol = Mage::getResourceModel('temando/package_collection');
	    foreach ($customPackageCol->getItems() as $custPackage)
	    {
		$this->_packagingTypes[] = array(
		    'value' => 'custom_' . $custPackage->getId(),
		    'label' => $this->__($custPackage->getName())
		);
	    }
	}
	
	return $this->_packagingTypes;
    }
    
    /**
     * Returns JSON encoded array of all custom packaging types
     * 
     * @return string
     */
    public function getCustomPackages()
    {
	if (is_null($this->_customPackages)) {
	    $customPackageCol = Mage::getResourceModel('temando/package_collection');
	    foreach ($customPackageCol->getItems() as $custPackage)
	    {
		$this->_customPackages['custom_' . $custPackage->getId()] = $custPackage->toArray();
	    }
	}
	return Mage::helper('core')->jsonEncode($this->_customPackages);
    }
    
}
