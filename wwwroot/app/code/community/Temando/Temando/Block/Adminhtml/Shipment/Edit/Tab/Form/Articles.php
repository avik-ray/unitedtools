<?php

class Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Form_Articles 
    extends Temando_Temando_Block_Adminhtml_Shipment_Edit_Tab_Abstract
{
    /**
     * Gets label for packaging type by index number
     * 
     * @param int $packagingType
     * @return string
     */
    public function getPackagingLabel($packagingType)
    {
	return Mage::getModel('temando/system_config_source_shipment_packaging')->getOptionLabel($packagingType);
    }
    
    /**
     * Gets shipment currency code (by order store)
     * 
     * @return string
     */
    public function getStoreCurrencyCode()
    {
	return $this->getShipment()->getOrder()->getStore()->getCurrentCurrencyCode();
    }
    
    /**
     * Gets symbol for shipment currency code (by order store)
     * 
     * @return string
     */
    public function getShipmentCurrencySymbol()
    {
	return Mage::app()->getLocale()->currency($this->getStoreCurrencyCode())->getSymbol();
    }
}