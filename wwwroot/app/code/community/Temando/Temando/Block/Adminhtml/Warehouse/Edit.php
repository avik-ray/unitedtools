<?php

class Temando_Temando_Block_Adminhtml_Warehouse_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    
    public function __construct()
    {
	$this->_objectId = 'id';
        $this->_blockGroup = 'temando';
        $this->_controller = 'adminhtml_warehouse';
        parent::__construct();
	

	$this->_addButton('save_and_continue_edit', array(
            'class'   => 'save',
            'label'   => Mage::helper('temando')->__('Save and Continue Edit'),
            'onclick' => 'editForm.submit($(\'edit_form\').action + \'back/edit/\')',
        ), 10);
	
	$url = $this->getUrl('etemando/pcs/autocompletecart', array('_secure' => Mage::app()->getStore()->isCurrentlySecure()));
	$script = "
	    var avs = new Avs( '$url', 'warehouse_postcode', 'warehouse_city', 'warehouse_country' );
	";
	$this->_formScripts[] = $script;
    } 

    /**
     * Getter for form header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $warehouse = Mage::registry('current_temando_warehouse');
        if ($warehouse->getId()) {
            return Mage::helper('temando')->__("Edit Warehouse '%s'", $this->escapeHtml($warehouse->getName()));
        }
        else {
            return Mage::helper('temando')->__('New Warehouse');
        }
    }
    
}
