<?php

set_time_limit(0);

/* @var $this Mage_Eav_Model_Entity_Setup */
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Multi-warehouse Support
 */
$installer->run("
   DROP TABLE IF EXISTS {$this->getTable('temando_zone')};
   CREATE TABLE {$this->getTable('temando_zone')} (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `country` varchar(2) NOT NULL,
    `ranges` text NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE `name` (`name`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
");

$installer->run("
  INSERT INTO {$this->getTable('temando_zone')} (`id`, `name`, `country`, `ranges`) VALUES
    (1, 'All of Australia', 'AU', '1:9999');
");

$installer->run("
   DROP TABLE IF EXISTS {$this->getTable('temando_warehouse')};
   CREATE TABLE {$this->getTable('temando_warehouse')} (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `is_active` tinyint(1) NOT NULL,
    `company_name` varchar(255) NOT NULL,
    `street` varchar(255) NOT NULL,
    `city` varchar(255) NOT NULL,
    `region` varchar(255) NOT NULL,
    `postcode` varchar(50) DEFAULT NULL,
    `country` varchar(50) NOT NULL,
    `contact_name` varchar(255) NOT NULL,
    `contact_email` varchar(255) NOT NULL,
    `contact_phone_1` varchar(255) NOT NULL,
    `contact_phone_2` varchar(255) DEFAULT NULL,
    `contact_fax` varchar(255) DEFAULT NULL,
    `priority` int(11) DEFAULT NULL,
    `location_type` varchar(255) NOT NULL,
    `store_ids` varchar(255) NOT NULL,
    `zone_ids` varchar(255) DEFAULT NULL,
    `loading_facilities` char(1) NOT NULL DEFAULT 'N',
    `dock` char(1) NOT NULL DEFAULT 'N',
    `forklift` char(1) NOT NULL DEFAULT 'N',
    `limited_access` char(1) NOT NULL DEFAULT 'N',
    `postal_box` char(1) NOT NULL DEFAULT 'N',
    `label_type` VARCHAR(15) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
");
   
$installer->run("
  ALTER TABLE {$this->getTable('temando_shipment')} 
    ADD `warehouse_id` int(11) NOT NULL AFTER `order_id`   
   ; 
");

$installer->run("
  ALTER TABLE {$this->getTable('temando_shipment')} 
    ADD CONSTRAINT `fk_warehouse_id` 
    FOREIGN KEY (`warehouse_id`) 
    REFERENCES {$this->getTable('temando_warehouse')} (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
   ; 
");

/**
 * Custom Packaging Support
 */
$installer->run("
   DROP TABLE IF EXISTS {$this->getTable('temando_package')};
   CREATE TABLE {$this->getTable('temando_package')} (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `packaging` int(255) NOT NULL,
    `length` decimal(12, 4) NOT NULL,
    `width` decimal(12, 4) NOT NULL,
    `height` decimal(12, 4) NOT NULL,
    `weight` decimal(12, 4) NOT NULL,
    `measure_unit` varchar(255) NOT NULL,
    `weight_unit` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8; 
");

/**
 * Rule Engine Support
 */
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('temando_rule')};
CREATE TABLE {$this->getTable('temando_rule')} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `stop_other` tinyint(1) NOT NULL DEFAULT '0',
  `store_ids` text NOT NULL,
  `group_ids` text NOT NULL,
  `condition_time_type` tinyint(4) DEFAULT NULL,
  `condition_time_value` varchar(8) DEFAULT NULL,
  `condition_day` text,
  `conditions_serialized` mediumtext,
  `action_rate_type` tinyint(4) NOT NULL,
  `action_static_value` float DEFAULT NULL,
  `action_static_label` varchar(500) DEFAULT NULL,
  `action_dynamic_carriers` text,
  `action_dynamic_filter` tinyint(4) DEFAULT NULL,
  `action_dynamic_adjustment_type` tinyint(4) DEFAULT NULL,
  `action_dynamic_adjustment_value` varchar(15) DEFAULT NULL,
  `action_dynamic_show_carrier_name` tinyint(4) NOT NULL DEFAULT '1',
  `action_dynamic_show_carrier_method` tinyint(4) NOT NULL DEFAULT '1',
  `action_dynamic_show_carrier_time` tinyint(4) NOT NULL DEFAULT '1',
  `action_dynamic_label` varchar(500) DEFAULT NULL,
  `action_restrict_note` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
");

/**
 * Remove existing product attributes
 */
$installer->removeAttribute('catalog_product', 'temando_packaging_mode');
$installer->removeAttribute('catalog_product', 'temando_packaging');
$installer->removeAttribute('catalog_product', 'temando_fragile');
$installer->removeAttribute('catalog_product', 'temando_length');
$installer->removeAttribute('catalog_product', 'temando_width');
$installer->removeAttribute('catalog_product', 'temando_height');

/**
 * Add extra attributes to support multi-packaging 
 */
$installer->addAttribute('catalog_product', 'temando_packaging_mode',
        array(
            'type' => 'int',
            'label' => 'Packaging Mode',
            'group' => 'Temando',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'source' => 'eav/entity_attribute_source_boolean',
            'input' => 'select',
            'default' => false,
            'visible' => false,
            'required' => false,
            'user_defined' => false,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'unique' => false
        )
);

for($i = 1; $i <= 5; $i++) {
    $installer->addAttribute('catalog_product', "temando_package_{$i}_type",
	    array(
		'type' => 'int',
		'label' => 'Packaging Type',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'source' => 'temando/entity_attribute_source_packaging',
		'input' => 'select',
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );
    
    $installer->addAttribute('catalog_product', "temando_package_{$i}_description",
	    array(
		'type' => 'varchar',
		'label' => 'Description',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => true
	    )
    );
    
    $installer->addAttribute('catalog_product', "temando_package_{$i}_value",
	    array(
		'type' => 'decimal',
		'label' => 'Value',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );

    $installer->addAttribute('catalog_product', "temando_package_{$i}_fragile",
	    array(
		'type' => 'int',
		'label' => 'Fragile',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'source' => 'eav/entity_attribute_source_boolean',
		'input' => 'select',
		'default' => false,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );
    
    $installer->addAttribute('catalog_product', "temando_package_{$i}_weight",
	    array(
		'type' => 'decimal',
		'label' => 'Weight',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );

    $installer->addAttribute('catalog_product', "temando_package_{$i}_length",
	    array(
		'type' => 'decimal',
		'label' => 'Length',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );

    $installer->addAttribute('catalog_product', "temando_package_{$i}_width",
	    array(
		'type' => 'decimal',
		'label' => 'Width',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );

    $installer->addAttribute('catalog_product', "temando_package_{$i}_height",
	    array(
		'type' => 'decimal',
		'label' => 'Height',
		'group' => 'Temando',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => false,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'unique' => false
	    )
    );
}
//make all attributes invisible in backend - custom tab on edit product page
$installer->run("
    UPDATE {$this->getTable('catalog_eav_attribute')} SET `is_visible` = 0
    WHERE `attribute_id` IN(SELECT `attribute_id`
    FROM {$this->getTable('eav_attribute')}
    WHERE `attribute_code` LIKE 'temando%');
");

$installer->endSetup();
?>