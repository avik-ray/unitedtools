<?php

class Temando_Temando_Adminhtml_WarehouseController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
	$this->loadLayout()->renderLayout();
    }

    public function editAction() {
	$id = $this->getRequest()->getParam('id', null);
	$model = Mage::getModel('temando/warehouse');
	if ($id) {
	    $model->load((int) $id);
	    if ($model->getId()) {
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if ($data) {
		    $model->setData($data)->setId($id);
		}
	    } else {
		Mage::getSingleton('adminhtml/session')->addError($this->__('Warehouse does not exist'));
		$this->_redirect('*/*/');
	    }
	}
	Mage::register('current_temando_warehouse', $model);

	$this->loadLayout();
	$this->renderLayout();
    }

    public function newAction() {
	$this->_forward('edit');
    }

    public function saveAction() {
	if ($data = $this->getRequest()->getPost()) {
	    $model = Mage::getModel('temando/warehouse');
	    $id = $this->getRequest()->getParam('id');
	    if ($id) {
		$model->load($id);
	    }

	    //convert arrays to string
	    foreach ($data as $key => $val) {	
		if (is_array($val)) {
		    $data[$key] = implode(',', $val);
		}
	    }
	    	    
	    $model->setData($data);
	    Mage::getSingleton('adminhtml/session')->setFormData($data);
	    try {
		if ($id) {
		    $model->setId($id);
		}
		$model->save();
		/* @var $model Temando_Temando_Model_Warehouse */	
		if (!$model->getId()) {
		    Mage::throwException($this->__('Error saving warehouse'));
		}

		//sync with temando.com
		$request['location'] = $model->toCreateLocationRequestArray();		
		try {
		    $api = Mage::getModel('temando/api_client');
		    $api->connect(
			Mage::helper('temando')->getConfigData('general/username'),
			Mage::helper('temando')->getConfigData('general/password'),
			Mage::helper('temando')->getConfigData('general/sandbox'));
		    $result = $api->getLocations(array('type' => 'Origin', 'clientId' => Mage::helper('temando')->getClientId(), 'description' => $model->getName()));
		    if($result && isset($result->locations->location)) {
			//location exists = update
			$api->updateLocation($request);
		    }
		} catch (Exception $e) {
		    try {
			$result = $api->createLocation($request);
		    } catch(Exception $e) {
			//cannot create location
			Mage::getSingleton('adminhtml/session')->addError($this->__('An error occured when synchronizing with temando.com'));
		    }
		}
		
		Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Warehouse was successfully saved.'));
		Mage::getSingleton('adminhtml/session')->setFormData(false);

		// The following line decides if it is a "save" or "save and continue"
		if ($this->getRequest()->getParam('back')) {
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		} else {
		    $this->_redirect('*/*/');
		}
	    } catch (Exception $e) {
		Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		if ($model && $model->getId()) {
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		} else {
		    $this->_redirect('*/*/');
		}
	    }

	    return;
	}
	Mage::getSingleton('adminhtml/session')->addError($this->__('No data found to save'));
	$this->_redirect('*/*/');
    }

    public function deleteAction() {
	$id = $this->getRequest()->getParam('id');
	$model = Mage::getModel('temando/warehouse')->load($id);

	if ($model->getId() == $id) {
	    try {
		$model->delete();
		$this->_getSession()->addSuccess($this->__('The warehouse has been deleted.'));
		$this->_redirect('*/*/');
		return;
	    } catch (Exception $e) {
		$this->_getSession()->addError($this->__('Error deleting warehouse.'));
		$this->_redirect('*/*/edit', array('id' => $id));
		return;
	    }
	} else {
	    $this->_getSession()->addError($this->__('Invalid warehouse id supplied. Warehouse does not exist.'));
	    $this->_redirect('*/*/');
	    return;
	}
    }

}

