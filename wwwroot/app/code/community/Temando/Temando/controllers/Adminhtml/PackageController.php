<?php

class Temando_Temando_Adminhtml_PackageController extends Mage_Adminhtml_Controller_Action {
    
        
    public function indexAction()
    {
	$this->loadLayout()
	     ->_setActiveMenu('temando/package')
	     ->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage Packaging'), Mage::helper('adminhtml')->__('Manage Packaging'))
	     ->renderLayout();
    }
    
    public function newAction() {
	$this->_forward('edit');
    }

    public function editAction() {
	$id = $this->getRequest()->getParam('id', null);
	$model = Mage::getModel('temando/package');
	if ($id) {
	    $model->load((int) $id);
	    if ($model->getId()) {
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if ($data) {
		    $model->setData($data)->setId($id);
		}
	    } else {
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('temando')->__('Package does not exist'));
		$this->_redirect('*/*/');
	    }
	}
	Mage::register('current_temando_package', $model);

	$this->loadLayout();
	$this->renderLayout();
    }
    
    
    public function saveAction() {	
	if ($data = $this->getRequest()->getPost()) {
	    $model = Mage::getModel('temando/package');
	    $data = $this->getRequest()->getPost();
	    
	    $id = $this->getRequest()->getParam('id');
	    if ($id) {
		$model->load($id);
	    }
	    
	    $model->setData($data);

	    Mage::getSingleton('adminhtml/session')->setFormData($data);
	    try {
		if ($id) {
		    $model->setId($id);
		}
		$model->save();

		if (!$model->getId()) {
		    Mage::throwException(Mage::helper('temando')->__('Error saving package data'));
		}

		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('temando')->__('Package successfully saved.'));
		Mage::getSingleton('adminhtml/session')->setFormData(false);

		// The following line decides if it is a "save" or "save and continue"
		if ($this->getRequest()->getParam('back')) {
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		} else {
		    $this->_redirect('*/*/');
		}
	    } catch (Exception $e) {
		Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		if ($model && $model->getId()) {
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		} else {
		    $this->_redirect('*/*/');
		}
	    }

	    return;
	}
	Mage::getSingleton('adminhtml/session')->addError(Mage::helper('temando')->__('No data found to save'));
	$this->_redirect('*/*/');
    }

    public function deleteAction() {
	if ($id = $this->getRequest()->getParam('id')) {
	    try {
		$model = Mage::getModel('temando/package')->load($id);
		$model->delete();
		$this->_getSession()->addSuccess(Mage::helper('temando')->__('Package has been deleted.'));
		$this->_redirect('*/*/');
		return;
	    } catch (Exception $e) {
		Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
		return;
	    }
	}
	Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the package to delete.'));
	$this->_redirect('*/*/');
    }
    
}
