<?php

class Temando_Temando_Adminhtml_RuleController extends Mage_Adminhtml_Controller_Action {
    
    const MODE_DISABLE = 0;
    const MODE_ENABLE = 1;
    const MODE_DELETE = 2;
    
    
    protected $_allowedModes = array(
	self::MODE_DISABLE => 'disabled',
	self::MODE_ENABLE => 'enabled',
	self::MODE_DELETE => 'deleted',
    );
    
    
    public function indexAction()
    {
	$this->loadLayout()
	     ->_setActiveMenu('temando/rule')
	     ->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage Rules'), Mage::helper('adminhtml')->__('Manage Rules'))
	     ->renderLayout();
    }
    
    public function newAction() {
	$this->_forward('edit');
    }

    public function editAction() {
	$id = $this->getRequest()->getParam('id', null);
	$model = Mage::getModel('temando/rule');
	if ($id) {
	    $model->load((int) $id);
	    if ($model->getId()) {
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if ($data) {
		    $model->setData($data)->setId($id);
		}
	    } else {
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('temando')->__('Rule does not exist'));
		$this->_redirect('*/*/');
	    }
	}
	$model->getConditions()->setJsFormObject('rule_conditions_fieldset');
	Mage::register('current_temando_rule', $model);

	$this->loadLayout();
	$this->renderLayout();
    }
    
    
    public function saveAction() {
	if ($this->getRequest()->getPost()) {
	    try {
		/** @var $model Temando_Temando_Model_Rule */
		$model = Mage::getModel('temando/rule');
		$data = $this->getRequest()->getPost();
		$data = $this->_filterDates($data, array('from_date', 'to_date'));
		$id = $this->getRequest()->getParam('id');
		if ($id) {
		    $model->load($id);
		    if ($id != $model->getId()) {
			Mage::throwException(Mage::helper('temando')->__('Wrong rule specified.'));
		    }
		}
		$session = Mage::getSingleton('adminhtml/session');
		$validateResult = $model->validateData(new Varien_Object($data));
		if ($validateResult !== true) {
		    foreach ($validateResult as $errorMessage) {
			$session->addError($errorMessage);
		    }
		    $session->setPageData($data);
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		    return;
		}
		if (isset($data['rule']['conditions'])) {
		    $data['conditions'] = $data['rule']['conditions'];
		}
		unset($data['rule']);
		//convert other data arrays to string
		foreach ($data as $key => $val) {
		    if ($key !== 'conditions' && is_array($val)) {
			$data[$key] = implode(',', $val);
		    }
		}
		//load conditions
		$model->loadPost($data);
		$session->setPageData($model->getData());
		$model->save();
		$session->addSuccess(Mage::helper('temando')->__('The rule has been saved.'));
		$session->setPageData(false);
		if ($this->getRequest()->getParam('back')) {
		    $this->_redirect('*/*/edit', array('id' => $model->getId()));
		    return;
		}
		$this->_redirect('*/*/');
		return;
	    } catch (Mage_Core_Exception $e) {
		$this->_getSession()->addError($e->getMessage());
		$id = (int) $this->getRequest()->getParam('id');
		if (!empty($id)) {
		    $this->_redirect('*/*/edit', array('id' => $id));
		} else {
		    $this->_redirect('*/*/new');
		}
		return;
	    } catch (Exception $e) {
		$this->_getSession()->addError(
			Mage::helper('temando')->__('An error occurred while saving the rule data. Please review your entries and try again.'));
		Mage::logException($e);
		Mage::getSingleton('adminhtml/session')->setPageData($data);
		$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
		return;
	    }
	}
	$this->_redirect('*/*/');
    }

    public function deleteAction() {
	if ($id = $this->getRequest()->getParam('id')) {
	    try {
		$model = Mage::getModel('temando/rule')->load($id);
		$model->delete();
		$this->_getSession()->addSuccess(Mage::helper('temando')->__('The rule has been deleted.'));
		$this->_redirect('*/*/');
		return;
	    } catch (Exception $e) {
		Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
		return;
	    }
	}
	Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the rule to delete.'));
	$this->_redirect('*/*/');
    }
    
    public function massDisableAction() {
	return $this->massStatusAction(self::MODE_DISABLE);
    }
    
    public function massEnableAction() {
	return $this->massStatusAction(self::MODE_ENABLE);
    }
    
    public function massDeleteAction() {
	return $this->massStatusAction(self::MODE_DELETE);
    }
    
    protected function massStatusAction($mode = 0)
    {
	if(!array_key_exists($mode, $this->_allowedModes)) {
	    $this->_getSession()->addError('Invalid mode specified for mass status action.');
	    $this->_redirect('*/*');
	    return;
	}	
	$params = $this->getRequest()->getParams();
	if (!isset($params['massaction']) || !is_array($params['massaction']) || empty($params['massaction'])) {
	    $this->_getSession()->addError(Mage::helper('temando')->__('No rules selected.'));
	    $this->_redirect('*/*/');
	}
	$rule_ids = $params['massaction'];
	$notices = array(); $count = 0;
	foreach ($rule_ids as $id) {
	    $rule = Mage::getModel('temando/rule')->load($id);
	    if (!$rule || !$rule->getId()) {
		$notices[] = "Rule ID $id not found.";
		continue;
	    }	    
	    switch($mode) {
		case self::MODE_DELETE:
		    $rule->delete(); break;
		case self::MODE_ENABLE:
		case self::MODE_DISABLE:
		    $rule->setIsActive($mode);
		    $rule->save();
		    break;
	    }
	    
	    $count++;
	}
	if (!empty($notices)) {
	    foreach ($notices as $notice)
		$this->_getSession()->addError($notice);
	}	
	$successMsg = $this->_allowedModes[$mode];
	$this->_getSession()->addSuccess("Total of $count rules $successMsg.");
	//back to index shipment grid
	$this->_redirect('*/*/');

	return;
    }    
}