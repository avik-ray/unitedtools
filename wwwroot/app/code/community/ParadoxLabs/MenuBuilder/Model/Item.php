<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Model_Item extends Mage_Core_Model_Abstract
{
	protected $_resourceCollectionName = 'menubuilder/item_collection';

	public function _construct() {
		parent::_construct();
		$this->_init('menubuilder/item');
	}

	public function getForm() {
		return Mage::app()->getLayout()->createBlock('menubuilder/admin_edit')->setItem( $this )->toHtml();
	}
}
