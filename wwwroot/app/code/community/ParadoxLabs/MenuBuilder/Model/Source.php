<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category    ParadoxLabs
 * @package     ParadoxLabs_MenuBuilder
 * @author      Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Model_Source
{
    /**
     * Build an array of menus for configuration dropdowns.
     */
    public function toOptionArray($addEmpty = true)
    {
        $menus = Mage::getModel('menubuilder/menu')->getCollection()->getIterator();

        $options = array();
        $options[] = array(
            'label' => Mage::helper('adminhtml')->__('- None -'),
            'value' => ''
        );

        foreach( $menus as $menu ) {
            $options[] = array(
               'label' => $menu->getName(),
               'value' => $menu->getId()
            );
        }

        return $options;
    }
}
