<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Model_Observer
{
	/**
	 * When saving a new CMS page, add it to any menus set as auto-add.
	 */
	public function savePage( $observer ) {
		$page = $observer->getObject();

		if( $page->getIsNewPage() && $page->getIsActive() ) {
			$menus = Mage::getModel('menubuilder/menu')->getCollection()
							->addAttributeToFilter( 'auto_add_pages', 1 )
							->getIterator();

			foreach( $menus as $menu ) {
				// Get max order in menu
				$order = Mage::getModel('menubuilder/item')->getCollection()
								->addAttributeToSort( 'sort_order', 'desc' )
								->addAttributeToFilter( 'menu_id', $menu->getId() )
								->getFirstItem();

				// Create and save the new menu item
				$item = Mage::getModel('menubuilder/item')
							->setLabel( $page->getTitle() )
							->setUrl( $page->getPageId() )
							->setMenuId( $menu->getId() )
							->setSortOrder( $order->getSortOrder() + 1 )
							->setType( 'page' )
							->save();

				// Update cache
				Mage::app()->cleanCache( array( 'menubuilder' ) );
			}
		}
	}
}
