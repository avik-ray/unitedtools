<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Model_Menu extends Mage_Core_Model_Abstract
{
	protected $_resourceCollectionName = 'menubuilder/menu_collection';

	public function _construct() {
		parent::_construct();
		$this->_init('menubuilder/menu');
	}

	/**
	 * Update a menu with the given input fields and items.
	 * If given an ID > 0, an existing menu will be loaded; otherwise, a new one is created.
	 * 
	 * @param  int 		$id
	 * @param  array 	$post
	 * @return $this
	 */
	public function updateMenu( $id, $post ) {
		if( empty($post['name']) || empty($post['identifier']) ) {
			return $this;
		}

		if( $id > 0 ) {
			$this->load( $id );
		}
		else {
			$this->setCreated( time() );
		}

		$this->setName( $post['name'] )
			 ->setIdentifier( $post['identifier'] )
			 ->setLayout( $post['layout'] )
			 ->setAutoAddPages( $post['auto_add_pages'] ? 1 : 0 )
			 ->save();

		/**
		 * If new menu, associate any items with id=0 with it.
		 * This is an icky special case thanks to the chosen workflow. Items are saved before the menu exists.
		 */
		if( $id == 0 ) {
			$items = Mage::getModel('menubuilder/item')->getCollection()
							->addAttributeToFilter('menu_id', 0)
							->getIterator();
			foreach( $items as $item ) {
				$item->setMenuId( $this->getId() )->save();
			}
		}
		
		return $this;
	}

	/**
	 * Fetch the admin form for this menu.
	 */
	public function getForm() {
		return Mage::app()->getLayout()->createBlock('menubuilder/admin_form')->setMenu( $this )->toHtml();
	}

	/**
	 * Fetch this menu's items collection.
	 */
	public function getItems() {
		return Mage::getModel('menubuilder/item')->getCollection()
						->addAttributeToFilter('menu_id', $this->getId())
						->addAttributeToSort('sort_order', 'asc')
						->getIterator();
	}

	/**
	 * Build the menu tree in JSON form for AJAX.
	 */
	public function getAdminTree() {
		$out  = '<div id="tree-'.$this->getId().'">';
		$out .= '</div>';

		$items = $this->getItems();

		/**
		 * Build the nested array in one magical pass.
		 */
		$json = $references = array();
		foreach( $items as $item ) {
			if( !isset( $references[ $item->getId() ] ) ) {
				$references[ $item->getId() ] = array(	'id'	=> $item->getId(),
														'label' => $item->getLabel()
														 );
			}

			// If it's a root node, add it directly to the tree.
			if( $item->getParentId() < 1 ) {
				$json[ $item->getId() ] = &$references[ $item->getId() ];
			}
			// Otherwise, add it as a reference in the parent.
			else {
				$references[ $item->getParentId() ]['children'][] = &$references[ $item->getId() ];
			}
		}

		return array( 'tree' => $out, 'json' => array_values($json) );
	}

	public function getFrontendTree() {
		$items = $this->getItems();

		/**
		 * Build the nested array in one magical pass.
		 */
		$array = $references = array();
		foreach( $items as $item ) {
			if( !isset( $references[ $item->getId() ] ) ) {
				$references[ $item->getId() ] = array(	'id'	=> $item->getId(),
														'label' => $item->getLabel(),
														'type'	=> $item->getType(),
														'url'	=> $item->getUrl()
														 );
			}

			// If it's a root node, add it directly to the tree.
			if( $item->getParentId() < 1 ) {
				$array[ $item->getId() ] = &$references[ $item->getId() ];
			}
			// Otherwise, add it as a reference in the parent.
			else {
				$references[ $item->getParentId() ]['children'][] = &$references[ $item->getId() ];
			}
		}

		return $array;
	}

	protected function _beforeSave() {
		Mage::app()->cleanCache( array( $this->getId() . '-' . $this->getLayout() ) );

		return parent::_beforeSave();
	}
}
