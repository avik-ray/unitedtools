<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Adminhtml_MenubuilderController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Render the menu builder interface.
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->_setActiveMenu('cms/menubuilder');
		$this->renderLayout();
	}

	/**
	 * Save all menu settings, and add any new additions.
	 */
	public function saveAction() {
		$menus = $this->getRequest()->getParam('menu');

		if( count($menus) > 0 ) {
			foreach( $menus as $id => $menu ) {
				$id = intval( $id );

				Mage::getModel('menubuilder/menu')->updateMenu( $id, $menu );
			}
		}
		
		Mage::app()->cleanCache( array( 'menubuilder' ) );
		
		$this->_getSession()->addSuccess( $this->__('Saved menu changes.') );
		$this->_redirect( '*/*/index' );
	}

	/**
	 * Delete a menu and all items.
	 */
	public function deleteAction() {
		$id = intval( $this->getRequest()->getParam('id') );

		// Kill the menu
		$menu = Mage::getModel('menubuilder/menu')->load( $id )->delete();

		// Kill the links
		$items = Mage::getModel('menubuilder/item')->getCollection()
						->addAttributeToFilter('menu_id', $id)
						->getIterator();

		foreach( $items as $item ) {
			$item->delete();
		}
		
		$this->_getSession()->addSuccess( $this->__("Deleted the '%s' menu.", $menu->getName()) );
		$this->_redirect( '*/*/index' );
	}

	/**
	 * Add an item or items to the current menu.
	 */
	public function addItemAction() {
		$type	= $this->getRequest()->getParam('type');
		$id		= intval( $this->getRequest()->getParam('menu') );

		$order	= Mage::getModel('menubuilder/item')->getCollection()
						->addAttributeToSort('sort_order', 'desc')
						->addAttributeToFilter('menu_id', $id)
						->getFirstItem();
		$order	= $order->getSortOrder() + 1;

		switch( $type ) {
			case 'link':
				// echo $url
				$post = $this->getRequest()->getParam('custom');

				$item = Mage::getModel('menubuilder/item')
							->setLabel( $post['text'] )
							->setUrl( $post['url'] )
							->setMenuId( $id )
							->setSortOrder( $order )
							->setType( 'link' )
							->save();
				break;
			case 'page':
				// echo Mage::helper('cms/page')->getPageUrl( $url );
				$pages = $this->getRequest()->getParam('page');

				foreach( $pages as $page ) {
					$page = Mage::getModel('cms/page')->load( intval($page) );

					$item = Mage::getModel('menubuilder/item')
								->setLabel( $page->getTitle() )
								->setUrl( $page->getId() )
								->setMenuId( $id )
								->setSortOrder( $order++ )
								->setType( 'page' )
								->save();
				}
				break;
			case 'cat':
				// echo Mage::getModel('catalog/category')->load(10)->getUrl();
				$cats = $this->getRequest()->getParam('category');

				foreach( $cats as $cat ) {
					$cat = Mage::getModel('catalog/category')->load( intval($cat) );

					$item = Mage::getModel('menubuilder/item')
								->setLabel( $cat->getName() )
								->setUrl( $cat->getEntityId() )
								->setMenuId( $id )
								->setSortOrder( $order++ )
								->setType( 'category' )
								->save();
				}
				break;
		}

		$menu = Mage::getModel('menubuilder/menu')->load( $id );
		$tree = $menu->getAdminTree();
		
		Mage::app()->cleanCache( array( $menu->getId() . '-' . $menu->getLayout() ) );

		echo json_encode( $tree );
	}

	/**
	 * Fetch the edit form for a given item.
	 */
	public function editItemAction() {
		$id		= intval( $this->getRequest()->getParam('id') );
		$item	= Mage::getModel('menubuilder/item')->load( $id );

		echo json_encode( array( 'form' => $item->getForm() ) );
	}

	/**
	 * Save an edit form and update the tree.
	 */
	public function doEditItemAction() {
		$id		= intval( $this->getRequest()->getParam('id') );
		$item	= Mage::getModel('menubuilder/item')->load( $id );

		if( $item->getType() == 'link' ) {
			$item->setUrl( $this->getRequest()->getParam('url') );
		}
		$item->setLabel( $this->getRequest()->getParam('label') );
		$item->save();

		$menu	= Mage::getModel('menubuilder/menu')->load( $item->getMenuId() );
		$tree	= $menu->getAdminTree();

		Mage::app()->cleanCache( array( $menu->getId() . '-' . $menu->getLayout() ) );

		echo json_encode( $tree );
	}

	/**
	 * Remove item(s) from the tree.
	 */
	public function removeItemAction() {
		$id 	= intval( $this->getRequest()->getParam('menu') );
		$tree	= json_decode( $this->getRequest()->getParam('tree'), 1 );

		// Turn recursive tree into flat id array.
		$new = $tmp = array();
		foreach( $tree as $item ) {
			$tmp = array_merge( $tmp, $this->processTree( $item, 0 ) );
		}
		foreach( $tmp as $item ) {
			$new[] = $item['id'];
		}

		$menu 	= Mage::getModel('menubuilder/menu')->load( $id );
		$items	= $menu->getItems();

		// Remove any items not found in the new list.
		foreach( $items as $item ) {
			if( !in_array( $item->getId(), $new ) ) {
				$item->delete();
			}
		}

		$tree	= $menu->getAdminTree();

		Mage::app()->cleanCache( array( $menu->getId() . '-' . $menu->getLayout() ) );

		echo json_encode( $tree );
	}

	/**
	 * Reorder a menu's items and update parent/child relations.
	 * [{"id":"1","name":"Home","is_open":true},
	 *  {"id":"2","name":"Welcome to our Exclusive Online Store","is_open":true,
	 *  	"children": [{"id":"3","name":"About  Us","is_open":true}]
	 *  }]
	 */
	public function reorderAction() {
		$id		= intval( $this->getRequest()->getParam('menu') );
		$tree	= json_decode( $this->getRequest()->getParam('tree'), 1 );

		// Turn recursive tree into flat ordered id/parent array.
		$items	= array();
		foreach( $tree as $item ) {
			$items = array_merge( $items, $this->processTree( $item, 0 ) );
		}

		$count	= 0;
		foreach( $items as $item ) {
			Mage::getModel('menubuilder/item')->load( $item['id'] )
					->setSortOrder( $count++ )
					->setParentId( $item['parent_id'] )
					->save();
		}

		$menu	= Mage::getModel('menubuilder/menu')->load( $id );
		$tree	= $menu->getAdminTree();

		Mage::app()->cleanCache( array( $menu->getId() . '-' . $menu->getLayout() ) );

		echo json_encode( $tree );
	}

	/**
	 * Turn the nested/recursive array into a flat array.
	 */
	protected function processTree( $tree, $parent ) {
		$items   = array();
		$items[] = array(	'id'		=> $tree['id'],
							'parent_id'	=> $parent );

		if( isset($tree['children']) ) {
			foreach( $tree['children'] as $child ) {
				$items = array_merge( $items, $this->processTree( $child, $tree['id'] ) );
			}
		}

		return $items;
	}
}
