<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

$installer	= $this;
$installer->startSetup();

$table		= $installer->getTable('menubuilder/menu');
$installer->run("CREATE TABLE IF NOT EXISTS {$table} (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255) NOT NULL DEFAULT '',
	identifier VARCHAR(255) NOT NULL DEFAULT '',
	created TIMESTAMP DEFAULT 0,
	modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	layout VARCHAR(255) NOT NULL DEFAULT '',
	auto_add_pages TINYINT(1) DEFAULT 0,
	UNIQUE menukey ( identifier )
);");

$table		= $installer->getTable('menubuilder/item');
$installer->run("CREATE TABLE IF NOT EXISTS {$table} (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	menu_id INT NOT NULL,
	parent_id INT NOT NULL DEFAULT 0,
	sort_order INT NOT NULL,
	label VARCHAR(255) NOT NULL DEFAULT '',
	url TEXT,
	type VARCHAR(255) NOT NULL DEFAULT '',
	INDEX menus (menu_id, sort_order)
);");

$installer->endSetup();
