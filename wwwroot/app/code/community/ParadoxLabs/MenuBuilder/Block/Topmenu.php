<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Block_Topmenu extends Mage_Page_Block_Html_Topmenu
{
	/**
	 * If we've chosen to override the normal primary navigation,
	 * we don't want Magento spitting that out.
	 */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {
		$id = Mage::getStoreConfig('menubuilder/menubuilder/primary_nav');

		if( empty($id) ) {
			return parent::getHtml();
		}
		else {
			return '';
		}
    }
}
