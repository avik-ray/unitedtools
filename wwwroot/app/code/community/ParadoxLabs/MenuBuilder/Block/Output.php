<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Block_Output extends Mage_Core_Block_Template
	implements Mage_Widget_Block_Interface
{
	public $menu;
	public $id;
	public $is_primary = 0;

	protected function _construct() {
		parent::_construct();
		$this->setTemplate('menubuilder/output.phtml');
	}

	/**
	 * Initialize block-level caching for this menu.
	 */
	protected function initCache() {
		$this->addData(array(
			'cache_lifetime'	=> 3600,
			'cache_tags'		=> array( $this->menu->getId() . '-' . $this->getOutputType(), 'menubuilder' ),
			'cache_key'			=> 'menu-' . $this->menu->getId() . '-' . $this->getOutputType(),
		));
	}

	/**
	 * Load menu by ID.
	 */
	public function setId( $id ) {
		return $this->setData( 'id', $id );
	}
	
	/**
	 * Interrupt addData for widgets.
	 */
    public function addData(array $arr)
    {
        foreach($arr as $index=>$value) {
            $this->setData($index, $value);
        }
        
        return $this;
    }
	
	/**
	 * Interrupt setData for widgets.
	 */
    public function setData($key, $value=null)
    {
		if( $key == 'id' ) {
			$this->menu = Mage::getModel('menubuilder/menu')->load( $value );

			$this->initCache();
		}
		
		parent::setData($key, $value);
		
		return $this;
    }

	/**
	 * Load menu by identifier.
	 */
	public function setIdentifier( $key ) {
		$this->menu = Mage::getModel('menubuilder/menu')->load( $key, 'identifier' );
		$this->setData( 'id', $this->menu->getId() );

		return parent::setIdentifier( $key );
	}

	public function setOutputType( $type ) {
		parent::setOutputType( $type );

		$this->initCache();

		return $this;
	}

	/**
	 * Allow XML overwrites of the menu output style.
	 */
	public function getOutputType() {
		$type = parent::getOutputType();

		if( !empty($type) ) {
			return $type;
		}
		else {
			return $this->menu->getLayout();
		}
	}

	/**
	 * Generate the nested menu recursively.
	 */
	public function renderMenu($item, $level=1, $items=1, $current=0) {
		$children = '';
		if( isset($item['children']) ) {
			$childCount = count($item['children']);
			$count = 0;
			foreach( $item['children'] as $child ) {
				$children .= $this->renderMenu( $child, $level+1, $childCount, $count++ );
			}
		}

		if( $item['type'] == 'page' ) {
			$url = Mage::helper('cms/page')->getPageUrl( $item['url'] );
		}
		elseif( $item['type'] == 'category' ) {
			$url = Mage::getModel('catalog/category')->load( $item['url'] )->getUrl();
		}
		else {
			$url = $item['url'];
		}

		$item['level']		= $level;
		$item['countItems'] = $items;
		$item['countCur']	= $current;
		$item['child_html'] = $children;
		$item['output_url'] = $url;

		return Mage::app()->getLayout()->createBlock('core/template')
						  ->setTemplate('menubuilder/output-item.phtml')
						  ->setItem( $item )
						  ->toHtml();
	}
}
