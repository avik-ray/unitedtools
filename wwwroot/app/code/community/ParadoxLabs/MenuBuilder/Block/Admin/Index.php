<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Block_Admin_Index extends Mage_Adminhtml_Block_Template
{
	protected $_menus;

	public function _construct() {
		$this->_menus = Mage::getModel('menubuilder/menu')->getCollection()
							->getIterator();
	}

	public function getMenus() {
		return $this->_menus;
	}

	public function getPages() {
		return Mage::getModel('cms/page')->getCollection()
					->getIterator();
	}

	public function getCats() {
		return Mage::getModel('catalog/category')->getCollection()
					->addAttributeToSelect('is_active')
					->addAttributeToSelect('name')
    				->addAttributeToSort('path', 'ASC')
    				->getIterator();
	}
}