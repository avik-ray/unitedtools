<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Block_Admin_Edit extends Mage_Adminhtml_Block_Template 
{
	protected $item;

	public function _construct() {
		$this->setTemplate('menubuilder/item-form.phtml');
		
		parent::_construct();
	}

	public function setItem( $item ) {
		$this->item = $item;

		return $this;
	}
}
