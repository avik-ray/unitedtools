<?php
/**
 * Magento Menu Builder
 *
 * ParadoxLabs inc
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Having a problem with the plugin?
 * Not sure what something means?
 * Need custom development?
 * Give us a call!
 *
 * @category	ParadoxLabs
 * @package		ParadoxLabs_MenuBuilder
 * @author		Ryan Hoerr <ryan@paradoxlabs.com>
 */

class ParadoxLabs_MenuBuilder_Block_Primarynav extends ParadoxLabs_MenuBuilder_Block_Output {
	protected function _construct() {
		parent::_construct();

		$id = Mage::getStoreConfig('menubuilder/menubuilder/primary_nav');

		/**
		 * If we're overriding the primary navigation, set up the block for it.
		 */
		if( !empty($id) ) {
			$this->setId( $id );
			$this->setTemplate('menubuilder/output-primarynav.phtml');
		}
	}
}
