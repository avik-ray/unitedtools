<?php

require_once( dirname(__FILE__).'/app/Mage.php' );
Mage::app("default");
$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
$sql = 'select sku, count(sku) as count, group_concat(entity_id) as ids from catalog_product_entity group by sku having count(sku)
 > 1';
echo "Duplicate SKUs";
echo "<table border='1'>";
echo "<tr><th>SKU</th><th>Count</th><th>Ids</th></tr>";
foreach($connection->fetchAll($sql) as $row) {
    echo "<tr><td>${row['sku']}</td><td>${row['count']}</td><td>${row['ids']}</td></tr>";
}
echo "</table>";
